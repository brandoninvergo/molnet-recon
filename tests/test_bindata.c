/* 
 * test_bindata.c --- 
 * 
 * Copyright (C) 2017, 2018, 2019 Brandon Invergo <invergo@ebi.ac.uk>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <check.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_int.h>
#include <gsl/gsl_math.h>
#include "helper_funcs.h"

#include "../src/bindata.h"
#include "../src/safe_alloc.h"

#define TOL 1e-6


START_TEST(test_bin_data_even_2)
{
  gsl_matrix_view dat_m;
  gsl_matrix_int_view bins_m;
  gsl_matrix_int *bins_test;
  double dat[] = {0.1, 0.3, 0.2, 0.7,
                  0.5, 0.2, 0.3, 0.1,
                  0.8, 0.9, 0.2, 0.3};
  int bins[] = {1, 1, 1, 2,
                2, 1, 1, 1,
                2, 2, 1, 1};
  dat_m = gsl_matrix_view_array (dat, 3, 4);
  bins_m = gsl_matrix_int_view_array (bins, 3, 4);
  bins_test = safe_matrix_int_calloc (3, 4);
  bin_data (&dat_m.matrix, bins_test, 2, BIN_EVEN);
  ck_assert (gsl_matrix_int_equal (bins_test, &bins_m.matrix));
  gsl_matrix_int_free (bins_test);
}
END_TEST


START_TEST(test_bin_data_even_3)
{
  gsl_matrix_view dat_m;
  gsl_matrix_int_view bins_m;
  gsl_matrix_int *bins_test;
  double dat[] = {0.1, 0.3, 0.2, 0.7,
                  0.5, 0.2, 0.3, 0.1,
                  0.8, 0.9, 0.2, 0.3};
  int bins[] = {1, 1, 1, 3,
                2, 1, 1, 1,
                3, 3, 1, 1};
  dat_m = gsl_matrix_view_array (dat, 3, 4);
  bins_m = gsl_matrix_int_view_array (bins, 3, 4);
  bins_test = safe_matrix_int_calloc (3, 4);
  bin_data (&dat_m.matrix, bins_test, 3, BIN_EVEN);
  ck_assert (gsl_matrix_int_equal (bins_test, &bins_m.matrix));
  gsl_matrix_int_free (bins_test);
}
END_TEST


START_TEST(test_bin_data_even_4)
{
  gsl_matrix_view dat_m;
  gsl_matrix_int_view bins_m;
  gsl_matrix_int *bins_test;
  double dat[] = {0.1, 0.3, 0.2, 0.7,
                  0.5, 0.2, 0.3, 0.1,
                  0.8, 0.9, 0.2, 0.3};
  int bins[] = {1, 1, 1, 3,
                3, 1, 1, 1,
                4, 4, 1, 1};
  dat_m = gsl_matrix_view_array (dat, 3, 4);
  bins_m = gsl_matrix_int_view_array (bins, 3, 4);
  bins_test = safe_matrix_int_calloc (3, 4);
  bin_data (&dat_m.matrix, bins_test, 4, BIN_EVEN);
  ck_assert (gsl_matrix_int_equal (bins_test, &bins_m.matrix));
  gsl_matrix_int_free (bins_test);
}
END_TEST


START_TEST(test_bin_data_quantiles_2)
{
  gsl_matrix_view dat_m;
  gsl_matrix_int_view bins_m;
  gsl_matrix_int *bins_test;
  double dat[] = {0.1, 0.3, 0.2, 0.7,
                  0.5, 0.2, 0.3, 0.1,
                  0.8, 0.9, 0.2, 0.3};
  int bins[] = {1, 2, 1, 2,
                2, 1, 2, 1,
                2, 2, 1, 2};
  dat_m = gsl_matrix_view_array (dat, 3, 4);
  bins_m = gsl_matrix_int_view_array (bins, 3, 4);
  bins_test = safe_matrix_int_calloc (3, 4);
  bin_data (&dat_m.matrix, bins_test, 2, BIN_QUANTILES);
  print_matrix_int (bins_test);
  ck_assert (gsl_matrix_int_equal (bins_test, &bins_m.matrix));
  gsl_matrix_int_free (bins_test);
}
END_TEST


START_TEST(test_bin_data_quantiles_3)
{
  gsl_matrix_view dat_m;
  gsl_matrix_int_view bins_m;
  gsl_matrix_int *bins_test;
  double dat[] = {0.1, 0.3, 0.2, 0.7,
                  0.5, 0.2, 0.3, 0.1,
                  0.8, 0.9, 0.2, 0.3};
  int bins[] = {1, 2, 2, 3,
                3, 2, 2, 1,
                3, 3, 2, 2};
  dat_m = gsl_matrix_view_array (dat, 3, 4);
  bins_m = gsl_matrix_int_view_array (bins, 3, 4);
  bins_test = safe_matrix_int_calloc (3, 4);
  bin_data (&dat_m.matrix, bins_test, 3, BIN_QUANTILES);
  ck_assert (gsl_matrix_int_equal (bins_test, &bins_m.matrix));
  gsl_matrix_int_free (bins_test);
}
END_TEST


START_TEST(test_bin_data_quantiles_4)
{
  gsl_matrix_view dat_m;
  gsl_matrix_int_view bins_m;
  gsl_matrix_int *bins_test;
  double dat[] = {0.1, 0.3, 0.2, 0.7,
                  0.5, 0.2, 0.3, 0.1,
                  0.8, 0.9, 0.2, 0.3};
  int bins[] = {1, 3, 2, 4,
                3, 2, 3, 1,
                4, 4, 2, 3};
  dat_m = gsl_matrix_view_array (dat, 3, 4);
  bins_m = gsl_matrix_int_view_array (bins, 3, 4);
  bins_test = safe_matrix_int_calloc (3, 4);
  bin_data (&dat_m.matrix, bins_test, 4, BIN_QUANTILES);
  ck_assert (gsl_matrix_int_equal (bins_test, &bins_m.matrix));
  gsl_matrix_int_free (bins_test);
}
END_TEST


Suite *
bindata_suite (void)
{
  Suite *s;
  TCase *tc_binning;

  s = suite_create ("bindata");

  tc_binning = tcase_create ("Binning Methods");
  tcase_add_test (tc_binning, test_bin_data_even_2);
  tcase_add_test (tc_binning, test_bin_data_even_3);
  tcase_add_test (tc_binning, test_bin_data_even_4);
  tcase_add_test (tc_binning, test_bin_data_quantiles_2);
  tcase_add_test (tc_binning, test_bin_data_quantiles_3);
  tcase_add_test (tc_binning, test_bin_data_quantiles_4);
  suite_add_tcase (s, tc_binning);

  return s;
}


int main (void)
{
  int n_fail;
  Suite *s;
  SRunner *sr;

  s = bindata_suite ();
  sr = srunner_create (s);

  srunner_run_all (sr, CK_NORMAL);
  n_fail = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
