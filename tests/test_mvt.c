/* 
 * test_mvt.c --- 
 * 
 * Copyright (C) 2017, 2019 Brandon M. Invergo <invergo@ebi.ac.uk>
 * 
 * Author: Brandon M. Invergo <invergo@ebi.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <check.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_statistics_double.h>
#include "../src/mvt.h"
#include "../src/safe_alloc.h"

#define TOL 1e-7
#define RANDTOL 1e-1

static gsl_rng *rng;


START_TEST(test_ran_multivariate_t)
{
  gsl_matrix *sigma;
  gsl_vector *mu, *result, *results1, *results2;
  size_t iterations = 100000;
  double nu = 5.0;
  double cs_ex;
  double mean1, mean2, cov, cov_ex, cor, cor_ex;
  int err;

  gsl_rng_set (rng, 1);

  result = safe_vector_alloc (2);

  sigma = safe_matrix_alloc (2, 2);
  gsl_matrix_set (sigma, 0, 0, 1.0);
  gsl_matrix_set (sigma, 0, 1, 0.5);
  gsl_matrix_set (sigma, 1, 0, 0.5);
  gsl_matrix_set (sigma, 1, 1, 1.0);
  mu = safe_vector_alloc (2);
  gsl_vector_set_zero (mu);
  gsl_rng_set (rng, 1);
  err = ran_multivariate_t (rng, mu, sigma, nu, result);
  ck_assert (err == 0);

  gsl_vector_set (mu, 0, 1.0);
  gsl_vector_set (mu, 1, 2.0);
  gsl_matrix_set (sigma, 0, 0, 4.0);
  gsl_matrix_set (sigma, 0, 1, 2.0);
  gsl_matrix_set (sigma, 1, 0, 2.0);
  gsl_matrix_set (sigma, 1, 1, 3.0);
  results1 = safe_vector_alloc (iterations);
  results2 = safe_vector_alloc (iterations);
  for (size_t i=0; i<iterations; i++)
    {
      err = ran_multivariate_t (rng, mu, sigma, nu, result);
      gsl_vector_set (results1, i, gsl_vector_get (result, 0));
      gsl_vector_set (results2, i, gsl_vector_get (result, 1));
    }
  /* Expected value: E[X] = mu*/
  mean1 = gsl_stats_mean (results1->data, results1->stride, iterations);
  mean2 = gsl_stats_mean (results2->data, results2->stride, iterations);
  ck_assert_msg (gsl_fcmp (mean1, gsl_vector_get (mu, 0), RANDTOL)==0,
                 "%f != %f", mean1, gsl_vector_get (mu, 0));
  ck_assert_msg (gsl_fcmp (mean2, gsl_vector_get (mu, 1), RANDTOL)==0,
                 "%f != %f", mean1, gsl_vector_get (mu, 1));

  /* Covariance: E[X] = nu/(nu-2)*sigma */
  cs_ex = nu/(nu-2.0);
  cov = gsl_stats_covariance_m (results1->data, results1->stride,
                                results1->data, results1->stride,
                                iterations, mean1, mean1);
  cov_ex = cs_ex * gsl_matrix_get (sigma, 0, 0);
  ck_assert_msg (gsl_fcmp (cov, cov_ex, RANDTOL)==0, "(1,1): %f != %f",
                 cov, cov_ex);
  cov = gsl_stats_covariance_m (results1->data, results1->stride,
                                results2->data, results2->stride,
                                iterations, mean1, mean2);
  cov_ex = cs_ex * gsl_matrix_get (sigma, 0, 1);
  ck_assert_msg (gsl_fcmp (cov, cov_ex, RANDTOL)==0, "(1,2): %f != %f",
                 cov, cov_ex);
  cov = gsl_stats_covariance_m (results2->data, results2->stride,
                                results1->data, results1->stride,
                                iterations, mean2, mean1);
  cov_ex = cs_ex * gsl_matrix_get (sigma, 1, 0);
  ck_assert_msg (gsl_fcmp (cov, cov_ex, RANDTOL)==0, "(2,1): %f != %f",
                 cov, cov_ex);
  cov = gsl_stats_covariance_m (results2->data, results2->stride,
                                results2->data, results2->stride,
                                iterations, mean2, mean2);
  cov_ex = cs_ex * gsl_matrix_get (sigma, 1, 1);
  ck_assert_msg (gsl_fcmp (cov, cov_ex, RANDTOL)==0, "(2,2): %f != %f",
                 cov, cov_ex);

  /* Correlation: Cor[X] = correlation associated with Sigma */
  cor_ex = (sqrt (1/gsl_matrix_get (sigma, 1, 1)) *
            gsl_matrix_get (sigma, 1, 0) *
            sqrt (1/gsl_matrix_get (sigma, 0, 0)));
  cor = gsl_stats_correlation (results1->data, results1->stride, results2->data,
                               results2->stride, iterations);
  ck_assert_msg (gsl_fcmp (cor, cor_ex, RANDTOL)==0, "%f != %f", cor, cor_ex);

  gsl_matrix_free (sigma);
  gsl_vector_free (result);
  gsl_vector_free (results1);
  gsl_vector_free (results2);
  gsl_vector_free (mu);
}
END_TEST



START_TEST(test_ran_multivariate_t_univariate)
{
  gsl_matrix *sigma;
  gsl_vector *mu, *result, *results;
  size_t iterations = 100000;
  double nu = 10.0;
  double mean, var, skew, ex_kurtosis;
  int err;

  sigma = safe_matrix_alloc (1, 1);
  result = safe_vector_alloc (1);
  mu = safe_vector_alloc (1);
  gsl_vector_set_zero (mu);
  results = safe_vector_alloc (iterations);
  gsl_matrix_set (sigma, 0, 0, 1.0);
  for (size_t i=0; i<iterations; i++)
    {
      err = ran_multivariate_t (rng, mu, sigma, nu, result);
      ck_assert (err == 0);
      gsl_vector_set (results, i,
                      gsl_vector_get (result, 0));
    }
  mean = gsl_stats_mean (results->data, results->stride, iterations);
  ck_assert_msg (gsl_fcmp (mean, -RANDTOL, RANDTOL)==1, "%f", mean);
  ck_assert_msg (gsl_fcmp (mean, RANDTOL, RANDTOL)==-1, "%f", mean);
  var = gsl_stats_variance (results->data, results->stride, iterations);
  ck_assert_msg (gsl_fcmp (var, nu/(nu-2.0), RANDTOL)==0, "%f", var);
  skew = gsl_stats_skew (results->data, results->stride, iterations);
  ck_assert_msg (gsl_fcmp (skew, -RANDTOL, RANDTOL)==1, "%f", skew);
  ck_assert_msg (gsl_fcmp (skew, RANDTOL, RANDTOL)==-1, "%f", skew);
  ex_kurtosis = gsl_stats_kurtosis (results->data, results->stride, iterations);
  ck_assert_msg (gsl_fcmp (ex_kurtosis, 6.0/(nu-4.0), RANDTOL)==0, "%f != %f",
                 ex_kurtosis, 6.0/(nu-4.0));
  gsl_matrix_free (sigma);
  gsl_vector_free (result);
  gsl_vector_free (results);
  gsl_vector_free (mu);
}
END_TEST


START_TEST(test_ran_multivariate_t_empty_sigma)
{
  gsl_matrix *sigma;
  gsl_vector *mu, *result;
  double nu = 3.0;
  int err;

  result = safe_vector_alloc (2);
  mu = safe_vector_alloc (2);
  gsl_vector_set_zero (mu);
  sigma = NULL;
  err = ran_multivariate_t (rng, mu, sigma, nu, result);
  ck_assert (err == -1);
  gsl_vector_free (result);
}
END_TEST


START_TEST(test_ran_multivariate_t_invalid_nu)
{
  gsl_matrix *sigma;
  gsl_vector *mu, *result;
  double nu;
  int err;

  result = safe_vector_alloc (2);
  sigma = safe_matrix_alloc (2, 2);
  gsl_matrix_set (sigma, 0, 0, 1.0);
  gsl_matrix_set (sigma, 0, 1, 0.5);
  gsl_matrix_set (sigma, 1, 0, 0.5);
  gsl_matrix_set (sigma, 1, 1, 1.0);
  mu = safe_vector_alloc (2);
  gsl_vector_set_zero (mu);

  nu = 0.0;
  err = ran_multivariate_t (rng, mu, sigma, nu, result);
  ck_assert (err == -1);

  nu = -1.0;
  err = ran_multivariate_t (rng, mu, sigma, nu, result);
  ck_assert (err == -1);
  gsl_matrix_free (sigma);
  gsl_vector_free (result);
}
END_TEST


START_TEST(test_ran_multivariate_t_invalid_sigma)
{
  gsl_matrix *sigma;
  gsl_vector *mu, *result;
  double nu;
  int err;

  result = safe_vector_alloc (2);
  sigma = safe_matrix_alloc (2, 2);
  gsl_matrix_set (sigma, 0, 0, 1.0);
  gsl_matrix_set (sigma, 0, 1, 1.0);
  gsl_matrix_set (sigma, 1, 0, 0.5);
  gsl_matrix_set (sigma, 1, 1, 1.0);
  mu = safe_vector_alloc (2);
  gsl_vector_set_zero (mu);
  err = ran_multivariate_t (rng, mu, sigma, nu, result);
  ck_assert (err == -1);
  gsl_matrix_free (sigma);

  gsl_vector_free (result);
}
END_TEST


START_TEST(test_ran_multivariate_t_pdf)
{
  gsl_vector *X, *work;
  gsl_matrix *sigma;
  double nu, result;
  int err;

  X = safe_vector_alloc (2);
  gsl_vector_set_zero (X);
  sigma = safe_matrix_alloc (2, 2);
  gsl_matrix_set_identity (sigma);
  work = safe_vector_alloc (2);
  nu = 1.0;
  err = ran_multivariate_t_pdf (X, sigma, nu, &result, work);
  ck_assert (err == 0);
  ck_assert (gsl_fcmp (result, 0.159154943091895, TOL) == 0);

  gsl_vector_set (X, 0, 1.0);
  gsl_matrix_set (sigma, 0, 1, 0.5);
  gsl_matrix_set (sigma, 1, 0, 0.5);
  nu = 2.0;
  err = ran_multivariate_t_pdf (X, sigma, nu, &result, work);
  ck_assert (err == 0);
  ck_assert (gsl_fcmp (result, 0.066159467450615, TOL) == 0);
  gsl_vector_free (X);
  gsl_matrix_free (sigma);
  gsl_vector_free (work);
}
END_TEST


START_TEST(test_ran_multivariate_t_pdf_invalid_nu)
{
  gsl_vector *X, *work;
  gsl_matrix *sigma;
  double nu, result;
  int err;

  X = safe_vector_alloc (2);
  gsl_vector_set_zero (X);
  sigma = safe_matrix_alloc (2, 2);
  gsl_matrix_set_identity (sigma);
  work = safe_vector_alloc (2);
  nu = 0.0;
  err = ran_multivariate_t_pdf (X, sigma, nu, &result, work);
  ck_assert (err == -1);
  gsl_vector_free (X);
  gsl_matrix_free (sigma);
  gsl_vector_free (work);
}
END_TEST


START_TEST(test_ran_multivariate_t_pdf_big_nu)
{
  gsl_vector *X, *work;
  gsl_matrix *sigma;
  double nu, result;
  int err;

  X = gsl_vector_alloc (2);
  gsl_vector_set_zero (X);
  sigma = gsl_matrix_alloc (2, 2);
  gsl_matrix_set_identity (sigma);
  work = gsl_vector_alloc (2);
  nu = 500.0;
  err = ran_multivariate_t_pdf (X, sigma, nu, &result, work);
  ck_assert (!gsl_isnan (result));
  gsl_vector_free (X);
  gsl_matrix_free (sigma);
  gsl_vector_free (work);
}
END_TEST


START_TEST(test_ran_multivariate_t_pdf_huge_nu)
{
  gsl_vector *X, *work;
  gsl_matrix *sigma;
  double nu, result;
  int err;

  X = gsl_vector_alloc (2);
  gsl_vector_set_zero (X);
  sigma = gsl_matrix_alloc (2, 2);
  gsl_matrix_set_identity (sigma);
  work = gsl_vector_alloc (2);
  nu = 5000.0;
  err = ran_multivariate_t_pdf (X, sigma, nu, &result, work);
  ck_assert (!gsl_isnan (result));
  gsl_vector_free (X);
  gsl_matrix_free (sigma);
  gsl_vector_free (work);
}
END_TEST


START_TEST(test_ran_multivariate_t_pdf_empty_X)
{
  gsl_vector *X, *work;
  gsl_matrix *sigma;
  double nu, result;
  int err;

  X = NULL;
  sigma = safe_matrix_alloc (2, 2);
  gsl_matrix_set_identity (sigma);
  work = safe_vector_alloc (2);
  nu = 1.0;
  err = ran_multivariate_t_pdf (X, sigma, nu, &result, work);
  ck_assert (err == -1);
  gsl_matrix_free (sigma);
}
END_TEST


START_TEST(test_ran_multivariate_t_pdf_invalid_X)
{
  gsl_vector *X, *work;
  gsl_matrix *sigma;
  double nu, result;
  int err;

  X = safe_vector_alloc (3);
  gsl_vector_set_zero (X);
  sigma = safe_matrix_alloc (2, 2);
  gsl_matrix_set_identity (sigma);
  work = safe_vector_alloc (2);
  nu = 1.0;
  err = ran_multivariate_t_pdf (X, sigma, nu, &result, work);
  ck_assert (err == -1);
  gsl_vector_free (X);
  gsl_matrix_free (sigma);
}
END_TEST


START_TEST(test_ran_multivariate_t_pdf_empty_sigma)
{
  gsl_vector *X, *work;
  gsl_matrix *sigma;
  double nu, result;
  int err;

  X = safe_vector_alloc (2);
  gsl_vector_set_zero (X);
  sigma = NULL;
  work = safe_vector_alloc (2);
  nu = 1.0;
  err = ran_multivariate_t_pdf (X, sigma, nu, &result, work);
  ck_assert (err == -1);
  gsl_vector_free (X);
}
END_TEST


START_TEST(test_ran_multivariate_t_pdf_invalid_sigma)
{
  gsl_vector *X, *work;
  gsl_matrix *sigma;
  double nu, result;
  int err;

  X = safe_vector_alloc (2);
  gsl_vector_set_zero (X);
  sigma = safe_matrix_alloc (2, 2);
  gsl_matrix_set_identity (sigma);
  gsl_matrix_set (sigma, 0, 1, 0.5);
  work = safe_vector_alloc (2);
  nu = 1.0;
  err = ran_multivariate_t_pdf (X, sigma, nu, &result, work);
  ck_assert (err == -1);
  gsl_vector_free (X);
  gsl_matrix_free (sigma);
}
END_TEST


Suite *
mvt_suite (void)
{
  Suite *s;
  TCase *tc_mvtrnd;
  TCase *tc_mvtpdf;

  s = suite_create ("mvt");

  tc_mvtrnd = tcase_create ("Multivariate t-distribution random number");
  tcase_add_test (tc_mvtrnd, test_ran_multivariate_t);
  tcase_add_test (tc_mvtrnd, test_ran_multivariate_t_univariate);
  tcase_add_test (tc_mvtrnd, test_ran_multivariate_t_empty_sigma);
  tcase_add_test (tc_mvtrnd, test_ran_multivariate_t_invalid_nu);
  tcase_add_test (tc_mvtrnd, test_ran_multivariate_t_invalid_sigma);
  suite_add_tcase (s, tc_mvtrnd);

  tc_mvtpdf = tcase_create ("Multivariate t-distribution PDF");
  tcase_add_test (tc_mvtrnd, test_ran_multivariate_t_pdf);
  tcase_add_test (tc_mvtrnd, test_ran_multivariate_t_pdf_invalid_nu);
  tcase_add_test (tc_mvtrnd, test_ran_multivariate_t_pdf_big_nu);
  tcase_add_test (tc_mvtrnd, test_ran_multivariate_t_pdf_huge_nu);
  tcase_add_test (tc_mvtrnd, test_ran_multivariate_t_pdf_invalid_X);
  tcase_add_test (tc_mvtrnd, test_ran_multivariate_t_pdf_invalid_sigma);
  suite_add_tcase (s, tc_mvtpdf);

  return s;
}


int main (void)
{
  int n_fail;
  Suite *s;
  SRunner *sr;
  const gsl_rng_type *rng_type;

  /* Random number generator setup */
  gsl_rng_env_setup();
  rng_type = gsl_rng_default;
  rng = gsl_rng_alloc (rng_type);

  s = mvt_suite ();
  sr = srunner_create (s);

  srunner_run_all (sr, CK_NORMAL);
  n_fail = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
