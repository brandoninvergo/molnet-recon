/* 
 * helper_funcs.h --- 
 * 
 * Copyright (C) 2017, 2018, 2019 Brandon Invergo <invergo@ebi.ac.uk>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef HELPER_FUNCS_H
#define HELPER_FUNCS_H

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_math.h>
#include <stdbool.h>

bool matrix_has_nan (gsl_matrix *M);
bool matrix_has_inf (gsl_matrix *M);
bool matrix_approx_equal (gsl_matrix *A, gsl_matrix *B, double tol);
bool vector_has_nan (gsl_vector *V);
bool vector_has_inf (gsl_vector *V);
bool vector_approx_equal (gsl_vector *A, gsl_vector *B, double tol);
void print_matrix (gsl_matrix *M);
void print_matrix_int (gsl_matrix_int *M);
void print_vector (gsl_vector *v);

#endif
