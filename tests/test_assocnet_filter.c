/* 
 * test_assocnet.c --- 
 * 
 * Copyright (C) 2017, 2019 Brandon Invergo <invergo@ebi.ac.uk>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <check.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_math.h>
#include "helper_funcs.h"

#include "../src/assocnet_filter.h"
#include "../src/safe_alloc.h"

#define TOL 1e-6


START_TEST(test_scale_matrix_minmax)
{
  gsl_matrix_view cor_m, out_m_exp;
  gsl_matrix *out_m;
  double cor[] = {1.0000000, -0.9343532, 0.8922686, 0.6067448,
                  -0.9343532, 1.0000000, -0.8533103, -0.7978025,
                  0.8922686, -0.8533103, 1.0000000, 0.7834077,
                  0.6067448, -0.7978025, 0.7834077, 1.0000000};
  cor_m = gsl_matrix_view_array (cor, 4, 4);
  double out_m_exp_dat[] = {1.0000000, 0.0000000, 0.9443062, 0.7966994,
                            0.0000000, 1.0000000, 0.0418966, 0.0705924,
                            0.9443062, 0.0418966, 1.0000000, 0.8880286,
                            0.7966994, 0.0705924, 0.8880286, 1.0000000};
  out_m_exp = gsl_matrix_view_array (out_m_exp_dat, 4, 4);
  out_m = safe_matrix_alloc (4, 4);
  scale_matrix_minmax (&cor_m.matrix, out_m, true);
  puts ("scale_matrix_minmax");
  print_matrix (out_m);
  puts ("");
  ck_assert (matrix_approx_equal (out_m, &out_m_exp.matrix, TOL));
  gsl_matrix_free (out_m);
}
END_TEST


START_TEST(test_scale_matrix_minmax_no_diag)
{
  gsl_matrix_view cor_m, out_m_exp;
  gsl_matrix *out_m;
  double cor[] = {1.0000000, -0.9343532, 0.8922686, 0.6067448,
                  -0.9343532, 1.0000000, -0.8533103, -0.7978025,
                  0.8922686, -0.8533103, 1.0000000, 0.7834077,
                  0.6067448, -0.7978025, 0.7834077, 1.0000000};
  cor_m = gsl_matrix_view_array (cor, 4, 4);
  double out_m_exp_dat[] = {1.0000000, 0.0000000, 1.0000000, 0.8436876,
                            0.0000000, 1.0000000, 0.0443676, 0.0747558,
                            1.0000000, 0.0443676, 1.0000000, 0.9404032,
                            0.8436876, 0.0747558, 0.9404032, 1.0000000};
  out_m_exp = gsl_matrix_view_array (out_m_exp_dat, 4, 4);
  out_m = safe_matrix_alloc (4, 4);
  scale_matrix_minmax (&cor_m.matrix, out_m, false);
  puts ("scale_matrix_minmax");
  print_matrix (out_m);
  puts ("");
  ck_assert (matrix_approx_equal (out_m, &out_m_exp.matrix, TOL));
  gsl_matrix_free (out_m);
}
END_TEST


START_TEST(test_scale_matrix_abs)
{
  gsl_matrix_view cor_m, out_m_exp;
  gsl_matrix *out_m;
  double cor[] = {1.0000000, -0.9343532, 0.8922686, 0.6067448,
                  -0.9343532, 1.0000000, -0.8533103, -0.7978025,
                  0.8922686, -0.8533103, 1.0000000, 0.7834077,
                  0.6067448, -0.7978025, 0.7834077, 1.0000000};
  cor_m = gsl_matrix_view_array (cor, 4, 4);
  double out_m_exp_dat[] = {1.0000000, 0.9343532, 0.8922686, 0.6067448,
                            0.9343532, 1.0000000, 0.8533103, 0.7978025,
                            0.8922686, 0.8533103, 1.0000000, 0.7834077,
                            0.6067448, 0.7978025, 0.7834077, 1.0000000};
  out_m_exp = gsl_matrix_view_array (out_m_exp_dat, 4, 4);
  out_m = safe_matrix_alloc (4, 4);
  scale_matrix_abs (&cor_m.matrix, out_m, true);
  puts ("scale_matrix_abs");
  print_matrix (out_m);
  puts ("");
  ck_assert (matrix_approx_equal (out_m, &out_m_exp.matrix, TOL));
  gsl_matrix_free (out_m);
}
END_TEST


START_TEST(test_remove_diagonal)
{
  gsl_matrix_view cor_m, out_m_exp;
  gsl_matrix *out_m;
  double cor[] = {1.0000000, -0.9343532, 0.8922686, 0.6067448,
                  -0.9343532, 1.0000000, -0.8533103, -0.7978025,
                  0.8922686, -0.8533103, 1.0000000, 0.7834077,
                  0.6067448, -0.7978025, 0.7834077, 1.0000000};
  cor_m = gsl_matrix_view_array (cor, 4, 4);
  double out_m_exp_dat[] = {0.0000000, -0.9343532, 0.8922686, 0.6067448,
                            -0.9343532, 0.0000000, -0.8533103, -0.7978025,
                            0.8922686, -0.8533103, 0.0000000, 0.7834077,
                            0.6067448, -0.7978025, 0.7834077, 0.0000000};
  out_m_exp = gsl_matrix_view_array (out_m_exp_dat, 4, 4);
  out_m = safe_matrix_alloc (4, 4);
  puts ("remove_diagonal");
  remove_diagonal (&cor_m.matrix, out_m);
  print_matrix (out_m);
  puts ("");
  ck_assert (matrix_approx_equal (out_m, &out_m_exp.matrix, TOL));
  gsl_matrix_free (out_m);
}
END_TEST


START_TEST(test_threshold_matrix)
{
  gsl_matrix_view scaled_cor_m, out_m_exp;
  gsl_matrix *out_m;
  double alpha = 0.5;
  double scaled_cor[] = {0.0000000, 0.0000000, 0.9443062, 0.7966994,
                         0.0000000, 0.0000000, 0.0418966, 0.0705924,
                         0.9443062, 0.0418966, 0.0000000, 0.8880286,
                         0.7966994, 0.0705924, 0.8880286, 0.0000000};
  scaled_cor_m = gsl_matrix_view_array (scaled_cor, 4, 4);
  double out_m_exp_dat[] = {0.0000000, 0.0000000, 0.9443062, 0.7966994,
                            0.0000000, 0.0000000, 0.0000000, 0.0705924,
                            0.9443062, 0.0000000, 0.0000000, 0.8880286,
                            0.7966994, 0.0705924, 0.8880286, 0.0000000};
  out_m_exp = gsl_matrix_view_array (out_m_exp_dat, 4, 4);
  out_m = safe_matrix_alloc (4, 4);
  threshold_matrix (&scaled_cor_m.matrix, out_m, alpha);
  puts ("threshold_matrix");
  print_matrix (out_m);
  puts ("");
  ck_assert (matrix_approx_equal (out_m, &out_m_exp.matrix, TOL));
  gsl_matrix_free (out_m);
}
END_TEST


START_TEST(test_deconvolve_matrix)
{
  gsl_matrix_view dat_m, deconv_exp;
  gsl_matrix *deconv;
  double beta = 0.99;
  double dat[] = {0.0000000, 0.0000000, 0.9443062, 0.7966994,
                  0.0000000, 0.0000000, 0.0418966, 0.0705924,
                  0.9443062, 0.0418966, 0.0000000, 0.8880286,
                  0.7966994, 0.0705924, 0.8880286, 0.0000000};;
  dat_m = gsl_matrix_view_array (dat, 4, 4);
  double deconv_exp_dat[] = {-3.940560e-01, -2.353023e-02, 5.278544e-01, 3.322858e-01,
                             -2.353023e-02, -1.758189e-03, 1.514791e-02, 3.924681e-02,
                             5.278544e-01, 1.514791e-02, -4.661097e-01, 4.545431e-01,
                             3.322858e-01, 3.924681e-02, 4.545431e-01, -3.465339e-01};
  deconv_exp = gsl_matrix_view_array (deconv_exp_dat, 4, 4);
  deconv = safe_matrix_alloc (4, 4);
  deconvolve_matrix (&dat_m.matrix, deconv, beta);
  puts ("deconvolve_matrix");
  print_matrix (deconv);
  puts ("");
  ck_assert (matrix_approx_equal (deconv, &deconv_exp.matrix, TOL));
  gsl_matrix_free (deconv);
}
END_TEST


START_TEST(test_filter_network_convolution)
{
  gsl_matrix_view cor_m, filtered_exp;
  gsl_matrix *filtered;
  struct arguments args;
  args.deconv_alpha = 1.0;
  args.deconv_beta = 0.99;
  args.pre_scale = SCALE_METHOD_MINMAX;
  args.post_scale = SCALE_METHOD_MINMAX;
  args.observed_only = false;
  args.scale_diag = true;
  double cor[] = {1.0000000, -0.9343532, 0.8922686, 0.6067448,
                  -0.9343532, 1.0000000, -0.8533103, -0.7978025,
                  0.8922686, -0.8533103, 1.0000000, 0.7834077,
                  0.6067448, -0.7978025, 0.7834077, 1.0000000};
  cor_m = gsl_matrix_view_array (cor, 4, 4);
  double filtered_exp_dat[] = {0.0724912, 0.4452670, 1.0000000, 0.8032439,
                               0.4452670, 0.4671713, 0.4841801, 0.5084253,
                               1.0000000, 0.4841801, 0.0000000, 0.9262436,
                               0.8032439, 0.5084253, 0.9262436, 0.1203019};
  filtered_exp = gsl_matrix_view_array (filtered_exp_dat, 4, 4);
  filtered = safe_matrix_alloc (4, 4);
  filter_network_deconvolution (&cor_m.matrix, filtered, args);
  puts ("filter_network_deconvolution");
  print_matrix (filtered);
  puts ("");
  ck_assert (matrix_approx_equal (filtered, &filtered_exp.matrix, TOL));
  gsl_matrix_free (filtered);
}
END_TEST


START_TEST(test_filter_network_convolution_obs_only)
{
  gsl_matrix_view cor_m, filtered_exp;
  gsl_matrix *filtered;
  struct arguments args;
  args.deconv_alpha = 1.0;
  args.deconv_beta = 0.99;
  args.pre_scale = SCALE_METHOD_MINMAX;
  args.post_scale = SCALE_METHOD_MINMAX;
  args.observed_only = true;
  args.scale_diag = true;
  double cor[] = {1.0000000, -0.9343532, 0.8922686, 0.6067448,
                  -0.9343532, 1.0000000, -0.8533103, -0.7978025,
                  0.8922686, -0.8533103, 1.0000000, 0.7834077,
                  0.6067448, -0.7978025, 0.7834077, 1.0000000};
  cor_m = gsl_matrix_view_array (cor, 4, 4);
  double filtered_exp_dat[] = {0.0000000, 0.0000000, 1.0000000, 0.8032439,
                               0.0000000, 0.0000000, 0.4841801, 0.5084253,
                               1.0000000, 0.4841801, 0.0000000, 0.9262436,
                               0.8032439, 0.5084253, 0.9262436, 0.0000000};
  filtered_exp = gsl_matrix_view_array (filtered_exp_dat, 4, 4);
  filtered = safe_matrix_alloc (4, 4);
  filter_network_deconvolution (&cor_m.matrix, filtered, args);
  puts ("filter_network_deconvolution --observed-only");
  print_matrix (filtered);
  puts ("");
  ck_assert (matrix_approx_equal (filtered, &filtered_exp.matrix, TOL));
  gsl_matrix_free (filtered);
}
END_TEST


Suite *
assocnet_suite (void)
{
  Suite *s;
  TCase *tc_deconv;

  s = suite_create ("assocnet-filter");

  tc_deconv = tcase_create ("Deconvolution");
  tcase_add_test (tc_deconv, test_scale_matrix_minmax);
  tcase_add_test (tc_deconv, test_scale_matrix_minmax_no_diag);
  tcase_add_test (tc_deconv, test_scale_matrix_abs);
  tcase_add_test (tc_deconv, test_remove_diagonal);
  tcase_add_test (tc_deconv, test_threshold_matrix);
  tcase_add_test (tc_deconv, test_deconvolve_matrix);
  tcase_add_test (tc_deconv, test_filter_network_convolution);
  tcase_add_test (tc_deconv, test_filter_network_convolution_obs_only);
  suite_add_tcase (s, tc_deconv);

  return s;
}


int main (void)
{
  int n_fail;
  Suite *s;
  SRunner *sr;

  s = assocnet_suite ();
  sr = srunner_create (s);

  srunner_run_all (sr, CK_NORMAL);
  n_fail = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
