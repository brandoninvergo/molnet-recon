/* 
 * test_assocnet.c --- 
 * 
 * Copyright (C) 2017, 2018, 2019 Brandon Invergo <invergo@ebi.ac.uk>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <check.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_int.h>
#include <gsl/gsl_math.h>
#include "helper_funcs.h"

#include "../src/assocnet.h"
#include "../src/safe_alloc.h"

#define TOL 1e-6


START_TEST(test_calc_cov_matrix)
{
  gsl_matrix_view dat_m, cov_exp;
  gsl_matrix *dev, *cov;
  double dat[] = {0.1, 0.3, 0.2, 0.7,
                  0.5, 0.2, 0.3, 0.1,
                  0.8, 0.9, 0.2, 0.3};
  dat_m = gsl_matrix_view_array (dat, 3, 4);
  double cov_exp_dat[] = {0.06916667, -0.03916667, -0.03833333,
                          -0.03916667, 0.02916667, 0.02166667,
                          -0.03833333, 0.02166667, 0.12333333};
  cov_exp = gsl_matrix_view_array (cov_exp_dat, 3, 3);
  cov = safe_matrix_alloc (3, 3);
  dev = safe_matrix_alloc (3, 4);
  calc_cov_matrix (&dat_m.matrix, dev, cov);
  ck_assert (matrix_approx_equal (&cov_exp.matrix, cov, TOL));
  gsl_matrix_free (cov);
  gsl_matrix_free (dev);
}
END_TEST


START_TEST(test_calc_corr_matrix_pearson)
{
  gsl_matrix_view dat_m, corr_exp;
  gsl_matrix *corr;
  double dat[] = {0.1, 0.3, 0.2, 0.7,
                  0.5, 0.2, 0.3, 0.1,
                  0.8, 0.9, 0.2, 0.3};
  dat_m = gsl_matrix_view_array (dat, 3, 4);
  double corr_exp_dat[] = {1.0, -0.8720167, -0.4150381,
                           -0.8720167, 1.0, 0.3612505,
                           -0.4150381, 0.3612505, 1.0};
  corr_exp = gsl_matrix_view_array (corr_exp_dat, 3, 3);
  corr = safe_matrix_alloc (3, 3);
  calc_corr_matrix_pearson (&dat_m.matrix, corr);
  ck_assert (matrix_approx_equal (corr, &corr_exp.matrix, TOL));
  gsl_matrix_free (corr);
}
END_TEST


START_TEST(test_unbiased_corr)
{
  gsl_matrix_view dat_m, corr_exp;
  gsl_matrix *corr;
  double dat[] = {0.1, 0.3, 0.2, 0.7,
                  0.5, 0.2, 0.3, 0.1,
                  0.8, 0.9, 0.2, 0.3};
  dat_m = gsl_matrix_view_array (dat, 3, 4);
  double corr_exp_dat[] = {1.0, -0.9764786, -0.5868106,
                           -0.9764786, 1.0, 0.5183038,
                           -0.5868106, 0.5183038, 1.0};
  corr_exp = gsl_matrix_view_array (corr_exp_dat, 3, 3);
  corr = safe_matrix_alloc (3, 3);
  calc_corr_matrix_pearson (&dat_m.matrix, corr);
  unbiased_corr (corr, 4);
  ck_assert (matrix_approx_equal (corr, &corr_exp.matrix, TOL));
  gsl_matrix_free (corr);
}
END_TEST


START_TEST(test_rank_matrix_rows)
{
  gsl_matrix_view dat_m, ranks_exp;
  gsl_matrix *ranks;
  double dat[] = {0.1, 0.3, 0.2, 0.7,
                  0.5, 0.2, 0.3, 0.1,
                  0.8, 0.9, 0.2, 0.3};
  dat_m = gsl_matrix_view_array (dat, 3, 4);
  double ranks_exp_dat[] = {1.0, 3.0, 2.0, 4.0,
                            4.0, 2.0, 3.0, 1.0,
                            3.0, 4.0, 1.0, 2.0};
  ranks_exp = gsl_matrix_view_array (ranks_exp_dat, 3, 4);
  ranks = safe_matrix_alloc (3, 4);
  rank_matrix_rows (&dat_m.matrix, ranks);
  ck_assert (matrix_approx_equal (ranks, &ranks_exp.matrix, TOL));
  gsl_matrix_free (ranks);
}
END_TEST


START_TEST(test_calc_corr_matrix_spearman)
{
  gsl_matrix_view dat_m, corr_exp;
  gsl_matrix *corr;
  double dat[] = {0.1, 0.3, 0.2, 0.7,
                  0.5, 0.2, 0.3, 0.1,
                  0.8, 0.9, 0.2, 0.3};
  dat_m = gsl_matrix_view_array (dat, 3, 4);
  double corr_exp_dat[] = {1.0, -1.0, 0,
                           -1.0, 1.0, 0.0,
                           0.0, 0.0, 1.0};
  corr_exp = gsl_matrix_view_array (corr_exp_dat, 3, 3);
  corr = safe_matrix_alloc (3, 3);
  calc_corr_matrix_spearman (&dat_m.matrix, corr);
  ck_assert (matrix_approx_equal (corr, &corr_exp.matrix, TOL));
  gsl_matrix_free (corr);
}
END_TEST


START_TEST(test_contingency_table)
{
  gsl_matrix_int_view dat_m;
  gsl_matrix_view counts_exp;
  gsl_matrix *counts;
  int dat[] = {1, 2, 1, 3,
               2, 3, 3, 1,
               1, 2, 1, 1};
  dat_m = gsl_matrix_int_view_array (dat, 3, 4);
  double counts_exp_dat[] = {0.0, 1.0, 1.0,
                             0.0, 0.0, 1.0,
                             1.0, 0.0, 0.0};
  counts_exp = gsl_matrix_view_array (counts_exp_dat, 3, 3);
  counts = safe_matrix_alloc (3, 3);
  contingency_table (&dat_m.matrix, counts, 0, 1, 1);
  ck_assert (matrix_approx_equal (counts, &counts_exp.matrix, TOL));
  gsl_matrix_free (counts);
}
END_TEST


START_TEST(test_calc_joint_probs)
{
  gsl_matrix_int_view dat_m;
  gsl_matrix_view joint_probs_exp;
  gsl_matrix *counts, *joint_probs;
  int dat[] = {1, 2, 1, 3,
               2, 3, 3, 1,
               1, 2, 1, 1};
  dat_m = gsl_matrix_int_view_array (dat, 3, 4);
  double joint_probs_exp_dat[] = {0.0, 0.25, 0.25,
                                  0.0,  0.0, 0.25,
                                  0.25, 0.0, 0.0};
  joint_probs_exp = gsl_matrix_view_array (joint_probs_exp_dat, 3, 3);
  counts = safe_matrix_alloc (3, 3);
  joint_probs = safe_matrix_alloc (3, 3);
  contingency_table (&dat_m.matrix, counts, 0, 1, 1);
  gsl_matrix_memcpy (joint_probs, counts);
  gsl_matrix_scale (joint_probs, 1.0/4.0);
  ck_assert (matrix_approx_equal (joint_probs, &joint_probs_exp.matrix, TOL));
  gsl_matrix_free (counts);
  gsl_matrix_free (joint_probs);
}
END_TEST


START_TEST(test_calc_marg_probs)
{
  gsl_matrix_int_view dat_m;
  gsl_vector_view marg_probs1_exp, marg_probs2_exp;
  gsl_matrix *counts, *joint_probs;
  gsl_vector *marg_probs1, *marg_probs2;
  int dat[] = {1, 2, 1, 3,
               2, 3, 3, 1,
               1, 2, 1, 1};
  dat_m = gsl_matrix_int_view_array (dat, 3, 4);
  double marg_probs1_exp_dat[] = {0.5, 0.25, 0.25};
  double marg_probs2_exp_dat[] = {0.25, 0.25, 0.5};
  marg_probs1_exp = gsl_vector_view_array (marg_probs1_exp_dat, 3);
  marg_probs2_exp = gsl_vector_view_array (marg_probs2_exp_dat, 3);
  counts = safe_matrix_alloc (3, 3);
  joint_probs = safe_matrix_alloc (3, 3);
  marg_probs1 = safe_vector_alloc (3);
  marg_probs2 = safe_vector_alloc (3);
  contingency_table (&dat_m.matrix, counts, 0, 1, 1);
  gsl_matrix_memcpy (joint_probs, counts);
  gsl_matrix_scale (joint_probs, 1.0/4.0);
  calc_marg_probs (joint_probs, marg_probs1, marg_probs2);
  ck_assert (vector_approx_equal (marg_probs1, &marg_probs1_exp.vector, TOL));
  ck_assert (vector_approx_equal (marg_probs2, &marg_probs2_exp.vector, TOL));
  gsl_matrix_free (counts);
  gsl_matrix_free (joint_probs);
  gsl_vector_free (marg_probs1);
  gsl_vector_free (marg_probs2);
}
END_TEST


START_TEST(test_calc_entropy)
{
  gsl_vector_view marg_probs_v;
  double entropy_exp = 1.039721;
  double entropy;
  double marg_probs[] = {0.5, 0.25, 0.25};
  marg_probs_v = gsl_vector_view_array (marg_probs, 3);
  entropy = calc_entropy (&marg_probs_v.vector);
  ck_assert (gsl_fcmp (entropy, entropy_exp, TOL) == 0);
}
END_TEST


START_TEST(test_calc_joint_entropy)
{
  gsl_matrix_view joint_probs_m;
  double entropy_exp = 1.386294;
  double entropy;
  double joint_probs[] = {0.0, 0.25, 0.25,
                          0.0,  0.0, 0.25,
                          0.25, 0.0, 0.0};
  joint_probs_m = gsl_matrix_view_array (joint_probs, 3, 3);
  entropy = calc_joint_entropy (&joint_probs_m.matrix);
  ck_assert (gsl_fcmp (entropy, entropy_exp, TOL) == 0);
}
END_TEST


START_TEST(test_calc_mi_matrix)
{
  gsl_matrix_int_view dat_m;
  gsl_matrix_view mi_exp;
  gsl_matrix *mi;
  int dat[] = {1, 2, 1, 3,
               2, 3, 3, 1,
               1, 2, 1, 1};
  dat_m = gsl_matrix_int_view_array (dat, 3, 4);
  double mi_exp_dat[] = {1.039721, 0.6931472, 0.5623351,
                         0.6931472, 1.039721, 0.2157616,
                         0.5623351, 0.2157616, 0.5623351};
  mi_exp = gsl_matrix_view_array (mi_exp_dat, 3, 3);
  mi = safe_matrix_alloc (3, 3);
  calc_mi_matrix (&dat_m.matrix, mi);
  puts ("");
  print_matrix (mi);
  ck_assert (matrix_approx_equal (mi, &mi_exp.matrix, TOL));
  gsl_matrix_free (mi);
}
END_TEST


Suite *
assocnet_suite (void)
{
  Suite *s;
  TCase *tc_pearson;
  TCase *tc_spearman;
  TCase *tc_mi;

  s = suite_create ("assocnet");

  tc_pearson = tcase_create ("Pearson's Correlation");
  tcase_add_test (tc_pearson, test_calc_cov_matrix);
  tcase_add_test (tc_pearson, test_calc_corr_matrix_pearson);
  suite_add_tcase (s, tc_pearson);

  tc_spearman = tcase_create ("Spearman's Correlation");
  tcase_add_test (tc_spearman, test_rank_matrix_rows);
  tcase_add_test (tc_spearman, test_calc_corr_matrix_spearman);
  suite_add_tcase (s, tc_spearman);

  tc_mi = tcase_create ("Mutual Information");
  tcase_add_test (tc_mi, test_contingency_table);
  tcase_add_test (tc_mi, test_calc_joint_probs);
  tcase_add_test (tc_mi, test_calc_marg_probs);
  tcase_add_test (tc_mi, test_calc_entropy);
  tcase_add_test (tc_mi, test_calc_joint_entropy);
  tcase_add_test (tc_mi, test_calc_mi_matrix);
  suite_add_tcase (s, tc_mi);

  return s;
}


int main (void)
{
  int n_fail;
  Suite *s;
  SRunner *sr;

  s = assocnet_suite ();
  sr = srunner_create (s);

  srunner_run_all (sr, CK_NORMAL);
  n_fail = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
