/* 
 * test_mra.c --- 
 * 
 * Copyright (C) 2018 Brandon M. Invergo <invergo@ebi.ac.uk>
 * 
 * Author: Brandon M. Invergo <invergo@ebi.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <check.h>

#include "helper_funcs.h"

#include "../src/mra.h"
#include "../src/safe_alloc.h"

#define KHOLO_TOL 1e-1


START_TEST(test_mra_4a)
{
  /* Fig. 4 MAPK test data 'a' from Kholodenko et al. 2002.  The
     results are provided at very low precision */
  gsl_matrix_view resp;
  gsl_matrix_view int_map;
  gsl_matrix *int_map_test;
  size_t i, j;
  double rounded;
  double resp_dat[] = {-7.4,   6.9,  3.7,
                       -6.2,  -3.1,  8.9,
                       -12.7, -6.3, -3.4};
  double int_map_dat[] = {-1.0,  0.0, -1.1,
                           1.9, -1.0, -0.6,
                           -0.0,  2.0, -1.0};
  resp = gsl_matrix_view_array (resp_dat, 3, 3);
  int_map = gsl_matrix_view_array (int_map_dat, 3, 3);
  int_map_test = safe_matrix_alloc (3, 3);
  mra (&resp.matrix, int_map_test);
  for (i=0; i<int_map_test->size1; i++)
    {
      for (j=0; j<int_map_test->size2; j++)
        {
          rounded = round (10.0 * gsl_matrix_get (int_map_test, i, j))/10.0;
          gsl_matrix_set (int_map_test, i, j, rounded);
        }
    }
  ck_assert (matrix_approx_equal (int_map_test, &int_map.matrix, KHOLO_TOL));
  gsl_matrix_free (int_map_test);
}
END_TEST


START_TEST(test_mra_4b)
{
  /* Fig. 4 MAPK test data 'b' from Kholodenko et al. 2002 */
  gsl_matrix_view resp;
  gsl_matrix_view int_map;
  gsl_matrix *int_map_test;
  size_t i, j;
  double rounded;
  double resp_dat[] = {-55.5,  42.9,  23.8,
                       -44.8, -21.1,  56.6,
                       -85.7, -42.4, -22.5};
  double int_map_dat[] = {-1.0,  0.0, -1.0,
                           1.8, -1.0, -0.6,
                           -0.0,  2.0, -1.0};
  resp = gsl_matrix_view_array (resp_dat, 3, 3);
  int_map = gsl_matrix_view_array (int_map_dat, 3, 3);
  int_map_test = safe_matrix_alloc (3, 3);
  mra (&resp.matrix, int_map_test);
  for (i=0; i<int_map_test->size1; i++)
    {
      for (j=0; j<int_map_test->size2; j++)
        {
          rounded = round (10.0 * gsl_matrix_get (int_map_test, i, j))/10.0;
          gsl_matrix_set (int_map_test, i, j, rounded);
        }
    }
  ck_assert (matrix_approx_equal (int_map_test, &int_map.matrix, KHOLO_TOL));
  gsl_matrix_free (int_map_test);
}
END_TEST


START_TEST(test_mra_4c)
{
  /* Fig. 4 MAPK test data 'c' from Kholodenko et al. 2002 */
  gsl_matrix_view resp;
  gsl_matrix_view int_map;
  gsl_matrix *int_map_test;
  size_t i, j;
  double rounded;
  double resp_dat[] = {-7.4, -7.0, -3.7,
                       -6.2,  3.1, -8.9,
                       -12.7, 6.3,  3.4};
  double int_map_dat[] = {-1.0,  -0.0, -1.1,
                           1.8, -1.0, -0.6,
                           0.0,  2.0, -1.0};
  resp = gsl_matrix_view_array (resp_dat, 3, 3);
  int_map = gsl_matrix_view_array (int_map_dat, 3, 3);
  int_map_test = safe_matrix_alloc (3, 3);
  mra (&resp.matrix, int_map_test);
  for (i=0; i<int_map_test->size1; i++)
    {
      for (j=0; j<int_map_test->size2; j++)
        {
          rounded = round (10.0 * gsl_matrix_get (int_map_test, i, j))/10.0;
          gsl_matrix_set (int_map_test, i, j, rounded);
        }
    }
  ck_assert (matrix_approx_equal (int_map_test, &int_map.matrix, KHOLO_TOL));
  gsl_matrix_free (int_map_test);
}
END_TEST


START_TEST(test_mra_4d)
{
  /* Fig. 4 MAPK test data 'd' from Kholodenko et al. 2002 */
  gsl_matrix_view resp;
  gsl_matrix_view int_map;
  gsl_matrix *int_map_test;
  size_t i, j;
  double rounded;
  double resp_dat[] = {-55.5, -46.3, -25.0,
                       -44.8,  20.3, -56.8,
                       -85.7,  39.4,  21.8};
  double int_map_dat[] = {-1.0, -0.0, -1.2,
                           1.8, -1.0, -0.6,
                          -0.0,  1.9, -1.0};
  resp = gsl_matrix_view_array (resp_dat, 3, 3);
  int_map = gsl_matrix_view_array (int_map_dat, 3, 3);
  int_map_test = safe_matrix_alloc (3, 3);
  mra (&resp.matrix, int_map_test);
  for (i=0; i<int_map_test->size1; i++)
    {
      for (j=0; j<int_map_test->size2; j++)
        {
          rounded = round (10.0 * gsl_matrix_get (int_map_test, i, j))/10.0;
          gsl_matrix_set (int_map_test, i, j, rounded);
        }
    }
  ck_assert (matrix_approx_equal (int_map_test, &int_map.matrix, KHOLO_TOL));
  gsl_matrix_free (int_map_test);
}
END_TEST


START_TEST(test_mra_7a)
{
  /* Fig. 7 gene network test data 'a' from Kholodenko et al. 2002 */
  gsl_matrix_view resp;
  gsl_matrix_view int_map;
  gsl_matrix *int_map_test;
  size_t i, j;
  double rounded;
  double resp_dat[] = {-34.8,  15.9,  -2.9,  -7.6,
                         2.2, -39.9,  -6.3, -16.1,
                        14.7, -27.7, -38.4,  -6.3,
                         4.2,  -9.1, -12.8, -37.5};
  double int_map_dat[] = {-1.0, -0.5,  0.0,  0.4,
                          -0.0, -1.0,  0.0,  0.4,
                          -0.4,  0.5, -1.0,  0.0,
                           0.0,  0.0,  0.3, -1.0};
  resp = gsl_matrix_view_array (resp_dat, 4, 4);
  int_map = gsl_matrix_view_array (int_map_dat, 4, 4);
  int_map_test = safe_matrix_alloc (4, 4);
  mra (&resp.matrix, int_map_test);
  for (i=0; i<int_map_test->size1; i++)
    {
      for (j=0; j<int_map_test->size2; j++)
        {
          rounded = round (10.0 * gsl_matrix_get (int_map_test, i, j))/10.0;
          gsl_matrix_set (int_map_test, i, j, rounded);
        }
    }
  ck_assert (matrix_approx_equal (int_map_test, &int_map.matrix, KHOLO_TOL));
  gsl_matrix_free (int_map_test);
}
END_TEST


START_TEST(test_mra_7b)
{
  /* Fig. 7 gene network test data 'b' from Kholodenko et al. 2002 */
  gsl_matrix_view resp;
  gsl_matrix_view int_map;
  gsl_matrix *int_map_test;
  size_t i, j;
  double rounded;
  double resp_dat[] = { 39.3, -22.8,  2.6, 11.4,
                        -3.3,  46.0,  5.9, 27.3,
                       -20.7,  42.4, 43.5, 14.0,
                        -6.6,  10.6, 10.9, 44.5};
  double int_map_dat[] = {-1.0, -0.6, -0.0,  0.6,
                           0.0, -1.0, -0.0,  0.6,
                          -0.5,  0.7, -1.0,  0.0,
                          -0.0, -0.0,  0.3, -1.0};
  resp = gsl_matrix_view_array (resp_dat, 4, 4);
  int_map = gsl_matrix_view_array (int_map_dat, 4, 4);
  int_map_test = safe_matrix_alloc (4, 4);
  mra (&resp.matrix, int_map_test);
  for (i=0; i<int_map_test->size1; i++)
    {
      for (j=0; j<int_map_test->size2; j++)
        {
          rounded = round (10.0 * gsl_matrix_get (int_map_test, i, j))/10.0;
          gsl_matrix_set (int_map_test, i, j, rounded);
        }
    }
  ck_assert (matrix_approx_equal (int_map_test, &int_map.matrix, KHOLO_TOL));
  gsl_matrix_free (int_map_test);
}
END_TEST


Suite *
mra_suite (void)
{
  Suite *s;
  TCase *tc_mra_main;

  s = suite_create ("mra");

  tc_mra_main = tcase_create ("mra_main");
  tcase_add_test (tc_mra_main, test_mra_4a);
  tcase_add_test (tc_mra_main, test_mra_4b);
  tcase_add_test (tc_mra_main, test_mra_4c);
  tcase_add_test (tc_mra_main, test_mra_4d);
  tcase_add_test (tc_mra_main, test_mra_7a);
  tcase_add_test (tc_mra_main, test_mra_7b);
  suite_add_tcase (s, tc_mra_main);

  return s;
}


int main (void)
{
  int n_fail;
  Suite *s;
  SRunner *sr;

  s = mra_suite ();
  sr = srunner_create (s);

  srunner_run_all (sr, CK_NORMAL);
  n_fail = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
