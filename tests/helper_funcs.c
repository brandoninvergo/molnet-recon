/* 
 * helper_funcs.c --- 
 * 
 * Copyright (C) 2017, 2018, 2019 Brandon Invergo <invergo@ebi.ac.uk>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "helper_funcs.h"


bool
matrix_has_nan (gsl_matrix *M)
{
  for (size_t i=0; i<M->size1; i++)
    {
      for (size_t j=0; j<M->size2; j++)
        {
          if (gsl_isnan (gsl_matrix_get (M, i, j)))
            return true;
        }
    }
  return false;
}


bool
matrix_has_inf (gsl_matrix *M)
{
  for (size_t i=0; i<M->size1; i++)
    {
      for (size_t j=0; j<M->size2; j++)
        {
          if (gsl_isinf (gsl_matrix_get (M, i, j)))
            return true;
        }
    }
  return false;
}


bool
matrix_approx_equal (gsl_matrix *A, gsl_matrix *B, double tol)
{
  if ((A->size1 != B->size1) || (A->size2 != B->size2))
    return false;
  for (size_t i=0; i<A->size1; i++)
    {
      for (size_t j=0; j<A->size2; j++)
        {
          if (gsl_fcmp (gsl_matrix_get (A, i, j),
                        gsl_matrix_get (B, i, j),
                        tol) != 0)
            return false;
        }
    }
  return true;
}


bool
vector_has_nan (gsl_vector *V)
{
  for (size_t i=0; i<V->size; i++)
    {
      if (gsl_isnan (gsl_vector_get (V, i)))
        return true;
    }
  return false;
}


bool
vector_has_inf (gsl_vector *V)
{
  for (size_t i=0; i<V->size; i++)
    {
      if (gsl_isinf (gsl_vector_get (V, i)))
        return true;
    }
  return false;
}


bool
vector_approx_equal (gsl_vector *A, gsl_vector *B, double tol)
{
  if (A->size != B->size)
    return false;
  for (size_t i=0; i<A->size; i++)
    {
      if (gsl_fcmp (gsl_vector_get (A, i),
                    gsl_vector_get (B, i),
                    tol) != 0)
        return false;
    }
  return true;
}


void
print_matrix (gsl_matrix *M)
{
  size_t i, j;
  for (i=0; i<M->size1; i++)
    {
      for (j=0; j<M->size2; j++)
        {
          printf ("%f\t", gsl_matrix_get (M, i, j));
        }
      puts ("");
    }
}


void
print_matrix_int (gsl_matrix_int *M)
{
  size_t i, j;
  for (i=0; i<M->size1; i++)
    {
      for (j=0; j<M->size2; j++)
        {
          printf ("%d\t", gsl_matrix_int_get (M, i, j));
        }
      puts ("");
    }
}


void
print_vector (gsl_vector *v)
{
  size_t i;
  for (i=0; i<v->size; i++)
    {
      printf ("%f\t", gsl_vector_get (v, i));
    }
  puts ("");
}

