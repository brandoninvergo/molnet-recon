/* 
 * test_readtable.c --- 
 * 
 * Copyright (C) 2019 Brandon Invergo <invergo@ebi.ac.uk>
 * 
 * Author: Brandon Invergo <brandon@invergo.net>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <check.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_int.h>

#include "helper_funcs.h"
#include "../src/readtable.h"
#include "../src/datatable.h"
#include "../src/safe_alloc.h"

#define TOL 1e-6


START_TEST(test_read_narrow_table)
{
  FILE *table_stream;
  gsl_matrix_view dmat_m, bmat_m;
  gsl_matrix_int_view imat_m;
  datatable *dtab, *btab;
  datatable_int *itab;

  double dmat[] = {0.1, 0.7,
                   0.3, 0.8};
  dmat_m = gsl_matrix_view_array (dmat, 2, 2);
  double bmat[] = {1.0, 1.0,
                   0.0, 0.0};
  bmat_m = gsl_matrix_view_array (bmat, 2, 2);
  int imat[] = {12, 3423,
                2, -24};
  imat_m = gsl_matrix_int_view_array (imat, 2, 2);

  table_stream = fopen ("data/test-table.tsv", "r");
  read_narrow_table (table_stream, "\t", false, 3, DATA_TYPE_FLOAT, &dtab,
                     DATA_TYPE_BOOL, &btab, DATA_TYPE_INT, &itab);
  fclose (table_stream);

  ck_assert (matrix_approx_equal (&dmat_m.matrix, dtab->data, TOL));
  ck_assert (matrix_approx_equal (&bmat_m.matrix, btab->data, TOL));
  ck_assert (gsl_matrix_int_equal (&imat_m.matrix, itab->data));
  datatable_free (dtab);
  datatable_free (btab);
  datatable_int_free (itab);
}
END_TEST


START_TEST(test_read_narrow_table_header)
{
  FILE *table_stream;
  gsl_matrix_view dmat_m, bmat_m;
  gsl_matrix_int_view imat_m;
  datatable *dtab, *btab;
  datatable_int *itab;

  double dmat[] = {0.1, 0.7,
                   0.3, 0.8};
  dmat_m = gsl_matrix_view_array (dmat, 2, 2);
  double bmat[] = {1.0, 1.0,
                   0.0, 0.0};
  bmat_m = gsl_matrix_view_array (bmat, 2, 2);
  int imat[] = {12, 3423,
                2, -24};
  imat_m = gsl_matrix_int_view_array (imat, 2, 2);

  table_stream = fopen ("data/test-table-header.tsv", "r");
  read_narrow_table (table_stream, "\t", true, 3, DATA_TYPE_FLOAT, &dtab,
                     DATA_TYPE_BOOL, &btab, DATA_TYPE_INT, &itab);
  fclose (table_stream);

  ck_assert (matrix_approx_equal (&dmat_m.matrix, dtab->data, TOL));
  ck_assert (matrix_approx_equal (&bmat_m.matrix, btab->data, TOL));
  ck_assert (gsl_matrix_int_equal (&imat_m.matrix, itab->data));
  datatable_free (dtab);
  datatable_free (btab);
  datatable_int_free (itab);
}
END_TEST


Suite *
readtable_suite (void)
{
  Suite *s;
  TCase *tc_narrow;

  s = suite_create ("readtable");

  tc_narrow = tcase_create ("Read narrow data tables");
  tcase_add_test (tc_narrow, test_read_narrow_table);
  tcase_add_test (tc_narrow, test_read_narrow_table_header);
  suite_add_tcase (s, tc_narrow);

  return s;
}


int main (void)
{
  int n_fail;
  Suite *s;
  SRunner *sr;

  s = readtable_suite ();
  sr = srunner_create (s);

  srunner_run_all (sr, CK_NORMAL);
  n_fail = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
