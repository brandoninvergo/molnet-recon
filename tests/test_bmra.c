/* 
 * test-bmra.c --- 
 * 
 * Copyright (C) 2017, 2019 Brandon M. Invergo <invergo@ebi.ac.uk>
 * 
 * Author: Brandon M. Invergo <invergo@ebi.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <check.h>

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_rng.h>
#include "helper_funcs.h"

#include "../src/bmra.h"
#include "../src/safe_alloc.h"

#define TOL 1e-7
#define RESULTS_TOL 0.05

static gsl_rng *rng;


void
init_args_Halasz_et_al_2016 (struct arguments *arguments)
{
  arguments->init = NET_INIT_FILE;
  arguments->init_file = NULL;
  arguments->edge_prior = EDGE_PRIOR_HAMMING;
  arguments->runs = 4;
  arguments->sampler = SAMPLER_MCMC;
  arguments->samples = 50000;
  arguments->burnin = arguments->samples/2;
  arguments->thinning = 1;
  arguments->delim = "\t";
  arguments->hamming_psi = 1.0;
  arguments->beta_a = 1.0;
  arguments->beta_b = 2.0;
  arguments->lambda = 0.2;
  arguments->igamma_a = 1.0;
  arguments->igamma_b = 1.0;
  arguments->zellner_g = ZELLNER_G_UIP;
  arguments->lpa = LPA_METHOD_IMPLEMENTED;
  arguments->subnet_only = true;
  arguments->trace = TRACE_NONE;
}


START_TEST(test_sub_matrix_by_col)
{
  gsl_matrix_view resp_sub;
  gsl_matrix *resp_var;
  gsl_vector_view A_init;
  struct arguments args;
  init_args_Halasz_et_al_2016 (&args);

  double resp_sub_dat[] = { 1.0,  2.0,  3.0,  4.0,
                            5.0,  6.0,  7.0,  8.0,
                            9.0, 10.0, 11.0, 12.0,
                           13.0, 14.0, 15.0, 16.0};
  resp_sub = gsl_matrix_view_array (resp_sub_dat, 4, 4);

  double A_init_dat[] = {1.0, 0.0, 0.0, 1.0};
  A_init = gsl_vector_view_array (A_init_dat, 4);

  resp_var = sub_matrix_by_col (&resp_sub.matrix, &A_init.vector);

  ck_assert (resp_var->size1 == resp_sub.matrix.size1 && resp_var->size2 == 2);
  ck_assert (gsl_fcmp (gsl_matrix_get (resp_var, 0, 0), 1.0, TOL) == 0);
  ck_assert (gsl_fcmp (gsl_matrix_get (resp_var, 3, 1), 16.0, TOL) == 0);

  gsl_matrix_free (resp_var);
}
END_TEST


START_TEST(test_sub_matrix_by_row)
{
  gsl_matrix_view resp_sub;
  gsl_matrix *resp_var;
  gsl_vector_view A_init;
  struct arguments args;
  init_args_Halasz_et_al_2016 (&args);

  double resp_sub_dat[] = { 1.0,  2.0,  3.0,  4.0,
                            5.0,  6.0,  7.0,  8.0,
                            9.0, 10.0, 11.0, 12.0,
                           13.0, 14.0, 15.0, 16.0};
  resp_sub = gsl_matrix_view_array (resp_sub_dat, 4, 4);

  double A_init_dat[] = {1.0, 0.0, 0.0, 1.0};
  A_init = gsl_vector_view_array (A_init_dat, 4);

  resp_var = sub_matrix_by_row (&resp_sub.matrix, &A_init.vector);

  ck_assert (resp_var->size1 == 2 && resp_var->size2 == resp_sub.matrix.size2);
  ck_assert (gsl_fcmp (gsl_matrix_get (resp_var, 0, 0), 1.0, TOL) == 0);
  ck_assert (gsl_fcmp (gsl_matrix_get (resp_var, 1, 0), 13.0, TOL) == 0);

  gsl_matrix_free (resp_var);
}
END_TEST


START_TEST(test_remove_edge)
{
  gsl_vector *A;
  A = safe_vector_alloc (4);
  gsl_vector_set_all (A, 1.0);
  remove_edge (A, rng);
  ck_assert ((size_t) gsl_blas_dasum (A) == 3);
  ck_assert ((size_t) gsl_vector_max (A) == 1);
  gsl_vector_set_zero (A);
  ck_assert ((size_t) gsl_blas_dasum (A) == 0);
  gsl_vector_free (A);
}
END_TEST


START_TEST(test_add_edge_from_A0)
{
  gsl_vector *A, *A0;
  size_t node = 0;

  A = safe_vector_alloc (4);
  A0 = safe_vector_alloc (4);
  gsl_vector_set_zero (A);
  gsl_vector_set_zero (A0);
  gsl_vector_set (A0, 1, 1.0);
  add_edge_from_A0 (A, A0, node, rng);
  ck_assert ((size_t) gsl_blas_dasum (A) == 1);
  ck_assert ((size_t) gsl_vector_max (A) == 1);
  ck_assert ((size_t) gsl_vector_get (A, 1) == 1);

  gsl_vector_set_all (A, 1.0);
  add_edge_from_A0 (A, A0, node, rng);
  ck_assert ((size_t) gsl_blas_dasum (A) == 4);
  ck_assert ((size_t) gsl_vector_max (A) == 1);

  gsl_vector_set_zero (A);
  gsl_vector_set_zero (A0);
  add_edge_from_A0 (A, A0, node, rng);
  ck_assert ((size_t) gsl_blas_dasum (A) == 0);
  ck_assert ((size_t) gsl_vector_max (A) == 0);

  gsl_vector_set_zero (A);
  gsl_vector_set_zero (A0);
  gsl_vector_set (A0, 1, 1.0);
  node = 1;
  add_edge_from_A0 (A, A0, node, rng);
  ck_assert ((size_t) gsl_blas_dasum (A) == 0);
  ck_assert ((size_t) gsl_vector_max (A) == 0);

  gsl_vector_free (A);
  gsl_vector_free (A0);
}
END_TEST


START_TEST(test_add_edge)
{
  gsl_vector *A;
  A = safe_vector_alloc (4);
  gsl_vector_set_zero (A);
  add_edge (A, 0, rng);
  ck_assert ((size_t) gsl_blas_dasum (A) == 1);
  ck_assert ((size_t) gsl_vector_max (A) == 1);
  ck_assert (!(bool) gsl_vector_get (A, 0));
  gsl_vector_set_zero (A);
  add_edge (A, 4, rng);
  ck_assert ((size_t) gsl_blas_dasum (A) == 0);
  ck_assert ((size_t) gsl_vector_max (A) == 0);
  gsl_vector_set_all (A, 1.0);
  add_edge (A, 0, rng);
  ck_assert ((size_t) gsl_blas_dasum (A) == 4);
  ck_assert ((size_t) gsl_vector_max (A) == 1);
  gsl_vector_set_all (A, 1.0);
  gsl_vector_set (A, 0, 0.0);
  add_edge (A, 0, rng);
  ck_assert ((size_t) gsl_blas_dasum (A) == 3);
  ck_assert ((size_t) gsl_vector_max (A) == 1);
  ck_assert (!(bool) gsl_vector_get (A, 0));
  gsl_vector_free (A);
}
END_TEST


START_TEST(test_gen_A_init)
{
  gsl_vector *A0, *A_init;
  size_t nkmax = 3;
  struct arguments args;
  size_t node=1;

  init_args_Halasz_et_al_2016 (&args);

  A0 = safe_vector_alloc (4);
  gsl_vector_set_zero (A0);
  gsl_vector_set (A0, 0, 1.0);
  A_init = gen_A_init (A0, nkmax, node, rng, args);
  ck_assert ((size_t) gsl_blas_dasum (A_init) < 2);
  ck_assert ((size_t) gsl_vector_max (A_init) == 0 ||
             (size_t) gsl_vector_max (A_init) == 1);
  gsl_vector_free (A_init);

  gsl_vector_set_zero (A0);
  A_init = gen_A_init (A0, nkmax, node, rng, args);
  ck_assert ((size_t) gsl_blas_dasum (A_init) == 0);
  gsl_vector_free (A_init);

  gsl_vector_set (A0, 0, 1.0);
  nkmax = 0.0;
  A_init = gen_A_init (A0, nkmax, node, rng, args);
  ck_assert ((size_t) gsl_blas_dasum (A_init) == 0);
  gsl_vector_free (A_init);

  gsl_vector_set_zero (A0);
  gsl_vector_set (A0, 0, 1.0);
  node = 0;
  A_init = gen_A_init (A0, nkmax, node, rng, args);
  ck_assert ((size_t) gsl_blas_dasum (A_init) == 0);
  gsl_vector_set_zero (A_init);

  gsl_vector_free (A0);
}
END_TEST


START_TEST(test_propose_A_new)
{
  gsl_vector *A, *A0, *A_new;
  size_t nkmax = 3;
  size_t node = 1;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);

  A = safe_vector_alloc (4);
  A0 = safe_vector_alloc (4);

  gsl_vector_set_zero (A0);
  gsl_vector_set (A0, 1, 1.0);
  gsl_vector_set (A0, 2, 1.0);
  gsl_vector_set_zero (A);
  gsl_vector_set (A, 1, 1.0);
  A_new = propose_A_new (A, A0, nkmax, node, rng, args);
  ck_assert ((size_t) gsl_blas_dasum (A_new) < 3);
  gsl_vector_free (A_new);
  gsl_vector_set (A, 2, 1.0);
  gsl_vector_set (A, 3, 1.0);
  A_new = propose_A_new (A, A0, nkmax, node, rng, args);
  ck_assert ((size_t) gsl_blas_dasum (A_new) < 3);
  gsl_vector_free (A_new);
  gsl_vector_set_zero (A);
  A_new = propose_A_new (A, A0, nkmax, node, rng, args);
  ck_assert ((size_t) gsl_blas_dasum (A_new) == 1);
  gsl_vector_free (A_new);

  gsl_vector_free (A);
  gsl_vector_free (A0);
}
END_TEST


START_TEST(test_hamming_distance)
{
  gsl_vector *A, *B;

  A = safe_vector_alloc (4);
  B = safe_vector_alloc (4);

  gsl_vector_set_zero (A);
  gsl_vector_set_zero (B);
  ck_assert (hamming_distance (A, B) == 0.0);
  ck_assert (gsl_fcmp (hamming_distance (A, B),
                       hamming_distance (B, A), TOL)==0);

  gsl_vector_set (A, 1, 1.0);
  ck_assert (gsl_fcmp (hamming_distance (A, B), 1.0, TOL)==0);
  ck_assert (gsl_fcmp (hamming_distance (A, B),
                       hamming_distance (B, A), TOL)==0);

  gsl_vector_set (B, 2, 1.0);
  ck_assert (gsl_fcmp (hamming_distance (A, B), 2.0, TOL)==0);
  ck_assert (gsl_fcmp (hamming_distance (A, B),
                       hamming_distance (B, A), TOL)==0);

  gsl_vector_set_all (A, 1.0);
  gsl_vector_set_all (B, 1.0);
  ck_assert (gsl_fcmp (hamming_distance (A, B), 0.0, TOL)==0);
  ck_assert (gsl_fcmp (hamming_distance (A, B),
                       hamming_distance (B, A), TOL)==0);

  gsl_vector_free (A);
  gsl_vector_free (B);
}
END_TEST


START_TEST(test_p_Ai_from_prior)
{
  gsl_vector_view A, A_prior;
  size_t node = 0;
  double p_Ai;
  struct arguments args;

  double A_data_a[] = {0.0, 1.0, 0.0, 1.0, 1.0};
  A = gsl_vector_view_array (A_data_a, 5);
  double A_prior_data_a[] = {0.0, 0.75, 0.3, 0.7, 0.5};
  A_prior = gsl_vector_view_array (A_prior_data_a, 5);

  args.prob_gamma = 0.0;
  p_Ai = p_Ai_from_prior (&A.vector, &A_prior.vector, node, args);
  ck_assert (gsl_fcmp (p_Ai, 0.18375, TOL)==0);

  double A_prior_data_b[] = {0.0, 0.75, 0.0, 0.7, 0.5};
  A_prior = gsl_vector_view_array (A_prior_data_b, 5);
  p_Ai = p_Ai_from_prior (&A.vector, &A_prior.vector, node, args);
  ck_assert (gsl_fcmp (p_Ai, 0.2625, TOL)==0);

  double A_data_b[] = {0.0, 1.0, 1.0, 1.0, 1.0};
  A = gsl_vector_view_array (A_data_b, 5);
  p_Ai = p_Ai_from_prior (&A.vector, &A_prior.vector, node, args);
  ck_assert (gsl_fcmp (p_Ai, 0.0, TOL)==0);
}
END_TEST


START_TEST(test_p_Ai_from_prior_w_gamma)
{
  gsl_vector_view A, A_prior;
  size_t node = 0;
  double p_Ai;
  struct arguments args;

  double A_data[] = {0.0, 1.0, 1.0, 1.0, 1.0};
  A = gsl_vector_view_array (A_data, 5);
  double A_prior_data[] = {0.0, 0.75, 0.0, 0.7, 0.5};
  A_prior = gsl_vector_view_array (A_prior_data, 5);

  args.prob_gamma = 0.0;
  p_Ai = p_Ai_from_prior (&A.vector, &A_prior.vector, node, args);
  ck_assert (p_Ai == 0.0);
  args.prob_gamma = 0.01;
  p_Ai = p_Ai_from_prior (&A.vector, &A_prior.vector, node, args);
  ck_assert (gsl_fcmp (p_Ai, 0.002625, TOL)==0);
}
END_TEST


START_TEST(test_sample_mini)
{
  bool result;
  double lp_A = -4.0, lp_A_alt = -3.0;
  gsl_rng_set (rng, 1);
  /* First random uniform value will be 0.417022.  Plugging this value
     into the Halasz et al Matlab code gives... */
  result = sample_mini (lp_A_alt, lp_A, rng);
  ck_assert (result);
  gsl_rng_set (rng, 4);
  /* Now with 0.967030 */
  result = sample_mini (lp_A_alt, lp_A, rng);
  ck_assert (!result);
}
END_TEST


START_TEST(test_sample_r_st_1d)
{
  double bs=5.843884512200428;
  double p=1.0, n=3.0;
  gsl_matrix *Vs;
  gsl_vector *mus, *r;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);
  Vs = safe_matrix_alloc (1, 1);
  gsl_matrix_set (Vs, 0, 0, 10.342354754270014);
  mus = safe_vector_alloc (1);
  gsl_vector_set (mus, 0, -0.899760380337770);
  r = safe_vector_alloc (1);
  gsl_rng_set (rng, 1);
  sample_r_st (bs, p, n, Vs, mus, r, rng, args);
  ck_assert (gsl_blas_dasum (r) != 0.0);
  gsl_matrix_free (Vs);
  gsl_vector_free (mus);
  gsl_vector_free (r);
}
END_TEST


START_TEST(test_sample_r_st_2d)
{
  double bs=1.821033920054435;
  double p=1.0, n=3.0;
  gsl_matrix_view Vs;
  gsl_vector_view mus;
  gsl_vector *r;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);

  double Vs_dat[] = { 0.635253089423275, -1.565598189899281,  1.624312545757892,
                     -1.565598189899281, 11.374882943561873,  0.954957282924982,
                      1.624312545757892,  0.954957282924982, 11.914316557137504};
  Vs = gsl_matrix_view_array (Vs_dat, 3, 3);

  double mus_dat[] = {-0.851427674553181, 0.491372086321300, -1.171710145212538};
  mus = gsl_vector_view_array (mus_dat, 3);
  r = safe_vector_alloc (3);
  gsl_rng_set (rng, 1);
  sample_r_st (bs, p, n, &Vs.matrix, &mus.vector, r, rng, args);
  ck_assert (gsl_blas_dasum (r) != 0.0);
}
END_TEST


START_TEST(test_sample_r_st_0d)
{
  double bs=5.851076762959463;
  double p=1.0, n=3.0;
  gsl_matrix *Vs;
  gsl_vector *mus, *r;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);
  Vs = NULL;
  mus = NULL;
  r = NULL;
  gsl_rng_set (rng, 1);
  sample_r_st (bs, p, n, Vs, mus, r, rng, args);
  ck_assert (r == NULL);
}
END_TEST


START_TEST(test_lp_A_comp_V1_1d)
{
  gsl_matrix_view resp_var, V1_exp;
  gsl_matrix *V1=NULL;
  double resp_var_dat[] = {0.133976571739506, 0.056661146961828, 0.036839207701224};
  resp_var = gsl_matrix_view_array (resp_var_dat, 1, 3);
  double V1_exp_dat[] = {0.022517334574155};
  V1_exp = gsl_matrix_view_array (V1_exp_dat, 1, 1);

  V1 = lp_A_comp_V1 (&resp_var.matrix);
  ck_assert (V1 != NULL);
  ck_assert (V1->size1 == 1);
  ck_assert (V1->size2 == 1);
  ck_assert (!matrix_has_nan (V1));
  ck_assert (!matrix_has_inf (V1));
  ck_assert (matrix_approx_equal (V1, &V1_exp.matrix, TOL));
  gsl_matrix_free (V1);
}
END_TEST


START_TEST(test_lp_A_comp_V1_2d)
{
  gsl_matrix_view resp_var, V1_exp;
  gsl_matrix *V1=NULL;

  double resp_var_dat[] = { 1.467373036854135,  0.724662422052526,  1.407240709966511,
                            0.270172131954003,  0.183140616297299,  0.126184139212187,
                           -0.167363967550404, -0.205957444998783, -0.215850696831997};
  resp_var = gsl_matrix_view_array (resp_var_dat, 3, 3);
  double V1_exp_dat[] = { 4.658645671008611,  0.706729881972366, -0.698588882113588,
                          0.706729881972366,  0.122455903211046, -0.110173187711564,
                         -0.698588882113588, -0.110173187711564,  0.117020690107498};
  V1_exp = gsl_matrix_view_array (V1_exp_dat, 3, 3);

  V1 = lp_A_comp_V1 (&resp_var.matrix);
  ck_assert (V1 != NULL);
  ck_assert (V1->size1 == 3);
  ck_assert (V1->size2 == 3);
  ck_assert (!matrix_has_nan (V1));
  ck_assert (!matrix_has_inf (V1));
  ck_assert (matrix_approx_equal (V1, &V1_exp.matrix, TOL));
  gsl_matrix_free (V1);
}
END_TEST


START_TEST(test_lp_A_comp_V1_0d)
{
  gsl_matrix *resp_var, *V1=NULL;

  resp_var = NULL;
  V1 = lp_A_comp_V1 (resp_var);
  ck_assert (V1 == NULL);
}
END_TEST


START_TEST(test_lp_A_comp_Vb_1_1d)
{
  gsl_matrix_view V1, Vb_1_exp;
  gsl_matrix *Vb_1=NULL;
  double c = 3.0;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);

  double V1_dat[] = {0.022517334574155};
  V1 = gsl_matrix_view_array (V1_dat, 1, 1);
  double Vb_1_exp_dat[] = {0.074172444858052};
  Vb_1_exp = gsl_matrix_view_array (Vb_1_exp_dat, 1, 1);

  Vb_1 = lp_A_comp_Vb_1 (&V1.matrix, c, args);
  ck_assert (Vb_1 != NULL);
  ck_assert (Vb_1->size1 == 1);
  ck_assert (Vb_1->size2 == 1);
  ck_assert (!matrix_has_nan (Vb_1));
  ck_assert (!matrix_has_inf (Vb_1));
  ck_assert (matrix_approx_equal (Vb_1, &Vb_1_exp.matrix, TOL));
  gsl_matrix_free (Vb_1);
}
END_TEST


START_TEST(test_lp_A_comp_Vb_1_2d)
{
  gsl_matrix_view V1, Vb_1_exp;
  gsl_matrix *Vb_1=NULL;
  double c = 3.0;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);

  double V1_dat[] = { 4.658645671008611,  0.706729881972366, -0.698588882113588,
                      0.706729881972366,  0.122455903211046, -0.110173187711564,
                     -0.698588882113588, -0.110173187711564,  0.117020690107498};
  V1 = gsl_matrix_view_array (V1_dat, 3, 3);
  double Vb_1_exp_dat[] = { 1.619548557002870,  0.235576627324122, -0.232862960704529,
                            0.235576627324122,  0.107485301070349, -0.036724395903855,
                           -0.232862960704529, -0.036724395903855,  0.105673563369166};
  Vb_1_exp = gsl_matrix_view_array (Vb_1_exp_dat, 3, 3);

  Vb_1 = lp_A_comp_Vb_1 (&V1.matrix, c, args);
  ck_assert (Vb_1 != NULL);
  ck_assert (Vb_1->size1 == 3);
  ck_assert (Vb_1->size2 == 3);
  ck_assert (!matrix_has_nan (Vb_1));
  ck_assert (!matrix_has_inf (Vb_1));
  ck_assert (matrix_approx_equal (Vb_1, &Vb_1_exp.matrix, TOL));
  gsl_matrix_free (Vb_1);
}
END_TEST


START_TEST(test_lp_A_comp_Vb_1_0d)
{
  gsl_matrix *V1, *Vb_1=NULL;
  double c = 3.0;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);

  V1 = NULL;
  Vb_1 = lp_A_comp_Vb_1 (V1, c, args);
  ck_assert (Vb_1 == NULL);
}
END_TEST


START_TEST(test_lp_A_comp_Vs_1_1d)
{
  gsl_matrix_view V1, Vb_1, Vs_1_exp;
  gsl_matrix *Vs_1=NULL;

  double V1_dat[] = {0.022517334574155};
  V1 = gsl_matrix_view_array (V1_dat, 1, 1);
  double Vb_1_dat[] = {0.074172444858052};
  Vb_1 = gsl_matrix_view_array (Vb_1_dat, 1, 1);
  double Vs_1_exp_dat[] = {0.096689779432207};
  Vs_1_exp = gsl_matrix_view_array (Vs_1_exp_dat, 1, 1);

  Vs_1 = lp_A_comp_Vs_1 (&V1.matrix, &Vb_1.matrix);
  ck_assert (Vs_1 != NULL);
  ck_assert (Vs_1->size1 == 1);
  ck_assert (Vs_1->size2 == 1);
  ck_assert (!matrix_has_nan (Vs_1));
  ck_assert (!matrix_has_inf (Vs_1));
  ck_assert (matrix_approx_equal (Vs_1, &Vs_1_exp.matrix, TOL));
  gsl_matrix_free (Vs_1);
}
END_TEST


START_TEST(test_lp_A_comp_Vs_1_2d)
{
  gsl_matrix_view V1, Vb_1, Vs_1_exp;
  gsl_matrix *Vs_1=NULL;

  double V1_dat[] = { 4.658645671008611,  0.706729881972366, -0.698588882113588,
                      0.706729881972366,  0.122455903211046, -0.110173187711564,
                     -0.698588882113588, -0.110173187711564,  0.117020690107498};
  V1 = gsl_matrix_view_array (V1_dat, 3, 3);
  double Vb_1_dat[] = { 1.619548557002870,  0.235576627324122, -0.232862960704529,
                        0.235576627324122,  0.107485301070349, -0.036724395903855,
                       -0.232862960704529, -0.036724395903855,  0.105673563369166};
  Vb_1 = gsl_matrix_view_array (Vb_1_dat, 3, 3);
  double Vs_1_exp_dat[] = { 6.278194228011482,  0.942306509296488, -0.931451842818117,
                            0.942306509296488,  0.229941204281395, -0.146897583615418,
                           -0.931451842818117, -0.146897583615418,  0.222694253476665};
  Vs_1_exp = gsl_matrix_view_array (Vs_1_exp_dat, 3, 3);

  Vs_1 = lp_A_comp_Vs_1 (&V1.matrix, &Vb_1.matrix);
  ck_assert (Vs_1 != NULL);
  ck_assert (Vs_1->size1 == 3);
  ck_assert (Vs_1->size2 == 3);
  ck_assert (!matrix_has_nan (Vs_1));
  ck_assert (!matrix_has_inf (Vs_1));
  ck_assert (matrix_approx_equal (Vs_1, &Vs_1_exp.matrix, TOL));
  gsl_matrix_free (Vs_1);
}
END_TEST


START_TEST(test_lp_A_comp_Vs_1_0d)
{
  gsl_matrix *V1, *Vb_1, *Vs_1=NULL;

  V1 = NULL;
  Vb_1 = NULL;
  Vs_1 = lp_A_comp_Vs_1 (V1, Vb_1);
  ck_assert (Vs_1 == NULL);
}
END_TEST


START_TEST(test_lp_A_comp_Vs_1d)
{
  gsl_matrix_view Vs_1, Vs_exp;
  gsl_matrix *Vs=NULL;

  double Vs_1_dat[] = {0.096689779432207};
  Vs_1 = gsl_matrix_view_array (Vs_1_dat, 1, 1);
  double Vs_exp_dat[] = {10.342354754270014};
  Vs_exp = gsl_matrix_view_array (Vs_exp_dat, 1, 1);

  Vs = lp_A_comp_Vs (&Vs_1.matrix);
  ck_assert (Vs != NULL);
  ck_assert (Vs->size1 == 1);
  ck_assert (Vs->size2 == 1);
  ck_assert (!matrix_has_nan (Vs));
  ck_assert (!matrix_has_inf (Vs));
  ck_assert (matrix_approx_equal (Vs, &Vs_exp.matrix, TOL));
  gsl_matrix_free (Vs);
}
END_TEST


START_TEST(test_lp_A_comp_Vs_2d)
{
  gsl_matrix_view Vs_1, Vs_exp;
  gsl_matrix *Vs=NULL;

  double Vs_1_dat[] = { 6.278194228011482,  0.942306509296488, -0.931451842818117,
                        0.942306509296488,  0.229941204281395, -0.146897583615418,
                       -0.931451842818117, -0.146897583615418,  0.222694253476665};
  Vs_1 = gsl_matrix_view_array (Vs_1_dat, 3, 3);
  double Vs_exp_dat[] = { 0.635253089423275, -1.565598189899281,  1.624312545757892,
                         -1.565598189899281, 11.374882943561873,  0.954957282924982,
                          1.624312545757892,  0.954957282924982, 11.914316557137504};
  Vs_exp = gsl_matrix_view_array (Vs_exp_dat, 3, 3);

  Vs = lp_A_comp_Vs (&Vs_1.matrix);
  ck_assert (Vs != NULL);
  ck_assert (Vs->size1 == 3);
  ck_assert (Vs->size2 == 3);
  ck_assert (!matrix_has_nan (Vs));
  ck_assert (!matrix_has_inf (Vs));
  ck_assert (matrix_approx_equal (Vs, &Vs_exp.matrix, TOL));
  gsl_matrix_free (Vs);
}
END_TEST


START_TEST(test_lp_A_comp_Vs_0d)
{
  gsl_matrix *Vs_1, *Vs=NULL;

  Vs_1 = NULL;
  Vs = lp_A_comp_Vs (Vs_1);
  ck_assert (Vs == NULL);
}
END_TEST


START_TEST(test_lp_A_comp_mus_1d)
{
  gsl_matrix_view resp_var, Vs;
  gsl_vector_view resp_sub_i, mus_exp;
  gsl_vector *mus=NULL;

  double resp_var_dat[] = {0.133976571739506, 0.056661146961828, 0.036839207701224};
  resp_var = gsl_matrix_view_array (resp_var_dat, 1, 3);
  double resp_sub_i_dat[] = {-1.630062629902668, 2.624505521481527, -0.470013444286751};
  resp_sub_i = gsl_vector_view_array (resp_sub_i_dat, 3);
  double Vs_dat[] = {10.342354754270014};
  Vs = gsl_matrix_view_array (Vs_dat, 1, 1);
  double mus_exp_dat[] = {-0.899760380337770};
  mus_exp = gsl_vector_view_array (mus_exp_dat, 1);

  mus = lp_A_comp_mus (&resp_var.matrix, &resp_sub_i.vector, &Vs.matrix);
  ck_assert (mus != NULL);
  ck_assert (mus->size == 1);
  ck_assert (!vector_has_nan (mus));
  ck_assert (!vector_has_inf (mus));
  ck_assert (vector_approx_equal (mus, &mus_exp.vector, TOL));
  gsl_vector_free (mus);
}
END_TEST


START_TEST(test_lp_A_comp_mus_2d)
{
  gsl_matrix_view resp_var, Vs;
  gsl_vector_view resp_sub_i, mus_exp;
  gsl_vector *mus=NULL;

  double resp_var_dat[] = { 1.467373036854135,  0.724662422052526,  1.407240709966511,
                            0.270172131954003,  0.183140616297299,  0.126184139212187,
                           -0.167363967550404, -0.205957444998783, -0.215850696831997};
  resp_var = gsl_matrix_view_array (resp_var_dat, 3, 3);
  double Vs_dat[] = { 0.635253089423275, -1.565598189899281,  1.624312545757892,
                     -1.565598189899281, 11.374882943561873,  0.954957282924982,
                      1.624312545757892,  0.954957282924982, 11.914316557137504};
  Vs = gsl_matrix_view_array (Vs_dat, 3, 3);
  double resp_sub_i_dat[] = {-1.561566076624013, 0.331417657875287, -1.236305613358123};
  resp_sub_i = gsl_vector_view_array (resp_sub_i_dat, 3);
  double mus_exp_dat[] = {-0.851427674553181, 0.491372086321300, -1.171710145212538};
  mus_exp = gsl_vector_view_array (mus_exp_dat, 3);

  mus = lp_A_comp_mus (&resp_var.matrix, &resp_sub_i.vector, &Vs.matrix);
  ck_assert (mus != NULL);
  ck_assert (mus->size == 3);
  ck_assert (!vector_has_nan (mus));
  ck_assert (!vector_has_inf (mus));
  ck_assert (vector_approx_equal (mus, &mus_exp.vector, TOL));
  gsl_vector_free (mus);
}
END_TEST


START_TEST(test_lp_A_comp_mus_0d)
{
  gsl_matrix *resp_var, *Vs;
  gsl_vector *resp_sub_i, *mus=NULL;

  resp_var = NULL;
  Vs = NULL;
  resp_sub_i = NULL;
  mus = lp_A_comp_mus (resp_var, resp_sub_i, Vs);
  ck_assert (mus == NULL);
}
END_TEST


START_TEST(test_lp_A_comp_bs_1d)
{
  gsl_vector_view resp_sub_i, mus;
  gsl_matrix_view Vs_1;
  double bs;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);

  double resp_sub_i_dat[] = {-1.630062629902668, 2.624505521481527, -0.470013444286751};
  resp_sub_i = gsl_vector_view_array (resp_sub_i_dat, 3);
  double mus_dat[] = {-0.899760380337770};
  mus = gsl_vector_view_array (mus_dat, 1);
  double Vs_1_dat[] = {0.096689779432207};
  Vs_1 = gsl_matrix_view_array (Vs_1_dat, 1, 1);

  bs = lp_A_comp_bs (&resp_sub_i.vector, &mus.vector, &Vs_1.matrix, args);
  ck_assert (gsl_fcmp (bs, 5.843884512200428, TOL) == 0);
}
END_TEST


START_TEST(test_lp_A_comp_bs_2d)
{
  gsl_vector_view resp_sub_i, mus;
  gsl_matrix_view Vs_1;
  double bs;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);

  double resp_sub_i_dat[] = {-1.561566076624013, 0.331417657875287, -1.236305613358123};
  resp_sub_i = gsl_vector_view_array (resp_sub_i_dat, 3);
  double mus_dat[] = {-0.851427674553181, 0.491372086321300, -1.171710145212538};
  mus = gsl_vector_view_array (mus_dat, 3);
  double Vs_1_dat[] = { 6.278194228011482,  0.942306509296488, -0.931451842818117,
                        0.942306509296488,  0.229941204281395, -0.146897583615418,
                       -0.931451842818117, -0.146897583615418,  0.222694253476665};
  Vs_1 = gsl_matrix_view_array (Vs_1_dat, 3, 3);
  bs = lp_A_comp_bs (&resp_sub_i.vector, &mus.vector, &Vs_1.matrix, args);
  ck_assert (gsl_fcmp (bs, 1.821033920054435, TOL) == 0);
}
END_TEST


START_TEST(test_lp_A_comp_bs_0d)
{
  gsl_vector_view resp_sub_i;
  gsl_vector *mus;
  gsl_matrix *Vs_1;
  double bs;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);

  double resp_sub_i_dat[] = {1.134977324565506, -0.309908709272610, 2.884083318934811};
  resp_sub_i = gsl_vector_view_array (resp_sub_i_dat, 3);

  mus = NULL;
  Vs_1 = NULL;
  bs = lp_A_comp_bs (&resp_sub_i.vector, mus, Vs_1, args);
  ck_assert (gsl_fcmp (bs, 5.851076762959463, TOL) == 0);
}
END_TEST


START_TEST(test_lp_A_given_R_1d)
{
  gsl_matrix_view resp_var;
  gsl_vector_view resp_sub_i, A, A0;
  double bs;
  gsl_matrix *Vs;
  gsl_vector *mus;
  double lp_A;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);

  double resp_var_dat[] = {0.133976571739506, 0.056661146961828, 0.036839207701224};
  resp_var = gsl_matrix_view_array (resp_var_dat, 1, 3);
  double resp_sub_i_dat[] = {-1.630062629902668, 2.624505521481527, -0.470013444286751};
  resp_sub_i = gsl_vector_view_array (resp_sub_i_dat, 3);

  double A_dat[] = {0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0};
  A = gsl_vector_view_array (A_dat, 8);
  double A0_dat[] = {0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0};
  A0 = gsl_vector_view_array (A0_dat, 8);
  lp_A = lp_A_given_R (&resp_var.matrix, &resp_sub_i.vector, &A.vector, &A0.vector,
                       NULL, 0, &bs, &Vs, &mus, args);
  ck_assert (gsl_fcmp (lp_A, -5.546046822404124, TOL) == 0);
}
END_TEST


START_TEST(test_lp_A_given_R_2d)
{
  gsl_matrix_view resp_var;
  gsl_vector_view resp_sub_i, A, A0;
  double bs;
  gsl_matrix *Vs;
  gsl_vector *mus;
  double lp_A;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);

  double resp_var_dat[] = { 1.467373036854135,  0.724662422052526,  1.407240709966511,
                            0.270172131954003,  0.183140616297299,  0.126184139212187,
                           -0.167363967550404, -0.205957444998783, -0.215850696831997};
  resp_var = gsl_matrix_view_array (resp_var_dat, 3, 3);
  double resp_sub_i_dat[] = {-1.561566076624013, 0.331417657875287, -1.236305613358123};
  resp_sub_i = gsl_vector_view_array (resp_sub_i_dat, 3);
  double A_dat[] = {0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0};
  A = gsl_vector_view_array (A_dat, 8);
  double A0_dat[] = {0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0};
  A0 = gsl_vector_view_array (A0_dat, 8);
  lp_A = lp_A_given_R (&resp_var.matrix, &resp_sub_i.vector, &A.vector, &A0.vector,
                       NULL, 0, &bs, &Vs, &mus, args);
  ck_assert (gsl_fcmp (lp_A, -3.346916037650428, TOL) == 0);
}
END_TEST


START_TEST(test_lp_A_given_R_0d)
{
  gsl_vector *A, *A0, *mus;
  gsl_vector_view resp_sub_i;
  gsl_matrix *resp_var, *Vs;
  double bs, lp_A;
  struct arguments args;

  init_args_Halasz_et_al_2016 (&args);

  A = safe_vector_calloc (8);
  A0 = safe_vector_calloc (8);
  gsl_vector_set (A0, 1, 1.0);
  gsl_vector_set (A0, 5, 1.0);
  resp_var = NULL;
  double resp_sub_i_dat[] = {1.134977324565506, -0.309908709272610, 2.884083318934811};
  resp_sub_i = gsl_vector_view_array (resp_sub_i_dat, 3);
  lp_A = lp_A_given_R (resp_var, &resp_sub_i.vector, A, A0,
                       NULL, 0, &bs, &Vs, &mus, args);
  ck_assert (gsl_fcmp (lp_A, -6.416564265876945, TOL) == 0);
  gsl_vector_free (A);
  gsl_vector_free (A0);
}
END_TEST


START_TEST(test_calc_matrix_means)
{
  datatable **m_all;
  datatable *m_mean;
  size_t i, j=0, n=4;
  char *names[2] = {"foo", "bar"};

  m_all = (datatable **) malloc (n * sizeof (datatable *));
  for (i=0; i<n; i++)
    {
      m_all[i] = datatable_alloc (2, 2, names, names);
      gsl_matrix_set (m_all[i]->data, 0, 0, j++);
      gsl_matrix_set (m_all[i]->data, 0, 1, j++);
      gsl_matrix_set (m_all[i]->data, 1, 0, j++);
      gsl_matrix_set (m_all[i]->data, 1, 1, j++);
    }
  m_mean = calc_matrix_means ((const datatable **) m_all, 4);
  ck_assert (gsl_fcmp (gsl_matrix_get (m_mean->data, 0, 0), 6.0, TOL)==0);
  ck_assert (gsl_fcmp (gsl_matrix_get (m_mean->data, 0, 1), 7.0, TOL)==0);
  ck_assert (gsl_fcmp (gsl_matrix_get (m_mean->data, 1, 0), 8.0, TOL)==0);
  ck_assert (gsl_fcmp (gsl_matrix_get (m_mean->data, 1, 1), 9.0, TOL)==0);
  for (i=0; i<4; i++)
    {
      datatable_free (m_all[i]);
    }
  free (m_all);
  datatable_free (m_mean);
}
END_TEST


START_TEST(test_calc_matrix_std)
{
  datatable **m_all;
  datatable *m_mean, *m_std;
  size_t i, j=0, n=4;
  char *names[2] = {"foo", "bar"};

  m_all = (datatable **) malloc (n * sizeof (datatable *));
  for (i=0; i<n; i++)
    {
      m_all[i] = datatable_alloc (2, 2, names, names);
      gsl_matrix_set (m_all[i]->data, 0, 0, j++);
      gsl_matrix_set (m_all[i]->data, 0, 1, j++);
      gsl_matrix_set (m_all[i]->data, 1, 0, j++);
      gsl_matrix_set (m_all[i]->data, 1, 1, j++);
    }
  m_mean = datatable_alloc (2, 2, names, names);
  gsl_matrix_set (m_mean->data, 0, 0, 6.0);
  gsl_matrix_set (m_mean->data, 0, 1, 7.0);
  gsl_matrix_set (m_mean->data, 1, 0, 8.0);
  gsl_matrix_set (m_mean->data, 1, 1, 9.0);
  m_std = calc_matrix_std ((const datatable **) m_all, m_mean, 4);
  ck_assert (!gsl_isnan (gsl_matrix_get (m_std->data, 0, 0)));
  ck_assert (gsl_fcmp (gsl_matrix_get (m_std->data, 0, 0), 5.163978, TOL)==0);
  ck_assert (!gsl_isnan (gsl_matrix_get (m_std->data, 0, 1)));
  ck_assert (gsl_fcmp (gsl_matrix_get (m_std->data, 0, 1), 5.163978, TOL)==0);
  ck_assert (!gsl_isnan (gsl_matrix_get (m_std->data, 1, 0)));
  ck_assert (gsl_fcmp (gsl_matrix_get (m_std->data, 1, 0), 5.163978, TOL)==0);
  ck_assert (!gsl_isnan (gsl_matrix_get (m_std->data, 1, 1)));
  ck_assert (gsl_fcmp (gsl_matrix_get (m_std->data, 1, 1), 5.163978, TOL)==0);
  for (i=0; i<4; i++)
    {
      datatable_free (m_all[i]);
    }
  free (m_all);
  datatable_free (m_mean);
  datatable_free (m_std);
}
END_TEST

void
run_results_tests (const char *experiment)
{
  struct arguments args;
  datatable *A_init = NULL;
  datatable *response = NULL, *A_prior = NULL;
  datatable *perts = NULL;
  datatable **A_all = NULL;
  datatable **r_all = NULL;
  datatable *P_A = NULL;
  datatable *r_mean = NULL;
  datatable *r_std = NULL;
  gsl_matrix *lp_A_all = NULL;
  gsl_matrix *lp_A_t_all = NULL;
  gsl_matrix *lp_r_all = NULL;
  size_t i, j, n;
  datatable *pub_P_A, *pub_r_mean, *pub_r_std;
  char data_file[44], results_file[47];
  FILE *data_stream, *results_stream;

  snprintf (data_file, 43, "data/Halasz-et-al-2016/%s_data.tsv", experiment);
  data_file[43] = '\0';
  snprintf (results_file, 46, "data/Halasz-et-al-2016/%s_results.tsv",
            experiment);
  results_file[46] = '\0';

  init_args_Halasz_et_al_2016 (&args);

  n = ((args.samples-args.burnin)/args.thinning)*args.runs;

  data_stream = fopen (data_file, "r");
  if (!data_stream)
    {
      error (EXIT_FAILURE, errno, "Failed to open data file");
    }
  read_narrow_table (data_stream, args.delim, false, 2, DATA_TYPE_FLOAT, &response,
                DATA_TYPE_BOOL, &perts);
  A_init = net_init_file (response, "data/Halasz-et-al-2016/init-network.tsv",
                          args.delim);
  bmra_main_loop (response, perts, A_init, A_prior, &A_all, &r_all,
                  &lp_A_all, &lp_A_t_all, &lp_r_all, args);
  P_A = calc_matrix_means ((const datatable **) A_all, n);
  r_mean = calc_matrix_means ((const datatable **) r_all, n);
  r_std = calc_matrix_std ((const datatable **) r_all, r_mean, n);

  results_stream = fopen (results_file, "r");
  if (!results_stream)
    {
      error (EXIT_FAILURE, errno, "Failed to open results file");
    }
  read_narrow_table (results_stream, args.delim, false, 3, DATA_TYPE_FLOAT, &pub_P_A,
                DATA_TYPE_FLOAT, &pub_r_mean, DATA_TYPE_FLOAT, &pub_r_std);
  gsl_matrix_transpose (pub_P_A->data);
  gsl_matrix_transpose (pub_r_mean->data);
  gsl_matrix_transpose (pub_r_std->data);

  ck_assert_uint_eq (P_A->data->size1, pub_P_A->data->size1);
  ck_assert_uint_eq (P_A->data->size2, pub_P_A->data->size2);
  ck_assert_uint_eq (r_mean->data->size1, pub_r_mean->data->size1);
  ck_assert_uint_eq (r_mean->data->size2, pub_r_mean->data->size2);
  ck_assert_uint_eq (r_std->data->size1, pub_r_std->data->size1);
  ck_assert_uint_eq (r_std->data->size2, pub_r_std->data->size2);

  for (i=0; i<P_A->data->size1; i++)
    {
      ck_assert_str_eq (P_A->rownames[i], pub_P_A->rownames[i]);
      for (j=0; j<P_A->data->size2; j++)
        {
          if (i==0)
            ck_assert_str_eq (P_A->rownames[j], pub_P_A->rownames[j]);
          ck_assert_msg ((gsl_fcmp (gsl_matrix_get (P_A->data, i, j),
                                    gsl_matrix_get (pub_P_A->data, i, j),
                                    RESULTS_TOL)==0 ||
                         gsl_fcmp (gsl_matrix_get (P_A->data, i, j)+1.0,
                                   gsl_matrix_get (pub_P_A->data, i, j)+1.0,
                                   RESULTS_TOL)==0),
                         "%s P(A) result mismatch for %s->%s.  "
                         "Expected %f but got %f", experiment, P_A->colnames[j],
                         P_A->rownames[i], gsl_matrix_get (pub_P_A->data, i, j),
                         gsl_matrix_get (P_A->data, i, j));
          ck_assert_msg ((gsl_fcmp (gsl_matrix_get (r_mean->data, i, j),
                                   gsl_matrix_get (pub_r_mean->data, i, j),
                                   RESULTS_TOL)==0) ||
                         ((gsl_fcmp (abs (gsl_matrix_get (r_mean->data, i, j)),
                                    abs (gsl_matrix_get (pub_r_mean->data, i, j)),
                                   RESULTS_TOL)==0) &&
                          abs (gsl_matrix_get (r_mean->data, i, j) - gsl_matrix_get (pub_r_mean->data, i, j)) < 0.01),
                         "%s mean(r) result mismatch for %s->%s.  "
                         "Expected %f but got %f", experiment, r_mean->colnames[j],
                         r_mean->rownames[i], gsl_matrix_get (pub_r_mean->data, i, j),
                         gsl_matrix_get (r_mean->data, i, j));
          ck_assert_msg (gsl_fcmp (gsl_matrix_get (r_std->data, i, j),
                                   gsl_matrix_get (pub_r_std->data, i, j),
                                   RESULTS_TOL)==0,
                         "%s std(r) result mismatch for %s->%s.  "
                         "Expected %f but got %f", experiment, r_std->colnames[j],
                         r_std->rownames[i], gsl_matrix_get (pub_r_std->data, i, j),
                         gsl_matrix_get (r_std->data, i, j));
        }
    }

  datatable_free (response);
  datatable_free (perts);
  datatable_free (A_init);
  datatable_free (P_A);
  datatable_free (r_mean);
  datatable_free (r_std);
  datatable_free (pub_P_A);
  datatable_free (pub_r_mean);
  datatable_free (pub_r_std);
  for (i=0; i<n; i++)
    {
      datatable_free (A_all[i]);
      datatable_free (r_all[i]);
    }
  free (A_all);
  free (r_all);
}


START_TEST(test_hct116_igf_results)
{
  run_results_tests ("hct116_igf");
}
END_TEST


START_TEST(test_hct116_tgf_results)
{
  run_results_tests ("hct116_tgf");
}
END_TEST


START_TEST(test_ht29_igf_results)
{
  run_results_tests ("ht29_igf");
}
END_TEST


START_TEST(test_ht29_tgf_results)
{
  run_results_tests ("ht29_tgf");
}
END_TEST


START_TEST(test_lm1215_igf_results)
{
  run_results_tests ("lm1215_igf");
}
END_TEST


START_TEST(test_lm1215_tgf_results)
{
  run_results_tests ("lm1215_tgf");
}
END_TEST


START_TEST(test_sw403_igf_results)
{
  run_results_tests ("sw403_igf");
}
END_TEST


START_TEST(test_sw403_tgf_results)
{
  run_results_tests ("sw403_tgf");
}
END_TEST


Suite *
bmra_suite (void)
{
  Suite *s;
  TCase *tc_lp_A;
  TCase *tc_util;
  TCase *tc_results;

  s = suite_create ("bmra");

  tc_lp_A = tcase_create ("lp_A");
  tcase_add_test (tc_lp_A, test_lp_A_comp_V1_1d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_V1_2d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_V1_0d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_Vb_1_1d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_Vb_1_2d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_Vb_1_0d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_Vs_1_1d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_Vs_1_2d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_Vs_1_0d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_Vs_1d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_Vs_2d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_Vs_0d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_mus_1d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_mus_2d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_mus_0d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_bs_1d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_bs_2d);
  tcase_add_test (tc_lp_A, test_lp_A_comp_bs_0d);
  tcase_add_test (tc_lp_A, test_p_Ai_from_prior);
  tcase_add_test (tc_lp_A, test_p_Ai_from_prior_w_gamma);
  tcase_add_test (tc_lp_A, test_lp_A_given_R_1d);
  tcase_add_test (tc_lp_A, test_lp_A_given_R_2d);
  tcase_add_test (tc_lp_A, test_lp_A_given_R_0d);
  tcase_add_test (tc_lp_A, test_sample_mini);
  tcase_add_test (tc_lp_A, test_sample_r_st_1d);
  tcase_add_test (tc_lp_A, test_sample_r_st_2d);
  tcase_add_test (tc_lp_A, test_sample_r_st_0d);
  suite_add_tcase (s, tc_lp_A);

  tc_util = tcase_create ("utils");
  tcase_add_test (tc_util, test_sub_matrix_by_row);
  tcase_add_test (tc_util, test_sub_matrix_by_col);
  tcase_add_test (tc_util, test_remove_edge);
  tcase_add_test (tc_util, test_add_edge_from_A0);
  tcase_add_test (tc_util, test_add_edge);
  tcase_add_test (tc_util, test_gen_A_init);
  tcase_add_test (tc_util, test_propose_A_new);
  tcase_add_test (tc_util, test_calc_matrix_means);
  tcase_add_test (tc_util, test_calc_matrix_std);
  tcase_add_test (tc_util, test_hamming_distance);
  suite_add_tcase (s, tc_util);

  tc_results = tcase_create ("results");
  tcase_add_test (tc_results, test_hct116_igf_results);
  tcase_add_test (tc_results, test_hct116_tgf_results);
  tcase_add_test (tc_results, test_ht29_igf_results);
  tcase_add_test (tc_results, test_ht29_tgf_results);
  tcase_add_test (tc_results, test_lm1215_igf_results);
  tcase_add_test (tc_results, test_lm1215_tgf_results);
  tcase_add_test (tc_results, test_sw403_igf_results);
  tcase_add_test (tc_results, test_sw403_tgf_results);
  suite_add_tcase (s, tc_results);

  return s;
}


int main (void)
{
  int n_fail;
  Suite *s;
  SRunner *sr;
  const gsl_rng_type *rng_type;

  /* Random number generator setup */
  gsl_rng_env_setup();
  rng_type = gsl_rng_default;
  rng = gsl_rng_alloc (rng_type);

  s = bmra_suite ();
  sr = srunner_create (s);

  srunner_run_all (sr, CK_NORMAL);
  n_fail = srunner_ntests_failed (sr);
  srunner_free (sr);
  return (n_fail == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
