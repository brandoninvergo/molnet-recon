/* 
 * readtable.c --- 
 * 
 * Copyright (C) 2017, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include "readtable.h"


struct index_count
{
  char *name;
  size_t count;
  UT_hash_handle hh;
};


void
prepare_data_columns (va_list arg_list, enum data_type **table_types,
                      void ****tables, size_t num_data_cols)
{
  size_t i;
  for (i=0; i<num_data_cols*2; i++)
    {
      switch (i % 2)
        {
        case 0:
          (*table_types)[i/2] = va_arg (arg_list, enum data_type);
          break;
        case 1:
          switch ((*table_types)[i/2])
            {
            case DATA_TYPE_FLOAT:
              (*tables)[i/2] = (void **) va_arg (arg_list, datatable**);
              break;
            case DATA_TYPE_INT:
              (*tables)[i/2] = (void **) va_arg (arg_list, datatable_int**);
              break;
            case DATA_TYPE_BOOL:
              (*tables)[i/2] = (void **) va_arg (arg_list, datatable**);
              break;
            }
        }
    }

}


void
update_index_count (struct index_count **hash, const char *index)
{
  struct index_count *item;
  HASH_FIND_STR(*hash, index, item);
  if (!item)
    {
      item = (struct index_count *) safe_malloc (sizeof (struct index_count));
      item->name = strdup (index);
      if (!item->name)
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      item->count = 1;
      HASH_ADD_KEYPTR(hh, *hash, item->name, strlen (item->name), item);
    }
  else
    {
      item->count++;
    }
}


bool
index_count_is_sane (struct index_count *hash)
{
    struct index_count *item, *tmp;
    size_t n = 0;

    HASH_ITER(hh, hash, item, tmp)
    {
      if (n == 0)
        {
          n = item->count;
        }
      else if (item->count != n)
        {
          error (0, 0, "Wrong number of instances of index '%s' (%zd != %zd)", item->name,
                 item->count, n);
          return false;
        }
    }
    return true;
}


void
build_table_indices (FILE *table_stream, char ***index1, char ***index2,
                     size_t *rows, size_t *cols, bool header, char *delim)
{
  struct index_count *row_hash = NULL, *col_hash = NULL, *item, *tmp;
  char *line = NULL, *index1_col = NULL, *index2_col = NULL;
  size_t line_len = 0;
  size_t i = 0, j = 0;

  /* We don't need the header.  Chomp it. */
  if (header)
    getline (&line, &line_len, table_stream);

  while (getline (&line, &line_len, table_stream) > 0)
    {
      /* The first token is a row index */
      index1_col = strtok (line, delim);
      if (!index1_col)
        {
          error (EXIT_FAILURE, errno, "Invalid narrow table format. Use: "
                 "INDEX1%sINDEX2%sVALUE", delim, delim);
        }
      update_index_count (&row_hash, index1_col);

      /* The second token is a column index */
      index2_col = strtok (NULL, delim);
      if (!index2_col)
        {
          error (EXIT_FAILURE, errno, "Invalid narrow table format. Use: "
                 "INDEX1%sINDEX2%sVALUE", delim, delim);
        }
      update_index_count (&col_hash, index2_col);
    }

  /* Get the total number of rows and columns */
  *rows = HASH_COUNT(row_hash);
  *cols = HASH_COUNT(col_hash);

  if (*rows == 0 || *cols == 0)
    error (EXIT_FAILURE, errno, "Empty data file?");

  /* Check that there aren't any extraneous data points */
  if (!index_count_is_sane (row_hash) || !index_count_is_sane (col_hash))
    {
      error (EXIT_FAILURE, errno, "Data table is not rectangular.");
    }

  /* Fill the indices */
  *index1 = (char **) safe_malloc (*rows * sizeof (char *));
  *index2 = (char **) safe_malloc (*cols * sizeof (char *));
  for (item=row_hash; item != NULL; item=item->hh.next)
    {
      (*index1)[i++] = item->name;
    }
  for (item=col_hash; item != NULL; item=item->hh.next)
    {
      (*index2)[j++] = item->name;
    }

  /* Clean up */
  HASH_ITER(hh, row_hash, item, tmp)
    {
      HASH_DEL(row_hash, item);
      free (item);
    }
  HASH_ITER(hh, col_hash, item, tmp)
    {
      HASH_DEL(col_hash, item);
      free (item);
    }
}


int
read_narrow_table (FILE *table_stream, char *delim, bool header,
                   size_t num_data_cols, ...)
{
  char **index1, **index2;
  char *row_index, *col_index;
  char *line = NULL;
  size_t line_len = 0;
  char *col = NULL;
  size_t index1_n = 0, index2_n = 0;
  va_list arg_list;
  void ***tables;
  enum data_type *table_types;
  size_t i;
  double tmp_bool_val;

  table_types = (enum data_type *) safe_malloc (num_data_cols *
                                                sizeof (enum data_type));
  tables = (void ***) safe_malloc (num_data_cols * sizeof (void ***));

  /* Set up the separate tables for each data column. */
  va_start (arg_list, num_data_cols);
  prepare_data_columns (arg_list, &table_types, &tables, num_data_cols);
  va_end (arg_list);

  /* Determine the dimensions of the data columns by scanning through
     the file and counting the number of unique row (input column 1) and column
     (input column 2) indices */
  build_table_indices (table_stream, &index1, &index2, &index1_n, &index2_n,
                       header, delim);

  for (i=0; i<num_data_cols; i++)
    {
      switch (table_types[i])
        {
        case DATA_TYPE_FLOAT:
          (*tables)[i] = datatable_alloc (index1_n, index2_n, index1, index2);
          break;
        case DATA_TYPE_INT:
          (*tables)[i] = datatable_int_alloc (index1_n, index2_n, index1, index2);
          break;
        case DATA_TYPE_BOOL:
          (*tables)[i] = datatable_alloc (index1_n, index2_n, index1, index2);
          break;
        }
    }
  /* Rewind the file stream */
  if (fseek (table_stream, 0L, SEEK_SET))
    {
      error (EXIT_FAILURE, errno, "Could not get back to the beginning of the "
             "data file");
    }
  /* Fill the data tables */
  /* We don't need the header.  Chomp it. */
  if (header)
    getline (&line, &line_len, table_stream);
  while (getline (&line, &line_len, table_stream) > 0)
    {
      /* Drop the newline */
      line[strcspn (line, "\n")] = '\0';
      /* Get the record row-column index */
      row_index = strtok (line, delim);
      col_index = strtok (NULL, delim);

      i = 0;
      col = strtok (NULL, delim);
      while (col)
        {
          switch (table_types[i])
            {
            case DATA_TYPE_FLOAT:
              datatable_set (*((datatable **) tables[i]), row_index, col_index,
                             strtod (col, NULL));
              break;
            case DATA_TYPE_INT:
              datatable_int_set (*((datatable_int **) tables[i]), row_index,
                                 col_index, strtol (col, NULL, 0));
              break;
            case DATA_TYPE_BOOL:
              if (!strcmp (col, "TRUE"))
                tmp_bool_val = 1.0;
              else if (!strcmp (col, "FALSE"))
                tmp_bool_val = 0.0;
              else
                error (EXIT_FAILURE, 0, "Boolean columns should only contain"
                       " \"TRUE\" and \"FALSE\" values");
              datatable_set (*((datatable **) tables[i]), row_index, col_index,
                             tmp_bool_val);
              break;
            }
          col = strtok (NULL, delim);
          i++;
        }
      if (i != num_data_cols)
        {
          fclose (table_stream);
          free (index1);
          free (index2);
          for (i=0; i<num_data_cols; i++)
            {
              switch (table_types[i])
                {
                  case DATA_TYPE_FLOAT:
                    datatable_free ((*tables)[i]);
                    break;
                  case DATA_TYPE_INT:
                    datatable_int_free ((*tables)[i]);
                    break;
                  case DATA_TYPE_BOOL:
                    datatable_free ((*tables)[i]);
                    break;
                }
            }
          free (tables);
          error (EXIT_FAILURE, 0, "Invalid data format. "
                 "Expected %zd data columns, found %zd.  See the manual "
                 "for more information.",
                 num_data_cols, i);
        }
    }
  if (fclose (table_stream))
    {
      error (EXIT_FAILURE, errno, "Failed to close data file");
    }
  for (size_t i=0; i<index1_n; i++)
    free (index1[i]);
  free (index1);
  for (size_t i=0; i<index2_n; i++)
    free (index2[i]);
  free (index2);
  free (tables);
  free (line);
  free (table_types);
}
