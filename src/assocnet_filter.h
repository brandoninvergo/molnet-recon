/* 
 * assocnet_filter.h --- 
 * 
 * Copyright (C) 2017, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef ASSOCNET_FILTER_H
#define ASSOCNET_FILTER_H

#include "config.h"

#include <stdbool.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_sort_vector.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_linalg.h>
#include <math.h>
#include "safe_alloc.h"
#include "datatable.h"

enum filter_method {FILTER_METHOD_DECONV};
enum scale_method {SCALE_METHOD_NONE, SCALE_METHOD_MINMAX, SCALE_METHOD_ABS};

struct arguments
{
  char *args[1];
  enum filter_method filter;
  enum scale_method pre_scale;
  enum scale_method post_scale;
  char *delim;
  double deconv_alpha;
  double deconv_beta;
  bool header_in;
  bool observed_only;
  bool scale_diag;
};

void scale_matrix_minmax (gsl_matrix *m, gsl_matrix *scaled_m,
                            bool scale_diag);
void scale_matrix_abs (const gsl_matrix *m, gsl_matrix *scaled_m,
                       bool scale_diag);
void remove_diagonal (const gsl_matrix *in_mat, gsl_matrix *out_mat);
void threshold_matrix (const gsl_matrix *in_mat, gsl_matrix *out_mat,
                       double alpha);
void deconvolve_matrix (const gsl_matrix *mat, gsl_matrix *deconv_mat,
                        double beta);
void filter_network_deconvolution (gsl_matrix *corr, gsl_matrix *filtered,
                                   struct arguments args);
int assocnet_filter_main (const datatable *data, datatable **filtered, 
                          struct arguments args);
void output_results (const datatable *filtered, struct arguments args);

#endif
