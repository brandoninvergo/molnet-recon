/* 
 * assocnet.h --- 
 * 
 * Copyright (C) 2017, 2018, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef ASSOCNET_H
#define ASSOCNET_H

#include "config.h"

#include <stdbool.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_int.h>
#include <gsl/gsl_matrix_uint.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_permute_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_sort_vector.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_statistics.h>
#include <math.h>
#include "safe_alloc.h"
#include "datatable.h"

enum assoc_method {ASSOC_METHOD_PEARSON, ASSOC_METHOD_SPEARMAN, ASSOC_METHOD_MI};
enum p_method {P_METHOD_NONE, P_METHOD_T, P_METHOD_Z, P_METHOD_BOOTSTRAP};

struct arguments
{
  char *args[1];
  enum assoc_method method;
  enum p_method pmethod;
  char *delim;
  bool unbiased;
  size_t bootstrap_n;
  bool header_in;
};

void bin_data_even (const gsl_matrix *data, gsl_matrix_int *bins,
                    size_t num_bins);
void bin_data_quantiles (const gsl_matrix *data, gsl_matrix_int *bins,
                         size_t num_bins);
void calc_cov_matrix (const gsl_matrix *data, gsl_matrix *dev, gsl_matrix *cov);
void calc_corr_matrix_pearson (const gsl_matrix *data, gsl_matrix *corr);
void rank_matrix_rows (const gsl_matrix *data, gsl_matrix *ranks);
void calc_corr_matrix_spearman (const gsl_matrix *data, gsl_matrix *corr);
void contingency_table (const gsl_matrix_int *data, gsl_matrix *counts, size_t i,
                        size_t j, size_t min_bin);
void calc_marg_probs (const gsl_matrix *joint_probs, gsl_vector *marg_probs1,
                      gsl_vector *marg_probs2);
double calc_entropy (const gsl_vector *probs);
double calc_joint_entropy (const gsl_matrix *joint_probs);
void calc_mi_matrix (const gsl_matrix_int *data, gsl_matrix *mi);
void unbiased_corr (gsl_matrix *corr, size_t n);
void calc_corr_p (gsl_matrix *data, gsl_matrix *corr, gsl_matrix *p,
                  struct arguments args);
int assocnet_main (const datatable *data, datatable **corr, datatable **pvals,
                   struct arguments args);
int assocnet_main_discr (const datatable_int *data, datatable **corr,
                         datatable **pvals, struct arguments args);
void output_results (const datatable *corr, const datatable *pvals,
                     struct arguments args);

#endif
