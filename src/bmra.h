/* 
 * bmra.h --- 
 * 
 * Copyright (C) 2017 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef BMRA_H
#define BMRA_H

#include "config.h"

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "error.h"
#include <errno.h>
#include <math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_char.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_sys.h>
#include <gsl/gsl_math.h>
#include <omp.h>
#include "datatable.h"
#include "readtable.h"
#include "mvt.h"
#include "safe_alloc.h"

enum net_init {NET_INIT_EMPTY, NET_INIT_FULL, NET_INIT_FILE, NET_INIT_RANDOM};
enum edge_prior_type {EDGE_PRIOR_HAMMING, EDGE_PRIOR_PROB, EDGE_PRIOR_BETA,
                      EDGE_PRIOR_BETA_HAMMING};
enum sampler_type {SAMPLER_MCMC, SAMPLER_GIBBS, SAMPLER_EXHAUSTIVE};
enum zellner_g_method {ZELLNER_G_RIC, ZELLNER_G_UIP, ZELLNER_G_BENCH};
enum trace_vals {TRACE_NONE = 0,
                 TRACE_LPA = (1u << 0),
                 TRACE_LPA_T = (1u << 1),
                 TRACE_LPR = (1u << 2)};
enum lpa_method {LPA_METHOD_WRITTEN, LPA_METHOD_IMPLEMENTED, LPA_METHOD_FULL};

struct arguments
{
  char *args[1];
  enum net_init init;
  char *init_file;
  enum edge_prior_type edge_prior;
  enum sampler_type sampler;
  size_t samples;
  size_t burnin;
  size_t runs;
  size_t thinning;
  bool subnet_only;
  char *delim;
  double hamming_psi;
  double beta_a;
  double beta_b;
  double lambda;
  double igamma_a;
  double igamma_b;
  double prob_gamma;
  enum zellner_g_method zellner_g;
  enum trace_vals trace;
  enum lpa_method lpa;
  char *trace_dir;
  bool header_in;
};

gsl_matrix *sub_matrix_by_col (gsl_matrix *source, gsl_vector *index);
gsl_matrix *sub_matrix_by_row (gsl_matrix *source, const gsl_vector *index);
void remove_edge (gsl_vector *A, gsl_rng *rng);
void add_edge_from_A0 (gsl_vector *A, const gsl_vector *A0, size_t node,
                       gsl_rng *rng);
void add_edge (gsl_vector *A, size_t node, gsl_rng *rng);
gsl_vector *gen_A_init (const gsl_vector *A0, size_t nkmax, size_t node,
                        gsl_rng *rng, struct arguments args);
gsl_vector *propose_A_new (const gsl_vector *A, const gsl_vector *A0, size_t nkmax,
                    size_t node, gsl_rng *rng, struct arguments args);
gsl_matrix *lp_A_comp_V1 (gsl_matrix *resp_var);
gsl_matrix *lp_A_comp_Vb_1 (gsl_matrix *V1, double c, struct arguments args);
gsl_matrix *lp_A_comp_Vs_1 (gsl_matrix *V1, gsl_matrix *Vb_1);
gsl_matrix *lp_A_comp_Vs (gsl_matrix *Vs_1);
gsl_vector *lp_A_comp_mus (gsl_matrix *resp_var, const gsl_vector *resp_sub_i,
                           gsl_matrix *Vs);
double lp_A_comp_bs (const gsl_vector *resp_sub_i, gsl_vector *mus,
                     gsl_matrix *Vs_1, struct arguments args);
double hamming_distance (const gsl_vector *A, const gsl_vector *B);
double p_Ai_from_prior (const gsl_vector *A, const gsl_vector *A_prior,
                        size_t node, struct arguments args);
double lp_A_given_R (gsl_matrix *resp_var, gsl_vector *resp_sub_i,
                     const gsl_vector *Ai, const gsl_vector *A,
                     const datatable *A_prior, size_t node, double *bs,
                     gsl_matrix **Vs, gsl_vector **mus, struct arguments args);
bool sample_mini (double lp_A_alt, double lp_A, gsl_rng *rng);
double sample_r_st (double bs, double p, double n, const gsl_matrix *Vs,
                    const gsl_vector *mus, gsl_vector *r, gsl_rng *rng,
                    struct arguments args);
void output_results (const datatable *P_A, const datatable *r_mean,
                     const datatable *r_std, struct arguments args);
datatable *calc_matrix_means (const datatable **m_all, size_t n);
datatable *calc_matrix_std (const datatable **m_all, const datatable *m_mean,
                            size_t n);
int bmra_main_loop (const datatable *response, const datatable *perts,
                    const datatable *A_init, const datatable *A_prior,
                    datatable ***A_all, datatable ***r_all, gsl_matrix **lp_A_all,
                    gsl_matrix **lp_A_t_all, gsl_matrix **lp_r_all,
                    struct arguments args);
datatable *net_init_empty (const datatable *response);
datatable *net_init_full (const datatable *response);
datatable *net_init_random (const datatable *response, struct arguments args);
datatable *net_init_file (const datatable *response, char *edge_file, char *delim);
datatable *net_prior (const datatable *response, datatable *A_init,
                      char *edge_file, char *delim);

#endif
