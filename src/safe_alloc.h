/* 
 * safe_alloc.h --- 
 * 
 * Copyright (C) 2017, 2018, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef SAFE_ALLOC_H
#define SAFE_ALLOC_H

#include "error.h"
#include <errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_int.h>
#include <gsl/gsl_vector.h>

void *safe_malloc (size_t n);
gsl_matrix *safe_matrix_alloc (size_t i, size_t j);
gsl_matrix_int *safe_matrix_int_alloc (size_t i, size_t j);
gsl_matrix *safe_matrix_calloc (size_t i, size_t j);
gsl_matrix_int *safe_matrix_int_calloc (size_t i, size_t j);
gsl_vector *safe_vector_alloc (size_t i);
gsl_vector *safe_vector_calloc (size_t i);

#endif
