/*
 * bindata.h --- 
 * 
 * Copyright (C) 2018, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DISCRETIZE_H
#define DISCRETIZE_H

#include <stdbool.h>
#include <string.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_int.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_statistics.h>
#include <omp.h>
#include "datatable.h"

enum bin_method {BIN_EVEN, BIN_QUANTILES};

struct arguments
{
  char *args[1];
  enum bin_method bin_method;
  char *delim;
  size_t bin_n;
  bool header_in;
};

void bin_data (const gsl_matrix *data, gsl_matrix_int *bins, size_t num_bins,
               enum bin_method method);
void output_results (const datatable_int *bins, struct arguments args);

#endif
