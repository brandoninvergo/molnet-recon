/* 
 * mra.c --- 
 * 
 * Copyright (C) 2018 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "mra.h"

void
mra (const gsl_matrix *response, gsl_matrix *intxns)
{
  gsl_matrix *resp_LU;
  gsl_matrix *resp_inv;
  gsl_matrix *resp_inv_diag_m;
  gsl_matrix *resp_inv_diag_inv;
  gsl_permutation *perm, *perm2;
  int signum, signum2;

  if (response->size1 != response->size2)
    error (EXIT_FAILURE, 0, "MRA only supports square matrices.");

  resp_LU = safe_matrix_alloc (response->size1, response->size2);
  gsl_matrix_memcpy (resp_LU, response);
  perm = gsl_permutation_alloc (response->size1);
  if (!perm)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory.");
    }
  gsl_linalg_LU_decomp (resp_LU, perm, &signum);
  resp_inv = safe_matrix_alloc (response->size1, response->size2);

  /* R_p^-1, following the notation in Kholodenko et al 2002 */
  gsl_linalg_LU_invert (resp_LU, perm, resp_inv);

  /* dg(R_p^-1) */
  resp_inv_diag_m = safe_matrix_alloc (response->size1, response->size2);
  gsl_matrix_set_identity (resp_inv_diag_m);
  gsl_matrix_mul_elements (resp_inv_diag_m, resp_inv);

  /* [dg(R_p^-1)]^-1 */
  perm2 = gsl_permutation_alloc (response->size1);
  if (!perm2)
    {
      error (EXIT_FAILURE, 0, "Failed to allocate memory.");
    }
  gsl_linalg_LU_decomp (resp_inv_diag_m, perm2, &signum2);
  resp_inv_diag_inv = safe_matrix_alloc (response->size1, response->size2);
  gsl_linalg_LU_invert (resp_inv_diag_m, perm2, resp_inv_diag_inv);

  /* -[dg(R_p^-1)]^-2 x R_p^-1 */
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, -1.0, resp_inv_diag_inv, resp_inv, 0.0, intxns);

  gsl_permutation_free (perm);
  gsl_permutation_free (perm2);
  gsl_matrix_free (resp_LU);
  gsl_matrix_free (resp_inv);
  gsl_matrix_free (resp_inv_diag_m);
  gsl_matrix_free (resp_inv_diag_inv);
  return;
}
