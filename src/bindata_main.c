/* 
 * bindata-main.c --- 
 * 
 * Copyright (C) 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "argp.h"
#include "error.h"
#include <errno.h>
#include <sys/stat.h>
#include <libgen.h>
#include <gsl/gsl_matrix.h>

#include "bindata.h"
#include "datatable.h"
#include "readtable.h"


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "bindata -- discretize a table of continuous data"
  "\v"
  "Input data should be in a 2-column \"narrow\" format: the first column "
  "contains the row names (e.g. node names), the second column contains the "
  "column names (e.g. condition names), and the third column contains the data "
  "(e.g. node value under that condition)."
  "\n\n"
  "If a data file is not provided, the data will be read from stdin.";

static char args_doc[] = "DATA";

static struct argp_option options[] = {
  {0, 0, 0, 0, "Data input", 1},
  {"delim", 'd', "CHAR", 0, "Column delimiter (default: \\t)", 0},
  {"header-in", 'h', 0, 0, "Data contains a header row", 0},
  {0, 0, 0, 0, "Methods", 2},
  {"method", 'm', "METHOD", 0, "Discretization method: 'even' (into"
   " evenly-spaced bins), 'quantile' (into evenly-sized bins by quantiles)"
   " (default: 'even')", 0},
  {0, 0, 0, 0, "Parameters", 3},
  {"num-bins", 'n', "NUMBER", 0, "Number of bins (default: 5)", 0},
  {0}
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char *tail;
  struct arguments *arguments = state->input;
  switch (key)
    {
    case 'd':
      if (strlen (arg) != 1)
        argp_usage (state);
      arguments->delim = arg;
      break;
    case 'h':
      arguments->header_in = true;
      break;
    case 'm':
      if (strlen (arg) == 0)
        argp_usage (state);
      if (!strcmp(arg, "even"))
        arguments->bin_method = BIN_EVEN;
      else if (!strcmp(arg, "quantile"))
        arguments->bin_method = BIN_QUANTILES;
      else
        argp_usage (state);
      break;
    case 'n':
      errno = 0;
      arguments->bin_n = (size_t) strtoul (arg, &tail, 0);
      if (arguments->bin_n == 0)
        argp_usage (state);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid number of bins");
        }
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num > 1)
        argp_usage (state);
      arguments->args[0] = arg;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};


void
init_args (struct arguments *arguments)
{
  arguments->bin_method = BIN_EVEN;
  arguments->delim = "\t";
  arguments->args[0] = NULL;
  arguments->header_in = false;
  arguments->bin_n = 5;
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  datatable *data = NULL;
  datatable_int *bins = NULL;
  FILE *table_stream;

  init_args (&arguments);
  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  if (arguments.args[0] == NULL)
    {
      table_stream = stdin;
    }
  else
    {
        table_stream = fopen (arguments.args[0], "r");
        if (!table_stream)
          {
            error (EXIT_FAILURE, errno, "Failed to open file");
          }
    }
  read_narrow_table (table_stream, arguments.delim, arguments.header_in,
                     1, DATA_TYPE_FLOAT, &data);
  bins = datatable_int_alloc (data->data->size1, data->data->size2,
                              data->rownames, data->colnames);

  bin_data (data->data, bins->data, arguments.bin_n, arguments.bin_method);
  output_results (bins, arguments);

  datatable_free (data);
  datatable_int_free (bins);
  exit (EXIT_SUCCESS);
}
