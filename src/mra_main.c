/* 
 * mra-main.c --- 
 * 
 * Copyright (C) 2018 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "argp.h"
#include "error.h"
#include <errno.h>
#include <sys/stat.h>
#include <libgen.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_matrix.h>

#include "mra.h"
#include "datatable.h"
#include "readtable.h"


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "mra -- reconstruct a network by Modular Response Analysis"
  "\v"
  "Input data should be in a 3-column \"narrow\" format: the first column "
  "contains the row names (e.g. node names), the second column contains the "
  "column names (e.g. condition names), and the third column contains the data "
  "(e.g. node value under that condition).  The conditions are assumed to "
  "appear in the same order as the nodes they perturb; i.e. the perturbation "
  "in column i should be a direct perturbation of the node in row i."
  "\n\n"
  "If a data file is not provided, the data will be read from stdin.";

static char args_doc[] = "DATA";

static struct argp_option options[] = {
  {0, 0, 0, 0, "Data input", 1},
  {"delim", 'd', "CHAR", 0, "Column delimiter (default: \\t)", 0},
  {"header-in", 'h', 0, 0, "Data contains a header row", 0},
  {0},
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch (key)
    {
    case 'd':
      if (strlen (arg) == 0)
        argp_usage (state);
      arguments->delim = arg;
      break;
    case 'h':
      arguments->header_in = true;
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num > 1)
        argp_usage (state);
      arguments->args[0] = arg;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};


void
init_args (struct arguments *arguments)
{
  arguments->delim = "\t";
  arguments->header_in = false;
}

void
output_results (const datatable *intxns, struct arguments args)
{
  size_t i, j;
  char *node1, *node2;
  double r;
  printf ("node1%snode2%sr\n", args.delim, args.delim);
  for (i=0; i<intxns->data->size2; i++)
    {
      node1 = intxns->colnames[i];
      for (j=0; j<intxns->data->size1; j++)
        {
          node2 = intxns->rownames[j];
          r = gsl_matrix_get (intxns->data, j, i);
          printf ("%s%s%s%s%f\n", node1, args.delim, node2, args.delim, r);
        }
    }
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  datatable *response = NULL;
  datatable *intxns = NULL;
  FILE *table_stream;
  size_t i, n;

  init_args (&arguments);
  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  if (arguments.args[0] == NULL)
    {
      table_stream = stdin;
    }
  else
    {
        table_stream = fopen (arguments.args[0], "r");
        if (!table_stream)
          {
            error (EXIT_FAILURE, errno, "Failed to open response file");
          }
    }
  read_narrow_table (table_stream, arguments.delim, arguments.header_in, 1,
                DATA_TYPE_FLOAT, &response);

  intxns = datatable_alloc (response->data->size1, response->data->size1,
                            response->rownames, response->rownames);

  mra (response->data, intxns->data);
  output_results (intxns, arguments);

  datatable_free (response);
  datatable_free (intxns);
  exit (EXIT_SUCCESS);
}
