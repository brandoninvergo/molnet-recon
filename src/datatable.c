/* 
 * datatable.c --- 
 * 
 * Copyright (C) 2017 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include "datatable.h"


datatable*
datatable_alloc (size_t size1, size_t size2, char **index1, char **index2)
{
  datatable *table;
  size_t len;
  struct dim_header *header;
  table = (datatable *) malloc (sizeof (datatable));
  if (!table)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  table->data = gsl_matrix_alloc (size1, size2);
  if (!table->data)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  table->rownames = (char **) malloc (size1 * sizeof (char *));
  if (!table->rownames)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  table->row_hash = NULL;
  for (size_t i=0; i<size1; i++)
    {
      len = strlen (index1[i]);
      table->rownames[i] = (char *) malloc ((len+1)*sizeof (char));
      if (!table->rownames[i])
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      strncpy (table->rownames[i], index1[i], len);
      table->rownames[i][len] = '\0';
      header = (struct dim_header *) malloc (sizeof (struct dim_header));
      if (!header)
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      header->name = table->rownames[i];
      header->index = i;
      HASH_ADD_KEYPTR(hh, table->row_hash, header->name, strlen (header->name), header);
    }
  table->colnames = (char **) malloc (size2 * sizeof (char *));
  if (!table->colnames)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  table->col_hash = NULL;
  for (size_t i=0; i<size2; i++)
    {
      len = strlen (index2[i]);
      table->colnames[i] = (char *) malloc ((len+1)*sizeof (char));
      if (!table->colnames[i])
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      strncpy (table->colnames[i], index2[i], len);
      table->colnames[i][len] = '\0';
      header = (struct dim_header *) malloc (sizeof (struct dim_header));
      if (!header)
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      header->name = table->colnames[i];
      header->index = i;
      HASH_ADD_KEYPTR(hh, table->col_hash, header->name, strlen (header->name), header);
    }
  return (table);
}


void
datatable_free (datatable *table)
{
  struct dim_header *header, *tmp;
  for (size_t i=0; i<table->data->size1; i++)
    {
      free (table->rownames[i]);
    }
  free (table->rownames);
  for (size_t i=0; i<table->data->size2; i++)
    {
      free (table->colnames[i]);
    }
  free (table->colnames);
  gsl_matrix_free (table->data);
  HASH_ITER(hh, table->row_hash, header, tmp)
    {
      HASH_DEL(table->row_hash, header);
      free (header);
    }
  HASH_ITER(hh, table->col_hash, header, tmp)
    {
      HASH_DEL(table->col_hash, header);
      free (header);
    }
  free (table);
}


void
datatable_set (datatable *table, const char *row, const char *col, double value)
{
  size_t i, j;
  struct dim_header *header;
  if (!table)
    error (EXIT_FAILURE, 0, "Attempt to set in non-existent data table");
  HASH_FIND_STR(table->row_hash, row, header);
  if (!header)
    {
      error (EXIT_FAILURE, 0, "Unknown row name: %s", row);
    }
  else
    {
      i = header->index;
    }
  HASH_FIND_STR(table->col_hash, col, header);
  if (!header)
    {
      error (EXIT_FAILURE, 0, "Unknown col name: %s", col);
    }
  else
    {
      j = header->index;
    }
  gsl_matrix_set (table->data, i, j, value);
}


double
datatable_get (datatable *table, const char *row, const char *col)
{
  size_t i, j;
  struct dim_header *header;
  if (!table)
    error (EXIT_FAILURE, 0, "Attempt to fetch from non-existent data table");
  HASH_FIND_STR(table->row_hash, row, header);
  if (!header)
    {
      error (EXIT_FAILURE, 0, "Unknown row name: %s", row);
    }
  else
    {
      i = header->index;
    }
  HASH_FIND_STR(table->col_hash, col, header);
  if (!header)
    {
      error (EXIT_FAILURE, 0, "Unknown col name: %s", col);
    }
  else
    {
      j = header->index;
    }
  return (gsl_matrix_get (table->data, i, j));
}


datatable_int*
datatable_int_alloc (size_t size1, size_t size2, char **index1, char **index2)
{
  datatable_int *table;
  size_t len;
  struct dim_header *header;
  table = (datatable_int *) malloc (sizeof (datatable_int));
  if (!table)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  table->data = gsl_matrix_int_alloc (size1, size2);
  if (!table->data)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  table->rownames = (char **) malloc (size1 * sizeof (char *));
  if (!table->rownames)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  table->row_hash = NULL;
  for (size_t i=0; i<size1; i++)
    {
      len = strlen (index1[i]);
      table->rownames[i] = (char *) malloc ((len+1)*sizeof (char));
      if (!table->rownames[i])
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      strncpy (table->rownames[i], index1[i], len);
      table->rownames[i][len] = '\0';
      header = (struct dim_header *) malloc (sizeof (struct dim_header));
      if (!header)
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      header->name = table->rownames[i];
      header->index = i;
      HASH_ADD_KEYPTR(hh, table->row_hash, header->name, strlen (header->name), header);
    }
  table->colnames = (char **) malloc (size2 * sizeof (char *));
  if (!table->colnames)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  table->col_hash = NULL;
  for (size_t i=0; i<size2; i++)
    {
      len = strlen (index2[i]);
      table->colnames[i] = (char *) malloc ((len+1)*sizeof (char));
      if (!table->colnames[i])
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      strncpy (table->colnames[i], index2[i], len);
      table->colnames[i][len] = '\0';
      header = (struct dim_header *) malloc (sizeof (struct dim_header));
      if (!header)
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      header->name = table->colnames[i];
      header->index = i;
      HASH_ADD_KEYPTR(hh, table->col_hash, header->name, strlen (header->name), header);
    }
  return (table);
}


void
datatable_int_free (datatable_int *table)
{
  struct dim_header *header, *tmp;
  for (size_t i=0; i<table->data->size1; i++)
    {
      free (table->rownames[i]);
    }
  free (table->rownames);
  for (size_t i=0; i<table->data->size2; i++)
    {
      free (table->colnames[i]);
    }
  free (table->colnames);
  gsl_matrix_int_free (table->data);
  HASH_ITER(hh, table->row_hash, header, tmp)
    {
      HASH_DEL(table->row_hash, header);
      free (header);
    }
  HASH_ITER(hh, table->col_hash, header, tmp)
    {
      HASH_DEL(table->col_hash, header);
      free (header);
    }
  free (table);
}


void
datatable_int_set (datatable_int *table, const char *row, const char *col, int value)
{
  size_t i, j;
  struct dim_header *header;
  if (!table)
    error (EXIT_FAILURE, 0, "Attempt to set in non-existent data table");
  HASH_FIND_STR(table->row_hash, row, header);
  if (!header)
    {
      error (EXIT_FAILURE, 0, "Unknown row name: %s", row);
    }
  else
    {
      i = header->index;
    }
  HASH_FIND_STR(table->col_hash, col, header);
  if (!header)
    {
      error (EXIT_FAILURE, 0, "Unknown col name: %s", col);
    }
  else
    {
      j = header->index;
    }
  gsl_matrix_int_set (table->data, i, j, value);
}


int
datatable_int_get (datatable_int *table, const char *row, const char *col)
{
  size_t i, j;
  struct dim_header *header;
  if (!table)
    error (EXIT_FAILURE, 0, "Attempt to fetch from non-existent data table");
  HASH_FIND_STR(table->row_hash, row, header);
  if (!header)
    {
      error (EXIT_FAILURE, 0, "Unknown row name: %s", row);
    }
  else
    {
      i = header->index;
    }
  HASH_FIND_STR(table->col_hash, col, header);
  if (!header)
    {
      error (EXIT_FAILURE, 0, "Unknown col name: %s", col);
    }
  else
    {
      j = header->index;
    }
  return (gsl_matrix_int_get (table->data, i, j));
}


datatable_uchar*
datatable_uchar_alloc (size_t size1, size_t size2, char **index1, char **index2)
{
  datatable_uchar *table;
  size_t len;
  struct dim_header *header;
  table = (datatable_uchar *) malloc (sizeof (datatable_uchar));
  if (!table)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  table->data = gsl_matrix_uchar_alloc (size1, size2);
  if (!table->data)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  table->rownames = (char **) malloc (size1 * sizeof (char *));
  if (!table->rownames)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  table->row_hash = NULL;
  for (size_t i=0; i<size1; i++)
    {
      len = strlen (index1[i]);
      table->rownames[i] = (char *) malloc ((len+1)*sizeof (char));
      if (!table->rownames[i])
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      strncpy (table->rownames[i], index1[i], len);
      table->rownames[i][len] = '\0';
      header = (struct dim_header *) malloc (sizeof (struct dim_header));
      if (!header)
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      header->name = table->rownames[i];
      header->index = i;
      HASH_ADD_KEYPTR(hh, table->row_hash, header->name, strlen (header->name), header);
    }
  table->colnames = (char **) malloc (size2 * sizeof (char *));
  if (!table->colnames)
    {
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  table->col_hash = NULL;
  for (size_t i=0; i<size2; i++)
    {
      len = strlen (index2[i]);
      table->colnames[i] = (char *) malloc ((len+1)*sizeof (char));
      if (!table->colnames[i])
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      strncpy (table->colnames[i], index2[i], len);
      table->colnames[i][len] = '\0';
      header = (struct dim_header *) malloc (sizeof (struct dim_header));
      if (!header)
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      header->name = table->colnames[i];
      header->index = i;
      HASH_ADD_KEYPTR(hh, table->col_hash, header->name, strlen (header->name), header);
    }
  return (table);
}


void
datatable_uchar_free (datatable_uchar *table)
{
  struct dim_header *header, *tmp;
  for (size_t i=0; i<table->data->size1; i++)
    {
      free (table->rownames[i]);
    }
  free (table->rownames);
  for (size_t i=0; i<table->data->size2; i++)
    {
      free (table->colnames[i]);
    }
  free (table->colnames);
  gsl_matrix_uchar_free (table->data);
  HASH_ITER(hh, table->row_hash, header, tmp)
    {
      HASH_DEL(table->row_hash, header);
      free (header);
    }
  HASH_ITER(hh, table->col_hash, header, tmp)
    {
      HASH_DEL(table->col_hash, header);
      free (header);
    }
  free (table);
}


void
datatable_uchar_set (datatable_uchar *table, const char *row, const char *col, char value)
{
  size_t i, j;
  struct dim_header *header;
  if (!table)
    error (EXIT_FAILURE, 0, "Attempt to set in non-existent data table");
  HASH_FIND_STR(table->row_hash, row, header);
  if (!header)
    {
      error (EXIT_FAILURE, 0, "Unknown row name: %s", row);
    }
  else
    {
      i = header->index;
    }
  HASH_FIND_STR(table->col_hash, col, header);
  if (!header)
    {
      error (EXIT_FAILURE, 0, "Unknown col name: %s", col);
    }
  else
    {
      j = header->index;
    }
  gsl_matrix_uchar_set (table->data, i, j, value);
}


char
datatable_uchar_get (datatable_uchar *table, const char *row, const char *col)
{
  size_t i, j;
  struct dim_header *header;
  if (!table)
    error (EXIT_FAILURE, 0, "Attempt to fetch from non-existent data table");
  HASH_FIND_STR(table->row_hash, row, header);
  if (!header)
    {
      error (EXIT_FAILURE, 0, "Unknown row name: %s", row);
    }
  else
    {
      i = header->index;
    }
  HASH_FIND_STR(table->col_hash, col, header);
  if (!header)
    {
      error (EXIT_FAILURE, 0, "Unknown col name: %s", col);
    }
  else
    {
      j = header->index;
    }
  return (gsl_matrix_uchar_get (table->data, i, j));
}
