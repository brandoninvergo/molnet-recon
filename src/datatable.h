/* 
 * datatable.h --- 
 * 
 * Copyright (C) 2017 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef DATATABLE_H
#define DATATABLE_H

#include "config.h"

#include "error.h"
#include <errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_int.h>
#include <gsl/gsl_matrix_uchar.h>
#include "uthash.h"

struct dim_header
{
  const char *name;
  size_t index;
  UT_hash_handle hh;
};

typedef struct datatable
{
  gsl_matrix *data;
  char **rownames;
  struct dim_header *row_hash;
  char **colnames;
  struct dim_header *col_hash;
} datatable;

typedef struct datatable_int
{
  gsl_matrix_int *data;
  char **rownames;
  struct dim_header *row_hash;
  char **colnames;
  struct dim_header *col_hash;
} datatable_int;

typedef struct datatable_uchar
{
  gsl_matrix_uchar *data;
  char **rownames;
  struct dim_header *row_hash;
  char **colnames;
  struct dim_header *col_hash;
} datatable_uchar;


datatable* datatable_alloc (size_t size1, size_t size2, char **index1,
                            char **index2);
void datatable_free (datatable *table);
void datatable_set (datatable *table, const char *row, const char *col,
                    double value);
double datatable_get (datatable *table, const char *row, const char *col);

datatable_int* datatable_int_alloc (size_t size1, size_t size2, char **index1,
                                    char **index2);
void datatable_int_free (datatable_int *table);
void datatable_int_set (datatable_int *table, const char *row, const char *col,
                        int value);
int datatable_int_get (datatable_int *table, const char *row, const char *col);

datatable_uchar* datatable_uchar_alloc (size_t size1, size_t size2, char **index1,
                                      char **index2);
void datatable_uchar_free (datatable_uchar *table);
void datatable_uchar_set (datatable_uchar *table, const char *row, const char *col,
                         char value);
char datatable_uchar_get (datatable_uchar *table, const char *row, const char *col);

#endif
