/* 
 * assocnet.c --- 
 * 
 * Copyright (C) 2017, 2018, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include "assocnet.h"


void
calc_cov_matrix (const gsl_matrix *data, gsl_matrix *dev, gsl_matrix *cov)
{
  size_t m=data->size1, n=data->size2;
  gsl_vector *ones, *row_means;
  gsl_matrix_view ones_row, row_means_m;
  gsl_matrix *row_means_rep;

  ones = safe_vector_alloc (n);
  gsl_vector_set_all (ones, 1.0);
  ones_row = gsl_matrix_view_vector (ones, 1, ones->size);
  row_means = safe_vector_alloc (m);
  row_means_rep = safe_matrix_alloc (m, n);

  gsl_blas_dgemv (CblasNoTrans, 1.0, data, ones, 0.0, row_means);
  gsl_vector_scale (row_means, 1.0/((double) data->size2));
  row_means_m = gsl_matrix_view_vector (row_means, m, 1);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, &row_means_m.matrix,
                  &ones_row.matrix, 0.0, row_means_rep);
  gsl_matrix_memcpy (dev, data);
  gsl_matrix_sub (dev, row_means_rep);
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, dev, dev, 0.0, cov);
  gsl_matrix_scale (cov, 1.0/((double) n - 1.0));

  gsl_vector_free (ones);
  gsl_vector_free (row_means);
  gsl_matrix_free (row_means_rep);
}


void
calc_corr_matrix_pearson (const gsl_matrix *data, gsl_matrix *corr)
{
  size_t i;
  size_t m=data->size1, n=data->size2;
  gsl_matrix *dev, *cov, *stdev_prod;
  gsl_vector *stdev, *ones;
  gsl_matrix_view stdev_m;

  cov = safe_matrix_alloc (m, m);
  dev = safe_matrix_alloc (m, n);
  stdev = safe_vector_alloc (m);
  stdev_prod = safe_matrix_alloc (m, m);
  ones = safe_vector_alloc (n);
  gsl_vector_set_all (ones, 1.0);

  /* Covariance */
  calc_cov_matrix (data, dev, cov);

  /* Product of standard deviations */
  gsl_matrix_mul_elements (dev, dev);
  gsl_blas_dgemv (CblasNoTrans, 1.0, dev, ones, 0.0, stdev);
  gsl_vector_scale (stdev, 1.0/((double) n - 1.0));
  for (i=0; i<stdev->size; i++)
    {
      gsl_vector_set (stdev, i, sqrt (gsl_vector_get (stdev, i)));
    }
  stdev_m = gsl_matrix_view_vector (stdev, stdev->size, 1);
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, &stdev_m.matrix,
                  &stdev_m.matrix, 0.0, stdev_prod);
  gsl_matrix_div_elements (cov, stdev_prod);
  gsl_matrix_memcpy (corr, cov);

  gsl_vector_free (ones);
  gsl_matrix_free (dev);
  gsl_matrix_free (cov);
  gsl_vector_free (stdev);
  gsl_matrix_free (stdev_prod);
}


void
rank_matrix_rows (const gsl_matrix *data, gsl_matrix *ranks)
{
  size_t i, j;
  gsl_permutation *perm, *rank;
  gsl_vector_view row;
  gsl_matrix *data_cpy;

  perm = gsl_permutation_alloc (data->size2);
  if (perm == NULL)
    error (EXIT_FAILURE, errno, "Failed to allocate permutation vector");
  rank = gsl_permutation_alloc (data->size2);
  if (rank == NULL)
    error (EXIT_FAILURE, errno, "Failed to allocate rank vector");
  data_cpy = safe_matrix_alloc (data->size1, data->size2);
  gsl_matrix_memcpy (data_cpy, data);

  for (i=0; i<data_cpy->size1; i++)
    {
      row = gsl_matrix_row (data_cpy, i);
      gsl_sort_vector_index (perm, &row.vector);
      gsl_permutation_inverse (rank, perm);
      for (j=0; j<ranks->size2; j++)
        {
          gsl_matrix_set (ranks, i, j, (double) rank->data[j]+1);
        }
    }
  gsl_permutation_free (perm);
  gsl_permutation_free (rank);
  gsl_matrix_free (data_cpy);
}


void
calc_corr_matrix_spearman (const gsl_matrix *data, gsl_matrix *corr)
{
  gsl_matrix *ranks;

  ranks = safe_matrix_alloc (data->size1, data->size2);
  rank_matrix_rows (data, ranks);
  calc_corr_matrix_pearson (ranks, corr);
  gsl_matrix_free (ranks);
}


void
contingency_table (const gsl_matrix_int *data, gsl_matrix *counts, size_t i,
                   size_t j, size_t bin_min)
{
  size_t k;

  gsl_matrix_set_zero (counts);
  for (k=0; k<data->size2; k++)
    {
      int bin1, bin2;
      bin1 = abs (gsl_matrix_int_get (data, i, k) - (int) bin_min);
      bin2 = abs (gsl_matrix_int_get (data, j, k) - (int) bin_min);
      gsl_matrix_set (counts, bin1, bin2,
                      gsl_matrix_get (counts, bin1, bin2) + 1);
    }
}


void
calc_marg_probs (const gsl_matrix *joint_probs, gsl_vector *marg_probs1,
                 gsl_vector *marg_probs2)
{
  gsl_vector *unityv;
  gsl_vector_set_zero (marg_probs1);
  gsl_vector_set_zero (marg_probs2);
  unityv = safe_vector_alloc (joint_probs->size1);
  gsl_vector_set_all (unityv, 1.0);
  gsl_blas_dgemv (CblasNoTrans, 1.0, joint_probs, unityv, 0.0, marg_probs1);
  gsl_blas_dgemv (CblasTrans, 1.0, joint_probs, unityv, 0.0, marg_probs2);
  gsl_vector_free (unityv);
}


double
calc_entropy (const gsl_vector *probs)
{
  size_t i;
  double h = 0.0;
  for (i=0; i<probs->size; i++)
    {
      double p;
      p = gsl_vector_get (probs, i);
      if (p > 0.0)
        h += p * log (p);
    }
  return -h;
}

double
calc_joint_entropy (const gsl_matrix *joint_probs)
{
  size_t i;
  double h = 0.0;
  for (i=0; i<joint_probs->size1*joint_probs->size2; i++)
    {
      double p;
      p = joint_probs->data[i];
      if (p > 0.0)
        h += p * log(p);
    }
  return -h;
}


void
calc_mi_matrix (const gsl_matrix_int *data, gsl_matrix *mi)
{
  int bin_min, bin_max;
  size_t num_bins, i, j;
  gsl_matrix *counts, *joint_probs;
  gsl_vector *marg_probs1, *marg_probs2;

  gsl_matrix_int_minmax(data, &bin_min, &bin_max);
  num_bins = (size_t) bin_max - bin_min + 1;
  counts = safe_matrix_alloc (num_bins, num_bins);
  joint_probs = safe_matrix_alloc (num_bins, num_bins);
  marg_probs1 = safe_vector_alloc (num_bins);
  marg_probs2 = safe_vector_alloc (num_bins);

  for (i=0; i<data->size1; i++)
    {
      for (j=0; j<data->size1; j++)
        {
          double h1, h2, h12, mutinf;
          contingency_table (data, counts, i, j, bin_min);
          gsl_matrix_memcpy (joint_probs, counts);
          gsl_matrix_scale (joint_probs, 1.0/((double) data->size2));
          calc_marg_probs (joint_probs, marg_probs1, marg_probs2);
          h1 = calc_entropy (marg_probs1);
          h2 = calc_entropy (marg_probs2);
          h12 = calc_joint_entropy (joint_probs);
          mutinf = h1 + h2 - h12;
          gsl_matrix_set (mi, i, j, mutinf);
        }
    }

  gsl_matrix_free (counts);
  gsl_matrix_free (joint_probs);
  gsl_vector_free (marg_probs1);
  gsl_vector_free (marg_probs2);
  return;
}


void
unbiased_corr (gsl_matrix *corr, size_t n)
{
  /* Approximate unbiased estimator of the true correlation */
  /* Olkin, I., & Pratt, J.W. (1958). Unbiased estimation of certain
     correlation coefficients.  Annals of Mathematical Statistics, 29,
     201-211 */
  gsl_matrix *corr2, *adj;

  corr2 = safe_matrix_alloc (corr->size1, corr->size2);
  adj = safe_matrix_alloc (corr->size1, corr->size2);
  gsl_matrix_memcpy (corr2, corr);
  gsl_matrix_mul_elements (corr2, corr2);
  gsl_matrix_set_all (adj, 1.0);
  gsl_matrix_sub (adj, corr2);
  gsl_matrix_scale (adj, 1.0/(2.0*((double) n - 3.0)));
  gsl_matrix_mul_elements (adj, corr);
  gsl_matrix_add (corr, adj);

  gsl_matrix_free (corr2);
  gsl_matrix_free (adj);
}


double
calc_corr_p_t (double r, double n, enum assoc_method method)
{
  double t;
  t = r*sqrt (((double) n - 2.0)/(1.0-r*r));
  return 2.0*gsl_cdf_tdist_P (-fabs (t), (double) n - 2.0);
}


double
calc_corr_p_rtoz (double r, double n, enum assoc_method method)
{
  double z;
  if (method == ASSOC_METHOD_PEARSON)
    z = sqrt ((double) n - 3.0) * atanh (r);
  else if (method == ASSOC_METHOD_SPEARMAN)
    z = sqrt (((double) n - 3.0)/1.06) * atanh (r);
  else
    return -1.0;
  return 2.0*gsl_cdf_ugaussian_P (-fabs (z));
}


void
gen_bootstrap (gsl_matrix ***bootstrap, gsl_matrix *data, struct arguments args)
{
  size_t i, n;
  gsl_matrix *data_cpy;
  gsl_vector_view row;
  gsl_permutation *perm;
  const gsl_rng_type * T;
  gsl_rng * r;

  *bootstrap = (gsl_matrix **)malloc (args.bootstrap_n * sizeof (gsl_matrix *));
  if (!(*bootstrap))
    error (EXIT_FAILURE, errno, "Failed to allocate %zd bootstraps",
           args.bootstrap_n);
  for (i=0; i<args.bootstrap_n; i++)
    (*bootstrap)[i] = safe_matrix_alloc (data->size1, data->size1);

  data_cpy = safe_matrix_alloc (data->size1, data->size2);
  perm = gsl_permutation_alloc (data->size2);
  gsl_permutation_init (perm);
  if (!perm)
    error (EXIT_FAILURE, errno, "Failed to allocate permutation vector");
  gsl_rng_env_setup();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);

  gsl_matrix_memcpy (data_cpy, data);
  for (n=0; n<args.bootstrap_n; n++)
    {
      for (i=0; i<data_cpy->size1; i++)
        {
          row = gsl_matrix_row (data_cpy, i);
          gsl_ran_shuffle (r, perm->data, data_cpy->size2, sizeof (size_t));
          gsl_permute_vector (perm, &row.vector);
        }
      switch (args.method)
        {
        case ASSOC_METHOD_PEARSON:
          calc_corr_matrix_pearson (data_cpy, (*bootstrap)[n]);
          break;
        case ASSOC_METHOD_SPEARMAN:
          calc_corr_matrix_spearman (data_cpy, (*bootstrap)[n]);
          break;
        case ASSOC_METHOD_MI:
          return;
        }
      if (args.unbiased)
        unbiased_corr ((*bootstrap)[n], data_cpy->size2);
    }

  gsl_matrix_free (data_cpy);
}


double
calc_corr_p_boot (double r, gsl_matrix **bootstrap, size_t i, size_t j, size_t n)
{
  double r_n, abs_r=fabs (r);
  size_t k, num_greater=0;

  if (bootstrap==NULL)
    error(EXIT_FAILURE, errno, "the whole damn list is missing");

  for (k=0; k<n; k++)
    {
      r_n = gsl_matrix_get (bootstrap[k], i, j);
      if (fabs (r_n) > abs_r)
        num_greater++;
    }
  return ((double) num_greater)/((double) n);
}


void
calc_corr_p (gsl_matrix *data, gsl_matrix *corr, gsl_matrix *pvals,
             struct arguments args)
{
  double r, p;
  size_t i, j, n=data->size2;
  gsl_matrix **bootstrap;

  if (args.pmethod == P_METHOD_BOOTSTRAP)
    gen_bootstrap (&bootstrap, data, args);

  for (i=0; i<corr->size1; i++)
    {
      for (j=0; j<corr->size2; j++)
        {
          r = gsl_matrix_get (corr, i, j);
          switch (args.pmethod)
            {
            case P_METHOD_T:
              p = calc_corr_p_t (r, (double) n, args.method);
              break;
            case P_METHOD_Z:
              p = calc_corr_p_rtoz (r, (double) n, args.method);
              break;
            case P_METHOD_BOOTSTRAP:
              p = calc_corr_p_boot (r, bootstrap, i, j, args.bootstrap_n);
              break;
            }
          gsl_matrix_set (pvals, i, j, p);
        }
    }

  if (args.pmethod == P_METHOD_BOOTSTRAP)
    {
      for (i=0; i<args.bootstrap_n; i++)
        gsl_matrix_free (bootstrap[i]);
      free (bootstrap);
    }
}


void
output_results (const datatable *assoc, const datatable *pvals,
                struct arguments args)
{
  size_t i, j;
  char *node1, *node2;
  double val, pval;
  if (args.pmethod != P_METHOD_NONE)
    printf ("node1%snode2%sassoc%sp\n", args.delim, args.delim, args.delim);
  else
    printf ("node1%snode2%sassoc\n", args.delim, args.delim);
  for (i=0; i<assoc->data->size1; i++)
    {
      node1 = assoc->rownames[i];
      for (j=0; j<assoc->data->size2; j++)
        {
          node2 = assoc->colnames[j];
          val = gsl_matrix_get (assoc->data, i, j);
          if (args.pmethod != P_METHOD_NONE)
            {
              pval = gsl_matrix_get (pvals->data, i, j);
              printf ("%s%s%s%s%f%s%f\n", node1, args.delim, node2, args.delim, val,
                      args.delim, pval);
            }
          else
            {
              printf ("%s%s%s%s%f\n", node1, args.delim, node2, args.delim, val);
            }
        }
    }
}


int
assocnet_main (const datatable *data, datatable **assoc, datatable **pvals,
               struct arguments args)
{
  size_t m = data->data->size1;
  *assoc = datatable_alloc (m, m, data->rownames, data->rownames);
  *pvals = datatable_alloc (m, m, data->rownames, data->rownames);

  switch (args.method)
    {
    case ASSOC_METHOD_PEARSON:
      calc_corr_matrix_pearson (data->data, (*assoc)->data);
      break;
    case ASSOC_METHOD_SPEARMAN:
      calc_corr_matrix_spearman (data->data, (*assoc)->data);
      break;
    case ASSOC_METHOD_MI:
      return (-1);
      break;
    }
  if (args.method == ASSOC_METHOD_PEARSON || args.method == ASSOC_METHOD_SPEARMAN)
    {
      if (args.unbiased)
        unbiased_corr ((*assoc)->data, data->data->size2);
      if (args.pmethod != P_METHOD_NONE)
        calc_corr_p (data->data, (*assoc)->data,  (*pvals)->data, args);
    }
  return (0);
}


int
assocnet_main_discr (const datatable_int *data, datatable **assoc, datatable **pvals,
                     struct arguments args)
{
  size_t m = data->data->size1;
  *assoc = datatable_alloc (m, m, data->rownames, data->rownames);
  /* *pvals = datatable_alloc (m, m, data->rownames, data->rownames); */

  switch (args.method)
    {
    case ASSOC_METHOD_PEARSON:
    case ASSOC_METHOD_SPEARMAN:
      return (-1);
    case ASSOC_METHOD_MI:
      calc_mi_matrix (data->data, (*assoc)->data);
      break;
    }
  return (0);
}
