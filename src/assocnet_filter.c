/* 
 * assocnet_filter.c --- 
 * 
 * Copyright (C) 2017, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include "assocnet_filter.h"


void
scale_matrix_minmax (gsl_matrix *m, gsl_matrix *scaled_m,
                       bool scale_diag)
{
  double m_min, m_max;
  if (scale_diag)
    {
      m_min = gsl_matrix_min (m);
      m_max = gsl_matrix_max (m);
      gsl_matrix_memcpy (scaled_m, m);
      gsl_matrix_add_constant (scaled_m, -m_min);
      gsl_matrix_scale (scaled_m, 1.0/(m_max-m_min));
    }
  else
    {
      gsl_matrix *m_cpy = safe_matrix_alloc (m->size1,  m->size2);
      gsl_matrix_memcpy (m_cpy, m);
      gsl_vector_view diag = gsl_matrix_diagonal (m_cpy);
      gsl_vector *scratch = safe_vector_alloc (m->size1);
      gsl_vector_set_all (scratch, GSL_NEGINF);
      gsl_vector_memcpy (&diag.vector, scratch);
      m_max = gsl_matrix_max (m_cpy);
      gsl_vector_set_all (scratch, GSL_POSINF);
      gsl_vector_memcpy (&diag.vector, scratch);
      m_min = gsl_matrix_min (m_cpy);
      gsl_matrix_free (m_cpy);
      gsl_vector_free (scratch);

      gsl_matrix_memcpy (scaled_m, m);
      gsl_matrix_add_constant (scaled_m, -m_min);
      gsl_matrix_scale (scaled_m, 1.0/(m_max - m_min));
      diag = gsl_matrix_diagonal (m);
      gsl_vector_view diag2 = gsl_matrix_diagonal (scaled_m);
      gsl_vector_memcpy (&diag2.vector, &diag.vector);
    }
}


void
scale_matrix_abs (const gsl_matrix *m, gsl_matrix *scaled_m,
                  bool scale_diag)
{
  double m_max = GSL_NEGINF, x;
  gsl_matrix_memcpy (scaled_m, m);
  for (size_t i=0; i<scaled_m->size1; i++)
    {
      for (size_t j=0; j<scaled_m->size2; j++)
        {
          x = gsl_matrix_get (scaled_m, i, j);
          if (!scale_diag)
            {
              if (i==j)
                continue;
              if (fabs (x) > m_max)
                m_max = fabs (x);
            }
          gsl_matrix_set (scaled_m, i, j, fabs (x));
        }
    }
  if (scale_diag)
    m_max = gsl_matrix_max (scaled_m);
  gsl_matrix_scale (scaled_m, 1.0/m_max);
}


void
remove_diagonal (const gsl_matrix *in_mat, gsl_matrix *out_mat)
{
  gsl_matrix *I, *ones;
  I = safe_matrix_alloc (in_mat->size1, in_mat->size2);
  ones = safe_matrix_alloc (in_mat->size1, in_mat->size2);
  gsl_matrix_set_identity (I);
  gsl_matrix_set_all (ones, 1.0);
  gsl_matrix_sub (ones, I);
  gsl_matrix_memcpy (out_mat, in_mat);
  gsl_matrix_mul_elements (out_mat, ones);
  gsl_matrix_free (I);
  gsl_matrix_free (ones);
}


void
threshold_matrix (const gsl_matrix *in_mat, gsl_matrix *out_mat, double alpha)
{
    gsl_vector_view mat_vals_tmp;
    gsl_vector *mat_vals;
    size_t i, j;
    double a_quantile;

    gsl_matrix_memcpy (out_mat, in_mat);

    /* threshold the input matrix */
    mat_vals_tmp = gsl_vector_view_array (in_mat->data,
                                          in_mat->size1*in_mat->size2);
    mat_vals = safe_vector_alloc (in_mat->size1*in_mat->size2);
    gsl_vector_memcpy (mat_vals, &mat_vals_tmp.vector);
    gsl_sort_vector (mat_vals);
    a_quantile = gsl_stats_quantile_from_sorted_data (mat_vals->data,
                                                      mat_vals->stride,
                                                      mat_vals->size,
                                                      1.0-alpha);
    for (i=0; i<in_mat->size1; i++)
      {
        for (j=0; j<in_mat->size2; j++)
          {
            if (gsl_matrix_get (in_mat, i, j) < a_quantile)
              gsl_matrix_set (out_mat, i, j, 0.0);
          }
      }
  gsl_vector_free (mat_vals);
}


void
deconvolve_matrix (const gsl_matrix *mat, gsl_matrix *deconv_mat, double beta)
{
  gsl_eigen_symmv_workspace *w;
  gsl_vector *evals, *evals2;
  gsl_matrix *evecs;
  gsl_matrix *tmp;
  gsl_matrix *evals_m;
  gsl_vector_view evals_m_diag;
  gsl_matrix *evecs_LU, *evecs_inv;
  gsl_permutation *perm;
  int signum;
  double lambda_p, lambda_n, m1, m2, m;

  tmp = safe_matrix_alloc (mat->size1, mat->size2);
  evals_m = safe_matrix_alloc (mat->size1, mat->size2);

  /* Eigen decomposition */
  gsl_matrix_memcpy (tmp, mat);
  w = gsl_eigen_symmv_alloc (mat->size1);
  evals = safe_vector_alloc (mat->size1);
  if (gsl_vector_max (evals) >= 1.0)
    error (EXIT_FAILURE, 0, "Assumption broken: largest eigenvalue must be less than  one");
  evecs = safe_matrix_alloc (mat->size1, mat->size2);
  gsl_eigen_symmv (tmp, evals, evecs, w);
  lambda_n = fabs (fmin (gsl_vector_min (evals), 0.0));
  lambda_p = fabs (fmax (gsl_vector_max (evals), 0.0));
  m1 = lambda_p*(1.0-beta)/beta;
  m2 = lambda_n*(1.0+beta)/beta;
  m = fmax (m1, m2);
  evals2 = safe_vector_alloc (mat->size1);
  gsl_vector_memcpy (evals2, evals);
  /* Network deconvolution */
  gsl_vector_add_constant (evals2, m);
  gsl_vector_div (evals, evals2);
  gsl_vector_free (evals2);
  /* Put eigen values on the diagonal */
  gsl_matrix_set_zero (evals_m);
  evals_m_diag = gsl_matrix_diagonal (evals_m);
  gsl_vector_memcpy (&evals_m_diag.vector, evals);
  perm = gsl_permutation_alloc (evecs->size1);
  if (!perm)
    error (EXIT_FAILURE, errno, "Failed to allocate permutation for inversion of eigen value matrix");
  evecs_LU = safe_matrix_alloc (evecs->size1, evecs->size2);
  evecs_inv = safe_matrix_alloc (evecs->size1, evecs->size2);
  gsl_matrix_memcpy (evecs_LU, evecs);
  gsl_linalg_LU_decomp (evecs_LU, perm, &signum);
  gsl_linalg_LU_invert (evecs_LU, perm, evecs_inv);
  gsl_permutation_free (perm);
  gsl_matrix_free (evecs_LU);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, evecs, evals_m, 0.0, tmp);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, tmp, evecs_inv, 0.0, deconv_mat);

  gsl_eigen_symmv_free (w);
  gsl_vector_free (evals);
  gsl_matrix_free (evecs);
  gsl_matrix_free (evecs_inv);
  gsl_matrix_free (tmp);
  gsl_matrix_free (evals_m);
}


void
filter_network_deconvolution (gsl_matrix *assoc, gsl_matrix *filtered,
                              struct arguments args)
{
  gsl_matrix *scaled_assoc, *scaled_assoc2, *thresh_assoc, *deconv_assoc;
  gsl_matrix *ind_edges, *ind_nonedges, *tmp;
  double m1, m2;
  size_t i, j;

  scaled_assoc = safe_matrix_alloc (assoc->size1, assoc->size2);
  scaled_assoc2 = safe_matrix_alloc (assoc->size1, assoc->size2);
  thresh_assoc = safe_matrix_alloc (assoc->size1, assoc->size2);
  deconv_assoc = safe_matrix_alloc (filtered->size1, filtered->size2);
  switch (args.pre_scale)
    {
    case SCALE_METHOD_MINMAX:
      scale_matrix_minmax (assoc, scaled_assoc, args.scale_diag);
      break;
    case SCALE_METHOD_ABS:
      scale_matrix_abs (assoc, scaled_assoc, args.scale_diag);
      break;
    case SCALE_METHOD_NONE:
      gsl_matrix_memcpy (scaled_assoc, assoc);
      break;
    }
  remove_diagonal (scaled_assoc, scaled_assoc2);
  threshold_matrix (scaled_assoc2, thresh_assoc, args.deconv_alpha);
  deconvolve_matrix (thresh_assoc, deconv_assoc, args.deconv_beta);
  if (args.observed_only)
    {
      ind_edges = safe_matrix_alloc (assoc->size1, assoc->size2);
      ind_nonedges = safe_matrix_alloc (assoc->size1, assoc->size2);
      tmp = safe_matrix_alloc (assoc->size1, assoc->size2);
      gsl_matrix_set_zero (ind_edges);
      for (i=0; i<ind_edges->size1; i++)
        {
          for (j=0; j<ind_edges->size2; j++)
            {
              if (gsl_matrix_get (thresh_assoc, i, j) > 0.0)
                {
                  gsl_matrix_set (ind_edges, i, j, 1.0);
                }
            }
        }
      gsl_matrix_set_all (ind_nonedges, 1.0);
      gsl_matrix_sub (ind_nonedges, ind_edges);
      gsl_matrix_memcpy (tmp, scaled_assoc2);
      gsl_matrix_mul_elements (tmp, ind_nonedges);
      m1 = gsl_matrix_max (tmp);
      m2 = gsl_matrix_min (deconv_assoc);
      gsl_matrix_add_constant (deconv_assoc, fmax (m1-m2, 0.0));
      gsl_matrix_mul_elements (deconv_assoc, ind_edges);
      gsl_matrix_add (deconv_assoc, tmp);
      gsl_matrix_free (ind_edges);
      gsl_matrix_free (ind_nonedges);
      gsl_matrix_free (tmp);
    }
  else
    {
      m2 = gsl_matrix_min (deconv_assoc);
      gsl_matrix_add_constant (deconv_assoc, fmax (-m2, 0));
    }
  switch (args.post_scale)
    {
    case SCALE_METHOD_MINMAX:
      scale_matrix_minmax (deconv_assoc, filtered, args.scale_diag);
      break;
    case SCALE_METHOD_ABS:
      scale_matrix_abs (deconv_assoc, filtered, args.scale_diag);
      break;
    case SCALE_METHOD_NONE:
      gsl_matrix_memcpy (filtered, deconv_assoc);
    }
  gsl_matrix_free (scaled_assoc);
  gsl_matrix_free (scaled_assoc2);
  gsl_matrix_free (thresh_assoc);
  gsl_matrix_free (deconv_assoc);
}


void
output_results (const datatable *filtered, struct arguments args)
{
  size_t i, j;
  char *node1, *node2;
  double val;
  printf ("node1%snode2%sfilter.weight\n", args.delim, args.delim);
  for (i=0; i<filtered->data->size1; i++)
    {
      node1 = filtered->rownames[i];
      for (j=0; j<filtered->data->size2; j++)
        {
          node2 = filtered->colnames[j];
          val = gsl_matrix_get (filtered->data, i, j);
          printf ("%s%s%s%s%f\n", node1, args.delim, node2, args.delim, val);
        }
    }
}


int
assocnet_filter_main (const datatable *data, datatable **filtered,
                      struct arguments args)
{
  size_t m = data->data->size1;
  *filtered = datatable_alloc (m, m, data->rownames, data->rownames);
  switch (args.filter)
    {
    case FILTER_METHOD_DECONV:
      filter_network_deconvolution (data->data, (*filtered)->data, args);
      break;
    default:
      break;
    }
  return (0);
}
