/* 
 * mvt.c --- 
 * 
 * Copyright (C) 2017, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include "mvt.h"

int ran_multivariate_t (const gsl_rng *R, const gsl_vector *mu,
                        const gsl_matrix *C, double nu,
                        gsl_vector *result)
{
  gsl_matrix *work;
  gsl_vector *mu_zero;
  gsl_vector *u;
  double cs, adj;

  if (nu <= 0.0)
    return -1;
  if (!C)
    return -1;

  work = gsl_matrix_alloc (C->size1, C->size2);
  if (!work)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  mu_zero = gsl_vector_alloc (work->size1);
  if (!mu_zero)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  u = gsl_vector_alloc (work->size1);
  if (!u)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");

  gsl_matrix_memcpy (work, C);

  gsl_error_handler_t * handler;
  handler = gsl_set_error_handler_off ();
  int err;
  err = gsl_linalg_cholesky_decomp1 (work);
  if (err == GSL_EDOM)
    {
      error (0, err, "matrix is not positive definite square");
      return GSL_EDOM;
    }
  gsl_set_error_handler (handler);
  gsl_vector_set_zero (mu_zero);
  gsl_ran_multivariate_gaussian (R, mu_zero, work, result);

  cs = gsl_ran_chisq (R, nu);
  adj = sqrt (nu/cs);
  gsl_vector_scale (result, adj);
  gsl_vector_add (result, mu);

  gsl_matrix_free (work);
  gsl_vector_free (mu_zero);
  gsl_vector_free (u);

  return (0);
}

int
ran_multivariate_t_pdf (const gsl_vector *X, const gsl_matrix *C,
                        double nu, double *result, gsl_vector *work)
{
  gsl_matrix *chol_tmp, *chol, *C_t;
  gsl_vector_view chol_diag;
  gsl_permutation *chol_P;
  int signum;

  if (!X || !C)
    return -1;
  if (nu <= 0.0)
    return -1;
  if (X->size != C->size1)
    return -1;
  if (C->size1 != C->size2)
    return -1;
  C_t = gsl_matrix_alloc (C->size1, C->size2);
  gsl_matrix_transpose_memcpy (C_t, C);
  if (!gsl_matrix_equal (C, C_t))
    {
      gsl_matrix_free (C_t);
      return -1;
    }
  gsl_matrix_free (C_t);

  double sqrt_det_sigma=1.0, scale, tmp;
  size_t d = C->size1;

  chol_tmp = gsl_matrix_alloc (C->size1, C->size2);
  if (!chol_tmp)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  gsl_matrix_memcpy (chol_tmp, C);
  gsl_linalg_cholesky_decomp1 (chol_tmp);
  /* Kludge to get just the lower-triangle of the Cholesky decomposition */
  chol = gsl_matrix_alloc (chol_tmp->size1, chol_tmp->size2);
  if (!chol)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  gsl_matrix_set_identity (chol);
  gsl_blas_dtrmm (CblasLeft, CblasLower, CblasNoTrans, CblasNonUnit, 1.0,
                  chol_tmp, chol);
  gsl_matrix_free (chol_tmp);

  chol_diag = gsl_matrix_diagonal (chol);
  for (size_t i=0; i<chol_diag.vector.size; i++)
    {
      sqrt_det_sigma *= gsl_vector_get (&chol_diag.vector, i);
    }

  scale = gsl_sf_lngamma ((nu + ((double) d))/2.0) - gsl_sf_lngamma (nu/2.0);
  scale = exp(scale);
  scale /= sqrt_det_sigma * pow(nu*M_PI, ((double) d)/2.0);

  chol_P = gsl_permutation_alloc (chol->size1);
  if (!chol_P)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  gsl_linalg_LU_decomp (chol, chol_P, &signum);
  gsl_linalg_LU_solve (chol, chol_P, X, work);

  gsl_blas_ddot (work, work, &tmp); /* sum-of-squares */

  *result = scale / pow((1.0+tmp/nu), (nu+((double) d))/2.0);

  gsl_permutation_free (chol_P);
  gsl_matrix_free (chol);

  return (0);
}
