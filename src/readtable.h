/* 
 * readtable.h --- 
 * 
 * Copyright (C) 2017, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef READTABLE_H
#define READTABLE_H

#include "config.h"
#include <stdio.h>
#include "error.h"
#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_matrix_int.h>
#include <gsl/gsl_matrix_char.h>

#include "datatable.h"
#include "safe_alloc.h"

enum data_type {DATA_TYPE_FLOAT, DATA_TYPE_INT, DATA_TYPE_BOOL};

int read_narrow_table (FILE *table_stream, char *delim, bool header, size_t num_data_cols, ...);

#endif
