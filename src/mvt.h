/* 
 * mvt.h --- 
 * 
 * Copyright (C) 2017 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef MVT_H
#define MVT_H

#include "config.h"

#include <math.h>
#include "error.h"
#include <errno.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_math.h>

int ran_multivariate_t (const gsl_rng *R, const gsl_vector *mu,
                        const gsl_matrix *C, double nu,
                        gsl_vector *result);

int ran_multivariate_t_pdf (const gsl_vector *X, const gsl_matrix *C,
                            double nu, double *result, gsl_vector *work);


#endif
