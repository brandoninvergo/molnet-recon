/* 
 * assocnet-main.c --- 
 * 
 * Copyright (C) 2017, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "argp.h"
#include "error.h"
#include <errno.h>
#include <sys/stat.h>
#include <libgen.h>
#include <gsl/gsl_matrix.h>

#include "assocnet.h"
#include "datatable.h"
#include "readtable.h"


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "assocnet -- reconstruct a network by statistical association"
  "\v"
  "Input data should be in a 2-column \"narrow\" format: the first column "
  "contains the row names (e.g. node names), the second column contains the "
  "column names (e.g. condition names), and the third column contains the data "
  "(e.g. node value under that condition)."
  "\n\n"
  "If a data file is not provided, the data will be read from stdin.";

static char args_doc[] = "DATA";

static struct argp_option options[] = {
  {0, 0, 0, 0, "Data input", 1},
  {"delim", 'd', "CHAR", 0, "Column delimiter (default: \\t)", 0},
  {"header-in", 'h', 0, 0, "Data contains a header row", 0},
  {0, 0, 0, 0, "Methods", 2},
  {"method", 'm', "METHOD", 0, "Association method: 'pearson' (Pearson's"
   " correlaton coefficient), 'spearman' (Spearman's rank correlation"
   " coefficient), 'mi' (mutual information) (default: 'pearson')", 0},
  {"unbiased-correlation", 'u', 0, 0, "Adjust correlations to remove bias", 0},
  {"p-method", 'p', "METHOD", 0, "Method for calculating correlation p-values:"
   " 't' (via Student's t-distribution), 'z' (via Fisher's r-to-Z"
   " transformation) 'bootstrap' (via bootstrapping), 'none' (don't calculate a"
   " p-value) (default: none)", 0},
  {0, 0, 0, 0, "Parameters", 3},
  {"p-permutations", 'n', "NUMBER", 0, "Number of permutations to do when"
   " estimating p-values by bootstrapping (default: 10000)", 0},
  {0}
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char *tail;
  struct arguments *arguments = state->input;
  switch (key)
    {
    case 'd':
      if (strlen (arg) != 1)
        argp_usage (state);
      arguments->delim = arg;
      break;
    case 'h':
      arguments->header_in = true;
      break;
    case 'm':
      if (strlen (arg) == 0)
        argp_usage (state);
      if (!strcmp(arg, "pearson"))
        arguments->method = ASSOC_METHOD_PEARSON;
      else if (!strcmp(arg, "spearman"))
        arguments->method = ASSOC_METHOD_SPEARMAN;
      else if (!strcmp(arg, "mi"))
        arguments->method = ASSOC_METHOD_MI;
      else
        argp_usage (state);
      break;
    case 'n':
      errno = 0;
      arguments->bootstrap_n = (size_t) strtoul (arg, &tail, 0);
      if (arguments->bootstrap_n == 0)
        argp_usage (state);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid number of permutations");
        }
      break;
    case 'p':
      if (strlen (arg) == 0)
        argp_usage (state);
      if (!strcmp(arg, "none"))
        arguments->pmethod = P_METHOD_NONE;
      else if (!strcmp(arg, "t"))
        arguments->pmethod = P_METHOD_T;
      else if (!strcmp(arg, "z"))
        arguments->pmethod = P_METHOD_Z;
      else if (!strcmp(arg, "bootstrap"))
        arguments->pmethod = P_METHOD_BOOTSTRAP;
      else
        argp_usage (state);
      break;
    case 'u':
      arguments->unbiased = true;
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num > 1)
        argp_usage (state);
      arguments->args[0] = arg;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};


void
init_args (struct arguments *arguments)
{
  arguments->method = ASSOC_METHOD_PEARSON;
  arguments->pmethod = P_METHOD_NONE;
  arguments->delim = "\t";
  arguments->unbiased = false;
  arguments->bootstrap_n = 10000;
  arguments->args[0] = NULL;
  arguments->header_in = false;
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  datatable *data = NULL;
  datatable_int *data_discr = NULL;
  datatable *assoc = NULL;
  datatable *pvals = NULL;
  FILE *table_stream;

  init_args (&arguments);
  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  if (arguments.unbiased && arguments.pmethod != P_METHOD_NONE)
    arguments.pmethod = P_METHOD_BOOTSTRAP;

  if (arguments.args[0] == NULL)
    {
      table_stream = stdin;
    }
  else
    {
      table_stream = fopen (arguments.args[0], "r");
      if (!table_stream)
        {
          error (EXIT_FAILURE, errno, "Failed to open response file");
        }
    }
  if (arguments.method == ASSOC_METHOD_MI)
    {
      arguments.pmethod = P_METHOD_NONE;
      read_narrow_table (table_stream, arguments.delim, arguments.header_in,
                         1, DATA_TYPE_INT, &data_discr);
      assocnet_main_discr (data_discr, &assoc, &pvals, arguments);
    }
  else
    {
      read_narrow_table (table_stream, arguments.delim, arguments.header_in,
                         1, DATA_TYPE_FLOAT, &data);
      assocnet_main (data, &assoc, &pvals, arguments);
    }

  output_results (assoc, pvals, arguments);

  if (data)
    datatable_free (data);
  if (data_discr)
    datatable_int_free (data_discr);
  if (assoc)
    datatable_free (assoc);
  if (pvals)
    datatable_free (pvals);
  exit (EXIT_SUCCESS);
}
