/* 
 * assocnet_filter_main.c --- 
 * 
 * Copyright (C) 2017, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "argp.h"
#include "error.h"
#include <errno.h>
#include <sys/stat.h>
#include <libgen.h>
#include <gsl/gsl_matrix.h>

#include "assocnet_filter.h"
#include "datatable.h"
#include "readtable.h"


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "assocnet-filter -- remove indirect edges from a"
  " statistical-association network"
  "\v"
  "Input data should be in a 3-column \"narrow\" format: the first column "
  "contains the row names (e.g. node names), the second column contains the "
  "column names (e.g. condition names), and the third column contains the data "
  "(e.g. node value under that condition)."
  "\n\n"
  "If a data file is not provided, the data will be read from stdin.";

static char args_doc[] = "DATA";

static struct argp_option options[] = {
  {0, 0, 0, 0, "Data input", 1},
  {"delim", 'd', "CHAR", 0, "Column delimiter (default: \\t)", 0},
  {"header-in", 'h', 0, 0, "Data contains a header row", 0},
  {0, 0, 0, 0, "Methods", 2},
  {"method", 'm', "METHOD", 0, "Method of filtering indirect edges:"
   " 'deconvolution' (default: deconvolution)",0},
  {"pre-scale-method", 's', "METHOD", 0, "Method for linearly scaling the input"
   " matrix: 'minmax' (normalization against the minimum and maximum values),"
   " 'abs' (normalization against the largest absolute value, for"
   " use when negative values are meaningful), 'none' (default: minmax)", 0},
  {"post-scale-method", 't', "METHOD", 0, "Method for linearly scaling the output"
   " matrix: 'minmax' (see --pre-scale-method), 'abs', 'none'"
   " (default: minmax)", 0},
  {"scale-diagonal", 130, NULL, 0, "Deconvolution: include the diagonal when"
   " performing matrix scaling (as in Feizi et al. 2013)", 0},
  {0, 0, 0, 0, "Parameters", 3},
  {"deconvolution-a", 'a', "VALUE", 0, "Fraction of observed edges to be used in"
   " the deconvolution process (e.g. a value of 0.75 would use the largest 75%"
   " of the edge values) (default: 1.0 (use all edges))", 0},
  {"deconvolution-b", 'b', "VALUE", 0, "Eigenvalue-scaling parameter (default:"
   " 0.99)", 0},
  {"observed-only", 140, 0, 0, "Display deconvolved values for observed edges"
   " only", 0},
  {0}
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char *tail;
  struct arguments *arguments = state->input;
  switch (key)
    {
    case 'a':
      errno = 0;
      arguments->deconv_alpha = strtod (arg, &tail);
      if (errno || arguments->deconv_alpha <= 0.0 ||
          arguments->deconv_alpha > 1.0)
        {
          error (EXIT_FAILURE, errno, "Invalid deconvolution data threshold."
                 " Values in the range (0.0, 1.0] are valid.");
        }
      break;
    case 'b':
      errno = 0;
      arguments->deconv_beta = strtod (arg, &tail);
      if (errno || arguments->deconv_beta <= 0.0 ||
          arguments->deconv_beta >= 1.0)
        {
          error (EXIT_FAILURE, errno, "Invalid deconvolution eigenvalue-scaling factor."
                 " Values in the range (0.0, 1.0) are valid.");
        }
      break;
    case 'd':
      if (strlen (arg) != 1)
        argp_usage (state);
      arguments->delim = arg;
      break;
    case 'h':
      arguments->header_in = true;
      break;
    case 'm':
      if (strlen (arg) == 0)
        argp_usage (state);
      if (!strcmp(arg, "deconvolution"))
        arguments->filter = FILTER_METHOD_DECONV;
      else
        argp_usage (state);
      break;
    case 's':
      if (strlen (arg) == 0)
        argp_usage (state);
      if (!strcmp(arg, "minmax"))
        arguments->pre_scale = SCALE_METHOD_MINMAX;
      else if (!strcmp(arg, "abs"))
        arguments->pre_scale = SCALE_METHOD_ABS;
      else if (!strcmp(arg, "none"))
        arguments->pre_scale = SCALE_METHOD_NONE;
      else
        argp_usage (state);
      break;
    case 't':
      if (strlen (arg) == 0)
        argp_usage (state);
      if (!strcmp(arg, "minmax"))
        arguments->post_scale = SCALE_METHOD_MINMAX;
      else if (!strcmp(arg, "abs"))
        arguments->post_scale = SCALE_METHOD_ABS;
      else if (!strcmp(arg, "none"))
        arguments->post_scale = SCALE_METHOD_NONE;
      else
        argp_usage (state);
      break;
    case 130:
      arguments->scale_diag = true;
      break;
    case 140:
      arguments->observed_only = true;
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num > 1)
        argp_usage (state);
      arguments->args[0] = arg;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};


void
init_args (struct arguments *arguments)
{
  arguments->delim = "\t";
  arguments->filter = FILTER_METHOD_DECONV;
  arguments->pre_scale = SCALE_METHOD_MINMAX;
  arguments->post_scale = SCALE_METHOD_MINMAX;
  arguments->deconv_alpha = 1.0;
  arguments->deconv_beta = 0.99;
  arguments->args[0] = NULL;
  arguments->header_in = false;
  arguments->observed_only = false;
  arguments->scale_diag = false;
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  datatable *data = NULL;
  datatable *filtered = NULL;
  char *data_file;
  FILE *table_stream;

  init_args (&arguments);
  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  if (arguments.args[0] == NULL)
    {
      table_stream = stdin;
    }
  else
    {
        table_stream = fopen (arguments.args[0], "r");
        if (!table_stream)
          {
            error (EXIT_FAILURE, errno, "Failed to open response file");
          }
    }
  read_narrow_table (table_stream, arguments.delim, arguments.header_in,
                1, DATA_TYPE_FLOAT, &data);

  assocnet_filter_main (data, &filtered, arguments);
  output_results (filtered, arguments);

  datatable_free (data);
  datatable_free (filtered);
  exit (EXIT_SUCCESS);
}
