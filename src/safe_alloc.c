/* 
 * safe_alloc.c --- 
 * 
 * Copyright (C) 2017, 2018, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "safe_alloc.h"


void *
safe_malloc (size_t n)
{
  void *dest = malloc (n);
  if (!dest)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  return dest;
}


gsl_matrix *
safe_matrix_alloc (size_t i, size_t j)
{
  gsl_matrix *dest = gsl_matrix_alloc (i, j);
  if (!dest)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  return dest;
}


gsl_matrix_int *
safe_matrix_int_alloc (size_t i, size_t j)
{
  gsl_matrix_int *dest = gsl_matrix_int_alloc (i, j);
  if (!dest)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  return dest;
}


gsl_matrix *
safe_matrix_calloc (size_t i, size_t j)
{
  gsl_matrix *dest = gsl_matrix_calloc (i, j);
  if (!dest)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  return dest;
}


gsl_matrix_int *
safe_matrix_int_calloc (size_t i, size_t j)
{
  gsl_matrix_int *dest = gsl_matrix_int_calloc (i, j);
  if (!dest)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  return dest;
}


gsl_vector *
safe_vector_alloc (size_t i)
{
  gsl_vector *dest = gsl_vector_alloc (i);
  if (!dest)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  return dest;
}


gsl_vector *
safe_vector_calloc (size_t i)
{
  gsl_vector *dest = gsl_vector_calloc (i);
  if (!dest)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  return dest;
}

