/*
 * bindata.c --- 
 * 
 * Copyright (C) 2018, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bindata.h"


void
calc_even_breaks (const gsl_matrix *data, size_t num_bins, double *breaks)
{
  double min_val, max_val;
  double bin_width;
  size_t i;

  if (num_bins <= 1)
    error (EXIT_FAILURE, 0, "Number of bins must be greater than 1.");

  min_val = gsl_matrix_min (data);
  max_val = gsl_matrix_max (data);
  bin_width = (max_val-min_val)/num_bins;
  for (i=0; i<num_bins; i++)
    {
      breaks[i] = min_val + bin_width * i;
    }
}


void
calc_quantile_breaks (const gsl_matrix *data, size_t num_bins, double *breaks)
{
  double *data_array;
  size_t n;
  size_t i;

  if (num_bins <= 1)
    error (EXIT_FAILURE, 0, "Number of bins for must be greater than 1.");

  n = data->size1 * data->size2;
  data_array = (double *) malloc (n * sizeof (double));
  if (data_array == NULL)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  memcpy (data_array, data->data, n*sizeof (double));
  gsl_sort (data_array, 1, n);
  for (i=0; i<num_bins; i++)
    {
      breaks[i] = gsl_stats_quantile_from_sorted_data
        (data_array, 1, n, ((double) i)/((double) num_bins));
    }
}


int
choose_bin (const double *breaks, size_t num_bins, double val)
{
  size_t b = num_bins / 2;
  size_t max = num_bins;
  while (true)
    {
      if ((b == num_bins - 1 && val >= breaks[b]) ||
          (val >= breaks[b] && val < breaks[b+1]))
        {
          return b;
        }
      else if (val < breaks[b])
        {
          max = b;
          b = b / 2;
        }
      else if (val >= breaks[b+1])
        {
          b = (max - b)/2 + b;
        }
    }
}


void
bin_data (const gsl_matrix *data, gsl_matrix_int *bins, size_t num_bins,
               enum bin_method method)
{
  size_t i;
  size_t num_breaks = num_bins;
  double *breaks;
  if (data->size1 != bins->size1 || data->size2 != bins->size2)
    error (EXIT_FAILURE, 0, "Bin matrix is a different size than data matrix");
  breaks = (double *) malloc (num_breaks*sizeof (double));
  if (!breaks)
    error (EXIT_FAILURE, errno, "Failed to allocate memory");
  switch (method)
    {
    case BIN_EVEN:
      calc_even_breaks (data, num_bins, breaks);
      break;
    case BIN_QUANTILES:
      calc_quantile_breaks (data, num_bins, breaks);
      break;
    }
  gsl_matrix_int_set_all (bins, -1);
#pragma omp parallel for private(i)
  for (i=0; i<data->size1; i++)
    {
      double val;
      size_t j, bin;
      for (j=0; j<data->size2; j++)
        {
          val = gsl_matrix_get (data, i, j);
          bin = choose_bin (breaks, num_breaks, val);
          gsl_matrix_int_set (bins, i, j, bin + 1);
        }
    }
  free (breaks);
}


void
output_results (const datatable_int *bins, struct arguments args)
{
  size_t i, j;
  char *node1, *node2;
  int val;
  printf ("node1%snode2%sbin\n", args.delim, args.delim);
  for (i=0; i<bins->data->size1; i++)
    {
      node1 = bins->rownames[i];
      for (j=0; j<bins->data->size2; j++)
        {
          node2 = bins->colnames[j];
          val = gsl_matrix_int_get (bins->data, i, j);
          printf ("%s%s%s%s%d\n", node1, args.delim, node2, args.delim, val);
        }
    }
}

