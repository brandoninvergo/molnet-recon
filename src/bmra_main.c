/* 
 * bmra-main.c --- 
 * 
 * Copyright (C) 2017 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "argp.h"
#include "error.h"
#include <errno.h>
#include <sys/stat.h>
#include <libgen.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_matrix.h>

#include "bmra.h"
#include "datatable.h"
#include "readtable.h"


const char *argp_program_version = PACKAGE_STRING;
const char *argp_program_bug_address = PACKAGE_BUGREPORT;

static char doc[] =
  "bmra -- reconstruct a network by Bayesian Modular Response Analysis"
  "\v"
  "Input data should be in a 4-column \"narrow\" format: the first column "
  "contains the row names (e.g. node names), the second column contains the "
  "column names (e.g. condition names), the third column contains the data "
  "(e.g. node value under that condition), and the fourth column contains "
  "boolean (TRUE/FALSE) perturbation information (e.g. whether that node "
  "is perturbed under that condition)."
  "\n\n"
  "If a data file is not provided, the data will be read from stdin.";

static char args_doc[] = "DATA";

static struct argp_option options[] = {
  {0, 0, 0, 0, "Data input", 1},
  {"delim", 'd', "CHAR", 0, "Column delimiter (default: \\t)", 0},
  {"header-in", 'h', 0, 0, "Data contains a header row", 0},
  {"init", 'i', "TYPE", 0, "How to initialize the network"
   " (values: 'full', 'empty', 'random', 'file'; default: 'full')", 0},
  {"init-file", 'f', "FILE", 0, "Initial network provided in an edge-list"
   " file (see option --init).  Edge weights, if present, are interpreted as"
   " edge prior probabilities (see option --edge-prior).",
   0},
  {0, 0, 0, 0, "Sampling", 2},
  {"runs", 'r', "NUMBER", 0, "Number of repeat runs (default: 4)", 0},
  {"samples", 's', "NUMBER", 0, "Number of samples (default: 50000)", 0},
  {"burn-in", 'b', "NUMBER", 0, "Number of burn-in samples (default: 25000)", 0},
  {"subnetworks-only", 130, 0, 0, "Restrict MCMC sampling to sub-networks of the"
   " initial network", 0},
  {"sampler", 131, "METHOD", 0, "Which sampler to use: mcmc, gibbs, or"
   " exhaustive (default: mcmc)", 0},
  {"thinning", 't', "NUMBER", 0, "Keep only every Nth sample (default: 1 (no"
   " thinning))", 0},
  {0, 0, 0, 0, "Parameters", 3},
  {"edge-prior", 'p', "TYPE", 0, "Edge prior probability (values: 'hamming',"
   " 'prob' (see --init-file), 'beta', 'beta-hamming'; default: 'beta')", 0},
  {"hamming-psi", 140, "VALUE", 0, "Used in calculating sub-network prior"
   " probabilities via the Hamming distance (default: 1.0)", 0},
  {"beta-a", 141, "VALUE", 0, "Beta function parameter used in calculating"
   " sub-network prior probabilities and in random network initialization"
   " (default: 1.0)", 0},
  {"beta-b", 142, "VALUE", 0, "Beta function parameter used in calculating"
   " sub-network prior probabilities and in random network inialization"
   " (default: 2.0)", 0},
  {"lambda", 143, "VALUE", 0, "Used in calculating interaction-strength prior"
   " probabilities (default: 0.2)", 0},
  {"igamma-a", 144, "VALUE", 0, "Location parameter used in calculating the error"
   " variance via the inverse gamma distribution (default: 1.0)", 0},
  {"igamma-b", 145, "VALUE", 0, "Scale parameter used in calculating the error"
   " variance via the inverse gamma distribution (default: 1.0)", 0},
  {"prob-gamma", 146, "VALUE", 0, "Small prior probability to assign to edges"
   " for which no prior probability exists (default: 0.0)", 0},
  {"zellner-g", 147, "METHOD", 0, "Method for setting the constant in Zellner's"
   " prior: 'uip' (# of conditions), 'ric' (# regulators squared), 'benchmark'"
   " (max(uip, ric)) (default: uip)", 0},
  {"lpa-method", 148, "METHOD", 0, "How to calculate log(P(A_i|R)): 'as-written'"
   " or 'as-implemented' (default: as-written)", 0},
  {0, 0, 0, 0, "Options", 3},
  {"trace", 150, "LIST", 0, "Write files containing parameter values across all"
   " sampling iterations.  (values: 'none', 'lpa' [log(P(A_i|R_i))], 'lpa_t'"
   " [log(P({A_i}^t|R_i)]], 'lpr' [log(P(r_i|A_i,R_i))])",
   0},
  {"trace-dir", 151, "DIRECTORY", 0, "Directory in which to write trace files"
   " (default: current directory)", 0},
  {0}
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char *tail;
  char *tok;
  struct stat s;
  int err;
  struct arguments *arguments = state->input;
  switch (key)
    {
    case 'b':
      errno = 0;
      arguments->burnin = (size_t) strtoul (arg, &tail, 0);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid burn-in number");
        }
      break;
    case 'd':
      if (strlen (arg) == 0)
        argp_usage (state);
      arguments->delim = arg;
      break;
    case 'h':
      arguments->header_in = true;
      break;
    case 'i':
      if (strlen (arg) == 0)
        argp_usage (state);
      if (!strcmp(arg, "empty"))
        arguments->init = NET_INIT_EMPTY;
      else if (!strcmp(arg, "full"))
        arguments->init = NET_INIT_FULL;
      else if (!strcmp(arg, "random"))
        arguments->init = NET_INIT_RANDOM;
      else if (!strcmp(arg, "file"))
        arguments->init = NET_INIT_FILE;
      else
        argp_usage (state);
      break;
    case 'f':
      if (strlen (arg) == 0)
        argp_usage (state);
      arguments->init_file = arg;
      break;
    case 'p':
      if (strlen (arg) == 0)
        argp_usage (state);
      if (!strcmp(arg, "hamming"))
        arguments->edge_prior = EDGE_PRIOR_HAMMING;
      else if (!strcmp(arg, "prob"))
        arguments->edge_prior = EDGE_PRIOR_PROB;
      else if (!strcmp(arg, "beta"))
        arguments->edge_prior = EDGE_PRIOR_BETA;
      else if (!strcmp(arg, "beta-hamming"))
        arguments->edge_prior = EDGE_PRIOR_BETA_HAMMING;
      else
        argp_usage (state);
      break;
    case 'r':
      errno = 0;
      arguments->runs = (size_t) strtoul (arg, &tail, 0);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid number of runs");
        }
      break;
    case 's':
      errno = 0;
      arguments->samples = (size_t) strtoul (arg, &tail, 0);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid number of samples");
        }
      break;
    case 't':
      errno = 0;
      arguments->thinning = (size_t) strtoul (arg, &tail, 0);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid thinning value");
        }
      break;
    case 130:
      arguments->subnet_only = true;
      break;
    case 131:
      if (strlen (arg) == 0)
        argp_usage (state);
      if (!strcmp(arg, "mcmc"))
        arguments->sampler = SAMPLER_MCMC;
      else if (!strcmp(arg, "gibbs"))
        arguments->sampler = SAMPLER_GIBBS;
      else if (!strcmp(arg, "exhaustive"))
        arguments->sampler = SAMPLER_EXHAUSTIVE;
      else
        argp_usage (state);
      break;
    case 140:
      errno = 0;
      arguments->hamming_psi = strtod (arg, &tail);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid 'psi' parameter");
        }
      break;
    case 141:
      errno = 0;
      arguments->beta_a = strtod (arg, &tail);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid Beta 'a' parameter");
        }
      break;
    case 142:
      errno = 0;
      arguments->beta_b = strtod (arg, &tail);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid Beta 'b' parameter");
        }
      break;
    case 143:
      errno = 0;
      arguments->lambda = strtod (arg, &tail);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid Beta 'b' parameter");
        }
      break;
    case 144:
      errno = 0;
      arguments->igamma_a = strtod (arg, &tail);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid Inverted Gamma 'a' parameter");
        }
      break;
    case 145:
      errno = 0;
      arguments->igamma_b = strtod (arg, &tail);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid Inverted Gamma 'b' parameter");
        }
      break;
    case 146:
      errno = 0;
      arguments->prob_gamma = strtod (arg, &tail);
      if (errno)
        {
          error (EXIT_FAILURE, errno, "Invalid gamma parameter");
        }
      break;
    case 147:
      if (strlen (arg) == 0)
        argp_usage (state);
      if (!strcmp(arg, "uip"))
        arguments->zellner_g = ZELLNER_G_UIP;
      else if (!strcmp(arg, "ric"))
        arguments->zellner_g = ZELLNER_G_RIC;
      else if (!strcmp(arg, "benchmark"))
        arguments->zellner_g = ZELLNER_G_BENCH;
      else
        argp_usage (state);
      break;
    case 148:
      if (strlen (arg) == 0)
        argp_usage (state);
      if (!strcmp(arg, "as-written"))
        arguments->lpa = LPA_METHOD_WRITTEN;
      else if (!strcmp(arg, "as-implemented"))
        arguments->lpa = LPA_METHOD_IMPLEMENTED;
      else
        argp_usage (state);
      break;
    case 150:
      tok = strtok (arg, ",");
      if (tok == NULL)
        argp_usage (state);
      while (tok != NULL)
        {
          if (!strcmp (tok, "lpa"))
            arguments->trace |= TRACE_LPA;
          else if (!strcmp (tok, "lpa_t"))
            arguments->trace |= TRACE_LPA_T;
          else if (!strcmp (tok, "lpr"))
            arguments->trace |= TRACE_LPR;
          else if (!strcmp (tok, "none"))
            arguments->trace |= TRACE_NONE;
          else
            argp_usage (state);
          tok = strtok (NULL, ",");
        }
      break;
    case 151:
      err = stat (arg, &s);
      if (err == -1)
        {
          error (EXIT_FAILURE, errno, "Error setting trace directory");
        }
      else
        {
          if (S_ISDIR (s.st_mode))
            {
              arguments->trace_dir = arg;
            }
          else
            {
              error (EXIT_FAILURE, errno, "Specified trace directory is not a"
                     " directory");
            }
        }
      break;
    case ARGP_KEY_ARG:
      if (state->arg_num > 1)
        argp_usage (state);
      arguments->args[0] = arg;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc};


void
init_args (struct arguments *arguments)
{
  arguments->init = NET_INIT_FULL;
  arguments->init_file = NULL;
  arguments->edge_prior = EDGE_PRIOR_BETA;
  arguments->runs = 4;
  arguments->sampler = SAMPLER_MCMC;
  arguments->samples = 50000;
  arguments->burnin = arguments->samples/2;
  arguments->thinning = 1;
  arguments->delim = "\t";
  arguments->subnet_only = false;
  arguments->hamming_psi = 1.0;
  arguments->beta_a = 1.0;
  arguments->beta_b = 2.0;
  arguments->lambda = 0.2;
  arguments->igamma_a = 1.0;
  arguments->igamma_b = 1.0;
  arguments->prob_gamma = 0.0;
  arguments->zellner_g = ZELLNER_G_UIP;
  arguments->lpa = LPA_METHOD_WRITTEN;
  arguments->trace = TRACE_NONE;
  arguments->args[0] = NULL;
  arguments->header_in = false;
}


void
write_trace (gsl_matrix *trace, enum trace_vals trace_val, struct arguments args)
{
  if (trace == NULL)
    return;
  char *in_path = strdupa (args.args[0]);
  char *base_file = basename (in_path);
  size_t base_len = strlen (base_file);
  char *out_file = (char *) malloc ((base_len + 13)*sizeof (char));
  char *out_tmp, *out_path;
  FILE *h;
  out_tmp = stpncpy (out_file, base_file, base_len*sizeof (char));
  switch (trace_val)
    {
    case TRACE_LPA:
      strncpy (out_tmp, ".lpa.trace", 10*sizeof (char));
      out_file[base_len+10] = '\0';
      break;
    case TRACE_LPA_T:
      strncpy (out_tmp, ".lpa_t.trace", 12*sizeof (char));
      out_file[base_len+12] = '\0';
      break;
    case TRACE_LPR:
      strncpy (out_tmp, ".lpr.trace", 10*sizeof (char));
      out_file[base_len+10] = '\0';
      break;
    case TRACE_NONE:
      free (out_file);
      return;
    }
  out_path = (char *) malloc ((strlen (args.trace_dir) + strlen (out_file) + 2)*sizeof (char));
  out_tmp = stpncpy (out_path, args.trace_dir, strlen (args.trace_dir)*sizeof (char));
  out_tmp = stpncpy (out_tmp, "/", sizeof (char));
  stpncpy (out_tmp, out_file, strlen (out_file)*sizeof (char));
  out_path[strlen (args.trace_dir)+strlen (out_file)+1] = '\0';
  free (out_file);
  h = fopen (out_path, "w");
  if (h==NULL)
    {
      free (out_file);
      free (out_path);
      error (EXIT_FAILURE, errno, "Error opening trace file");
    }
  for (size_t i=0; i<trace->size1; i++)
    {
      for (size_t j=0; j<trace->size2; j++)
        fprintf (h, "%f\t", gsl_matrix_get (trace, i, j));
      fprintf (h, "\n");
    }
  fclose (h);
  free (out_path);
}


int
main (int argc, char **argv)
{
  struct arguments arguments;
  datatable *A_init = NULL;
  datatable *response = NULL, *A_prior = NULL;
  datatable *perts = NULL;
  datatable **A_all = NULL;
  datatable **r_all = NULL;
  datatable *P_A = NULL;
  datatable *r_mean = NULL;
  datatable *r_std = NULL;
  gsl_matrix *lp_A_all = NULL;
  gsl_matrix *lp_A_t_all = NULL;
  gsl_matrix *lp_r_all = NULL;
  FILE *table_stream;
  size_t i, n;

  init_args (&arguments);
  argp_parse (&argp, argc, argv, 0, 0, &arguments);
  if (arguments.burnin >= arguments.samples)
    error (EXIT_FAILURE, 0, "Number of burn-in samples must be fewer than the number"
           " of samples");

  gsl_rng_env_setup();

  if (arguments.args[0] == NULL)
    {
      table_stream = stdin;
    }
  else
    {
        table_stream = fopen (arguments.args[0], "r");
        if (!table_stream)
          {
            error (EXIT_FAILURE, errno, "Failed to open response file");
          }
    }
  read_narrow_table (table_stream, arguments.delim, arguments.header_in, 2,
                DATA_TYPE_FLOAT, &response, DATA_TYPE_BOOL, &perts);

  switch (arguments.init)
    {
    case NET_INIT_EMPTY:
      A_init = net_init_empty (response);
      break;
    case NET_INIT_FULL:
      A_init = net_init_full (response);
      break;
    case NET_INIT_RANDOM:
      A_init = net_init_random (response, arguments);
      break;
    case NET_INIT_FILE:
      A_init = net_init_file (response, arguments.init_file, arguments.delim);
      if (arguments.edge_prior == EDGE_PRIOR_PROB)
        {
          A_prior = net_prior (response, A_init, arguments.init_file,
                               arguments.delim);
        }
      break;
    }
  if (!A_init)
    {
      error (EXIT_FAILURE, errno, "Failed to create initial network");
    }

  if (arguments.sampler == SAMPLER_EXHAUSTIVE)
    {
      n = arguments.runs * exp2((double) A_init->data->size1 - 1.0);
      arguments.lpa = LPA_METHOD_FULL;
    }
  else
    {
      n = ((arguments.samples-arguments.burnin)/arguments.thinning)*arguments.runs;
    }

  bmra_main_loop (response, perts, A_init, A_prior, &A_all, &r_all, &lp_A_all,
                  &lp_A_t_all, &lp_r_all, arguments);
  if (arguments.sampler == SAMPLER_EXHAUSTIVE)
    {
      P_A = datatable_alloc (A_all[0]->data->size1, A_all[0]->data->size2,
                             A_all[0]->rownames, A_all[0]->colnames);
      gsl_matrix_memcpy (P_A->data, A_all[0]->data);
    }
  else
    {
      P_A = calc_matrix_means ((const datatable **) A_all, n);
    }
  r_mean = calc_matrix_means ((const datatable **) r_all, n);
  r_std = calc_matrix_std ((const datatable **) r_all, r_mean, n);
  output_results (P_A, r_mean, r_std, arguments);

  if (arguments.trace & TRACE_LPA)
    {
      write_trace (lp_A_all, TRACE_LPA, arguments);
      free (lp_A_all);
    }
  if (arguments.trace & TRACE_LPA_T)
    {
      write_trace (lp_A_t_all, TRACE_LPA_T, arguments);
      free (lp_A_t_all);
    }
  if (arguments.trace & TRACE_LPR)
    {
      write_trace (lp_r_all, TRACE_LPR, arguments);
      free (lp_r_all);
    }

  datatable_free (response);
  datatable_free (perts);
  datatable_free (A_init);
  datatable_free (P_A);
  datatable_free (r_mean);
  datatable_free (r_std);
  if (arguments.sampler == SAMPLER_EXHAUSTIVE)
    datatable_free (A_all[0]);
  for (i=0; i<n; i++)
    {
      if (arguments.sampler != SAMPLER_EXHAUSTIVE)
        datatable_free (A_all[i]);
      datatable_free (r_all[i]);
    }
  free (A_all);
  free (r_all);

  if (arguments.edge_prior == EDGE_PRIOR_PROB)
    datatable_free (A_prior);
  exit (EXIT_SUCCESS);
}
