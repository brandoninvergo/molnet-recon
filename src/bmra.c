/* 
 * bmra.c --- 
 * 
 * Copyright (C) 2017, 2019 Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * Author: Brandon M. Invergo <b.invergo@exeter.ac.uk>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bmra.h"

#define TOL 1e-7

#define REPLACE_MATRIX(old, new) {              \
    if ((*(old)) != NULL)                       \
      {                                         \
        gsl_matrix_free ((*(old)));             \
      };                                        \
    *(old) = *(new);                            \
    *(new) = NULL;                              \
  }

#define REPLACE_VECTOR(old, new) {              \
    if ((*(old)) != NULL)                       \
      {                                         \
        gsl_vector_free ((*(old)));             \
      };                                        \
    *(old) = *(new);                            \
    *(new) = NULL;                              \
  }



gsl_matrix *
sub_matrix_by_col (gsl_matrix *source, gsl_vector *index)
{
  size_t i, j=0;
  size_t ncol = (size_t) gsl_blas_dasum (index);
  gsl_vector_view col;
  gsl_matrix *dest;
  if (source->size2 != index->size)
    error (EXIT_FAILURE, 0, "Invalid matrix index length (column-based sub-matrix) ");
  dest = safe_matrix_alloc (source->size1, ncol);
  for (i=0; i<index->size; i++)
    {
      if ((bool) gsl_vector_get (index, i))
        {
          col = gsl_matrix_column (source, i);
          gsl_matrix_set_col (dest, j++, &col.vector);
        }
    }
  return (dest);
}


gsl_matrix *
sub_matrix_by_row (gsl_matrix *source, const gsl_vector *index)
{
  size_t i, j=0;
  size_t nrow = (size_t) gsl_blas_dasum (index);
  gsl_vector_view row;
  gsl_matrix *dest;
  if (source->size1 != index->size)
    error (EXIT_FAILURE, 0, "Invalid matrix index length (row-based sub-matrix) ");
  dest = safe_matrix_alloc (nrow, source->size2);

  for (i=0; i<index->size; i++)
    {
      if ((size_t) gsl_vector_get (index, i) == 1)
        {
          row = gsl_matrix_row (source, i);
          gsl_matrix_set_row (dest, j++, &row.vector);
        }
    }
  return (dest);
}


gsl_vector *
delete_vector_elem (gsl_vector *v, size_t elem)
{
  gsl_vector *v_i;
  size_t i, j;
  v_i = safe_vector_alloc (v->size-1);
  j = 0;
  for (i=0; i<v->size; i++)
    {
      if (i == elem)
        continue;
      gsl_vector_set (v_i, j++, gsl_vector_get (v, i));
    }
  return v_i;
}


void
remove_edge (gsl_vector *A, gsl_rng *rng)
{
  unsigned int index;
  gsl_vector_uint *edges;
  size_t i, j=0;
  if ((size_t) gsl_vector_max (A) == 0)
    return;
  edges = gsl_vector_uint_alloc ((size_t) gsl_blas_dasum (A));
  for (i=0; i<A->size; i++)
    {
      if ((bool) gsl_vector_get (A, i))
        gsl_vector_uint_set (edges, j++, (unsigned int) i);
    }
  index = gsl_vector_uint_get
    (edges, (size_t) gsl_rng_uniform_int (rng, (unsigned long int) edges->size));
  gsl_vector_set (A, (size_t) index, 0.0);
  gsl_vector_uint_free (edges);
}


void
add_edge_from_A0 (gsl_vector *A, const gsl_vector *A0, size_t node, gsl_rng *rng)
{
  unsigned int index;
  gsl_vector *shared, *diff;
  gsl_vector_uint *A0_edges;
  size_t i, j=0;
  size_t num_edges;
  if ((size_t) gsl_vector_max (A0) == 0)
    return;
  if ((size_t) gsl_blas_dasum (A) == A->size)
    return;
  if (node >= A->size)
    {
      error (0, 0, "Invalid node to omit when adding an edge");
      return;
    }
  shared = gsl_vector_alloc (A0->size);
  gsl_vector_memcpy (shared, A0);
  gsl_vector_mul (shared, A);
  diff = gsl_vector_alloc (A0->size);
  gsl_vector_memcpy (diff, A0);
  gsl_vector_sub (diff, shared);
  gsl_vector_set (diff, node, 0.0);
  num_edges = (size_t) gsl_blas_dasum (diff);
  if (num_edges == 0)
    {
      gsl_vector_free (shared);
      gsl_vector_free (diff);
      return;
    }
  A0_edges = gsl_vector_uint_alloc (num_edges);
  for (i=0; i<A0->size; i++)
    {
      /* If the edge isn't in A0 or it is in A */
      if ((bool) gsl_vector_get (A0, i) &&
          !(bool) gsl_vector_get (A, i) &&
          i != node)
        gsl_vector_uint_set (A0_edges, j++, (unsigned int) i);
    }
  index = gsl_vector_uint_get
    (A0_edges, (size_t) gsl_rng_uniform_int (rng, (unsigned long int) num_edges));
  gsl_vector_set (A, (size_t) index, 1.0);
  gsl_vector_free (shared);
  gsl_vector_free (diff);
  gsl_vector_uint_free (A0_edges);
}


void
add_edge (gsl_vector *A, size_t node, gsl_rng *rng)
{
  unsigned int index;
  gsl_vector *empty;
  gsl_vector_uint *pos_edges;
  size_t num_empty;
  size_t i, j=0;
  if ((size_t) gsl_blas_dasum (A) > A->size-2)
    return;
  if (node >= A->size)
    {
      error (0, 0, "Invalid node to omit when adding an edge");
      return;
    }
  empty = gsl_vector_alloc (A->size);
  gsl_vector_set_all (empty, 1.0);
  gsl_vector_sub (empty, A);
  gsl_vector_set (empty, node, 0.0);
  num_empty = (size_t) gsl_blas_dasum (empty);
  if (num_empty < 1)
    {
      gsl_vector_free (empty);
      return;
    }
  pos_edges = gsl_vector_uint_alloc (num_empty);
  for (i=0; i<empty->size; i++)
    {
      if ((bool) gsl_vector_get (empty, i))
        gsl_vector_uint_set (pos_edges, j++, (unsigned int) i);
    }
  index = gsl_vector_uint_get
    (pos_edges, (size_t) gsl_rng_uniform_int (rng, (unsigned long int) pos_edges->size));
  gsl_vector_set (A, (size_t) index, 1.0);
  gsl_vector_free (empty);
  gsl_vector_uint_free (pos_edges);
}


void
remove_excess_edges (gsl_vector *A, size_t max_edges, gsl_rng *rng)
{
  size_t num_edges, num_nodes, rand_i;

  num_edges = (size_t) gsl_blas_dasum (A);
  num_nodes = (A)->size;
  while (num_edges > max_edges)
    {
      rand_i = (size_t) gsl_rng_uniform_int (rng, (unsigned long int) num_nodes);
      if ((size_t) gsl_vector_get (A, rand_i)==1)
        {
          gsl_vector_set (A, rand_i, 0.0);
          num_edges = (size_t) gsl_blas_dasum (A);
        }
    }
}


gsl_vector *
gen_A_init (const gsl_vector *A0, size_t nkmax, 
            size_t node, gsl_rng *rng, struct arguments args)
{
  size_t num_new_edge;
  size_t i;
  gsl_vector *Ai;
  Ai = safe_vector_calloc (A0->size);
  if (nkmax < 1 || (size_t) gsl_blas_dasum (A0) == 0)
    return Ai;
  num_new_edge = (size_t) gsl_rng_uniform_int (rng, (unsigned long int) nkmax+1);
  for (i=0; i<num_new_edge; i++)
    add_edge_from_A0 (Ai, A0, node, rng);
  return Ai;
}


gsl_vector *
propose_A_new (const gsl_vector *A, const gsl_vector *A0, size_t nkmax,
               size_t node, gsl_rng *rng, struct arguments args)
{
  gsl_vector *A_new;
  A_new = safe_vector_alloc (A0->size);
  gsl_vector_memcpy (A_new, A);
  if ((size_t) gsl_blas_dasum (A) == nkmax)
    {
      remove_edge (A_new, rng);
    }
  else
    {
      if ((size_t) gsl_vector_max (A) == 1)
        {
          if (gsl_rng_uniform (rng) < 0.5)
            {
              if (args.subnet_only)
                add_edge_from_A0 (A_new, A0, node, rng);
              else
                add_edge (A_new, node, rng);
            }
          else
            {
              remove_edge (A_new, rng);
            }
        }
      else
        {
          if (args.subnet_only)
            {
              add_edge_from_A0 (A_new, A0, node, rng);
            }
          else
            {
              add_edge (A_new, node, rng);
            }
        }
    }
  return A_new;
}


gsl_matrix *
lp_A_comp_V1 (gsl_matrix *resp_var)
{
  gsl_matrix *V1;
  if (!resp_var)
    return NULL;
  V1 = safe_matrix_alloc (resp_var->size1, resp_var->size1);
  /* V1 = resp_var * resp_var' */
  gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, resp_var, resp_var, 0.0, V1);
  return V1;
}


gsl_matrix *
lp_A_comp_Vb_1 (gsl_matrix *V1, double g, struct arguments args)
{
  gsl_matrix *I, *Vb_1;
  if (!V1)
    return NULL;
  I = safe_matrix_alloc (V1->size1, V1->size1);
  gsl_matrix_set_identity (I);
  Vb_1 = safe_matrix_alloc (V1->size1, V1->size1);

  /* Vb_1 = (V1 + lambda * I)/c */
  gsl_matrix_memcpy (Vb_1, I);
  gsl_matrix_scale (Vb_1, args.lambda);
  gsl_matrix_add (Vb_1, V1);
  gsl_matrix_scale (Vb_1, 1/g);

  gsl_matrix_free (I);

  return Vb_1;
}


gsl_matrix *
lp_A_comp_Vs_1 (gsl_matrix *V1, gsl_matrix *Vb_1)
{
  gsl_matrix *Vs_1;
  if (!V1)
    return NULL;
  Vs_1 = safe_matrix_alloc (V1->size1, V1->size2);
  /* Vs_1 = V1 + Vb_1 */
  gsl_matrix_memcpy (Vs_1, V1);
  gsl_matrix_add (Vs_1, Vb_1);

  return Vs_1;
}


gsl_matrix *
lp_A_comp_Vs (gsl_matrix *Vs_1)
{
  gsl_matrix *Vs, *Vs_1_LU;
  gsl_permutation *Vs_1_perm;
  int signum;
  if (!Vs_1)
    return NULL;
  Vs = safe_matrix_alloc (Vs_1->size1, Vs_1->size2);
  Vs_1_LU = safe_matrix_alloc (Vs_1->size1, Vs_1->size2);
  Vs_1_perm = gsl_permutation_alloc (Vs->size1);
  if (!Vs_1_perm)
    {
      gsl_matrix_free (Vs);
      gsl_matrix_free (Vs_1_LU);
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  /* Vs = (Vs_1)^-1 */
  gsl_matrix_memcpy (Vs_1_LU, Vs_1);
  gsl_linalg_LU_decomp (Vs_1_LU, Vs_1_perm, &signum);
  gsl_linalg_LU_invert (Vs_1_LU, Vs_1_perm, Vs);

  gsl_matrix_free (Vs_1_LU);
  gsl_permutation_free (Vs_1_perm);

  return Vs;
}


gsl_vector *
lp_A_comp_mus (gsl_matrix *resp_var, const gsl_vector *resp_sub_i, gsl_matrix *Vs)
{
  gsl_vector *mus;
  gsl_matrix *mus_tmp;
  if (!resp_var)
    return NULL;
  mus = safe_vector_alloc (resp_var->size1);
  mus_tmp = safe_matrix_alloc (resp_var->size1, resp_var->size2);

  /* mus = Vs * resp_var * resp_var_i */
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, Vs, resp_var, 0.0, mus_tmp);
  gsl_blas_dgemv (CblasNoTrans, 1.0, mus_tmp, resp_sub_i, 0.0, mus);

  gsl_matrix_free (mus_tmp);

  return mus;
}


double
lp_A_comp_bs (const gsl_vector *resp_sub_i, gsl_vector *mus, gsl_matrix *Vs_1,
              struct arguments args)
{
  gsl_matrix_view mus_m;
  gsl_matrix *bs_tmp;
  gsl_vector_view bs_tmp_v;
  double bs, bs_tmp2, bs_tmp3;

  gsl_blas_ddot (resp_sub_i, resp_sub_i, &bs_tmp2);

  if (Vs_1)
    {
      bs_tmp = safe_matrix_alloc (1, Vs_1->size1);

      /* bs = beta + 0.5*(resp_sub_i'*resp_sub_i - mus'*Vs_1*mus) */
      mus_m = gsl_matrix_view_vector (mus, 1, mus->size);
      gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, &mus_m.matrix, Vs_1, 0.0, bs_tmp);
      bs_tmp_v = gsl_matrix_row (bs_tmp, 0);
      gsl_blas_ddot (&bs_tmp_v.vector, mus, &bs_tmp3);
      bs = args.igamma_b + 0.5*(bs_tmp2 - bs_tmp3);
      gsl_matrix_free (bs_tmp);
    }
  else
    {
      bs = args.igamma_b + 0.5*bs_tmp2;
    }

  return bs;
}


void
calc_lp_comps (gsl_matrix *resp_var, gsl_vector *resp_sub_i,
               gsl_matrix **V1, gsl_matrix **Vb_1, gsl_matrix **Vs_1,
               gsl_matrix **Vs, gsl_vector **mus, double *bs,
               double g,
               struct arguments args)
{
  *V1 = lp_A_comp_V1 (resp_var);
  *Vb_1 = lp_A_comp_Vb_1 (*V1, g, args);
  *Vs_1 = lp_A_comp_Vs_1 (*V1, *Vb_1);
  *Vs = lp_A_comp_Vs (*Vs_1);
  *mus = lp_A_comp_mus (resp_var, resp_sub_i, *Vs);
  *bs = lp_A_comp_bs (resp_sub_i, *mus, *Vs_1, args);
}


double
hamming_distance (const gsl_vector *A, const gsl_vector *B)
{
  gsl_vector *hamming_calc;
  double hamming;

  if (A==NULL || B==NULL)
    return GSL_NAN;
  hamming_calc = safe_vector_alloc (A->size);
  gsl_vector_memcpy (hamming_calc, A);
  gsl_vector_sub (hamming_calc, B);
  hamming = gsl_blas_dasum (hamming_calc);
  gsl_vector_free (hamming_calc);
  return hamming;
}


double
p_Ai_from_prior (const gsl_vector *A, const gsl_vector *A_prior,
                 size_t node, struct arguments args)
{
  gsl_vector *A_prior_mod, *tmp;
  gsl_vector *work, *work2, *work3;
  double p_Ai = 1.0;

  A_prior_mod = safe_vector_alloc (A_prior->size);
  tmp = safe_vector_alloc (A_prior->size);
  work = safe_vector_alloc (A_prior->size);
  work2 = safe_vector_alloc (A_prior->size);
  work3 = safe_vector_alloc (A_prior->size);

  if (args.prob_gamma > 0.0)
    {
      /* Add a small probability, gamma, to edges that have no prior
         probability */
      gsl_vector_set_all (A_prior_mod, 1.0);
      gsl_vector_memcpy (tmp, A_prior);
      for (size_t i=0; i<tmp->size; i++)
        gsl_vector_set (tmp, i, ceil (gsl_vector_get (tmp, i)));
      gsl_vector_sub (A_prior_mod, tmp);
      gsl_vector_scale (A_prior_mod, args.prob_gamma);
      gsl_vector_set (A_prior_mod, node, 0.0);
      gsl_vector_add (A_prior_mod, A_prior);
    }
  else
    gsl_vector_memcpy (A_prior_mod, A_prior);

  gsl_vector_memcpy (work, A);
  gsl_vector_mul (work, A_prior_mod);
  gsl_vector_set_all (work2, 1.0);
  gsl_vector_sub (work2, A);
  gsl_vector_set_all (work3, 1.0);
  gsl_vector_sub (work3, A_prior_mod);
  gsl_vector_mul (work2, work3);
  gsl_vector_add (work, work2);

  for (size_t i=0; i<work->size; i++)
    {
      p_Ai *= gsl_vector_get (work, i);
    }
  gsl_vector_free (A_prior_mod);
  gsl_vector_free (tmp);
  gsl_vector_free (work);
  gsl_vector_free (work2);
  gsl_vector_free (work3);
  return p_Ai;
}


double
calc_p_Ai (const gsl_vector *A, const gsl_vector *A0, const datatable *A_prior,
           size_t node, struct arguments args)
{
  double num_nodes, num_regs, num_changes;
  double p_Ai;
  gsl_vector_view A_prior_i;
  switch (args.edge_prior)
    {
    case EDGE_PRIOR_HAMMING:
      p_Ai = exp (-args.hamming_psi * hamming_distance (A, A0));
      break;
    case EDGE_PRIOR_PROB:
      if (!A_prior)
        error (EXIT_FAILURE, 0, "Edge prior probability table missing");
      A_prior_i = gsl_matrix_row (A_prior->data, node);
      p_Ai = p_Ai_from_prior (A, &A_prior_i.vector, node, args);
      break;
    case EDGE_PRIOR_BETA:
      num_nodes = (double) A->size;
      num_regs = gsl_blas_dasum (A);
      p_Ai = (gsl_sf_choose (num_nodes-1.0, num_regs) *
              gsl_sf_beta (args.beta_a+num_regs,
                           args.beta_b+num_nodes-1.0-num_regs)/
              gsl_sf_beta (args.beta_a, args.beta_b));
      break;
    case EDGE_PRIOR_BETA_HAMMING:
      num_nodes = (double) A->size;
      num_changes = hamming_distance (A, A0);
      p_Ai = (gsl_sf_choose (num_nodes-1.0, num_changes) *
              gsl_sf_beta (args.beta_a+num_changes,
                           args.beta_b+num_nodes-1.0-num_changes)/
              gsl_sf_beta (args.beta_a, args.beta_b));
      break;
    }
  return p_Ai;
}


double
calc_det (const gsl_matrix *m)
{
  gsl_matrix *m_LU;
  gsl_permutation *m_perm;
  int m_signum;
  double det;

  if (m == NULL)
    return 1.0;

  m_LU = safe_matrix_alloc (m->size1, m->size2);
  m_perm = gsl_permutation_alloc (m->size1);
  if (!m_perm)
    {
      gsl_matrix_free (m_LU);
      error (EXIT_FAILURE, errno, "Failed to allocate memory");
    }
  gsl_matrix_memcpy (m_LU, m);
  gsl_linalg_LU_decomp (m_LU, m_perm, &m_signum);
  det = gsl_linalg_LU_det (m_LU, m_signum);
  gsl_matrix_free (m_LU);
  gsl_permutation_free (m_perm);
  return det;
}


double
lp_A_given_R (gsl_matrix *resp_var, gsl_vector *resp_sub_i,
              const gsl_vector *Ai, const gsl_vector *A0,
              const datatable *A_prior, size_t node,
              double *bs, gsl_matrix **Vs, gsl_vector **mus,
              struct arguments args)
{
  double lp;
  double Vs_det, Vb_1_det;
  gsl_matrix *V1=NULL;
  gsl_matrix *Vb_1=NULL;
  gsl_matrix *Vs_1=NULL;
  double as;
  double p_Ai;
  double n_p, n_k, g, x;

  n_p = (double) resp_sub_i->size;
  if (resp_var)
    n_k = (double) resp_var->size1;
  else
    n_k = 0;
  switch (args.zellner_g)
    {
    case ZELLNER_G_RIC:
      g = n_k * n_k;
      break;
    case ZELLNER_G_UIP:
      g = n_p;
      break;
    case ZELLNER_G_BENCH:
      g = GSL_MAX(n_p, n_k*n_k);
      break;
    }
  as = args.igamma_a + n_p/2.0;
  calc_lp_comps (resp_var, resp_sub_i, &V1, &Vb_1, &Vs_1, Vs,
                 mus, bs, g, args);
  p_Ai = calc_p_Ai (Ai, A0, A_prior, node, args);
  Vs_det = calc_det (*Vs);
  Vb_1_det = calc_det (Vb_1);
  switch (args.lpa)
    {
    case LPA_METHOD_IMPLEMENTED:
      /* P(Ai|R) as implemented by Halasz et al and Santra et al in
         their code */
      lp = 0.5*log (Vs_det) + 0.5*log (Vb_1_det) - as*log (*bs) + log (p_Ai);
      break;
    case LPA_METHOD_WRITTEN:
      /* P(Ai|R) as described by Santra et al when calculating to a
         constant of proportionality*/
      lp = (-n_k/2.0)*log(g) + 0.5*log (Vs_det) + 0.5*log (Vb_1_det) - as*log (*bs) + log (p_Ai);
      break;
    case LPA_METHOD_FULL:
      /* Handle a "poisonous" -Inf. Zellner's g can only be 0 in this
         case if n_k (# of potential regulators) is 0.  In this case,
         the following term should evaluate to 0, but GSL sets it to
         -Inf due to the logarithm */
      if (g == 0)
        x = 0.0;
      else
        x = -(n_k/2.0)*log (g);
      lp = (-(n_p/2.0)*log (sqrt (2*M_PI)) + x +
            args.igamma_a*log (args.igamma_b) -
            gsl_sf_lngamma (args.igamma_a) +
            gsl_sf_lngamma (as) + 0.5*log (Vs_det) +
            0.5*log (Vb_1_det) - as*log (*bs) + log (p_Ai));
      break;
    }
  gsl_matrix_free (V1);
  gsl_matrix_free (Vb_1);
  gsl_matrix_free (Vs_1);
  return (lp);
}


bool
sample_mini (double lp_A_t, double lp_A, gsl_rng *rng)
{
  double p_A_alt, p_A;
  double r;
  double accept;

  p_A = exp(lp_A);
  p_A_alt = exp(lp_A_t);
  accept = p_A_alt/(p_A+p_A_alt);
  r = gsl_rng_uniform_pos (rng);
  if (r < accept)
    return true;
  return false;
}


double
sample_r_st (double bs, double p, double n, const gsl_matrix *Vs,
             const gsl_vector *mus, gsl_vector *r, gsl_rng *rng,
             struct arguments args)
{
  gsl_matrix *sig;
  gsl_vector_view diag;
  gsl_matrix *diag_m;
  gsl_vector *ones;
  gsl_matrix *scale;
  gsl_vector *r1, *work;
  double lpr;
  double as = args.igamma_a + (n+p)/2.0;

  if (!Vs || !r)
    {
      return GSL_NEGINF;
    }

  sig = safe_matrix_alloc (Vs->size1, Vs->size2);
  gsl_matrix_memcpy (sig, Vs);
  gsl_matrix_scale (sig, bs/as);

  /* The following is done within MATLAB's mvtrand, but I prefer to
     keep it outside the mvt function.  Scale the Sigma matrix to be a
     correlation matrix instead of a covariance matrix */
  diag = gsl_matrix_diagonal (sig);
  ones = safe_vector_alloc (diag.vector.size);
  gsl_vector_set_all (ones, 1.0);
  if (gsl_vector_equal (&diag.vector, ones) == 0)
    {
      scale = safe_matrix_alloc (diag.vector.size, diag.vector.size);
      diag_m = safe_matrix_alloc (diag.vector.size, 1);
      gsl_matrix_set_col (diag_m, 0, &diag.vector);
      gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, diag_m, diag_m,
                      0.0, scale);
      gsl_matrix_free (diag_m);
      for (size_t i=0; i<scale->size1; i++)
        {
          for (size_t j=0; j<scale->size2; j++)
            gsl_matrix_set (scale, i, j, sqrt (gsl_matrix_get (scale, i, j)));
        }
      gsl_matrix_div_elements (sig, scale);
      gsl_matrix_free (scale);
    }
  gsl_vector_free (ones);

  ran_multivariate_t (rng, mus, sig, as, r);

  r1 = safe_vector_alloc (r->size);
  work = safe_vector_alloc (r->size);
  gsl_vector_memcpy (r1, r);
  gsl_vector_add (r1, mus);
  lpr = ran_multivariate_t_pdf (r1, sig, as, &lpr, work);

  gsl_vector_free (work);
  gsl_vector_free (r1);
  gsl_matrix_free (sig);
  return (log (lpr));
}


double
calc_edge_strengths (gsl_vector *A, double bs, double n, const gsl_matrix *Vs,
                     const gsl_vector *mus, gsl_vector **r, gsl_rng *rng, struct arguments args)
{
  gsl_vector *rt;
  double p, lpr;
  size_t j, k, num_nodes;
  p = mus->size;
  num_nodes = A->size;
  rt = safe_vector_alloc (p);
  *r = safe_vector_calloc (num_nodes);
  lpr = sample_r_st (bs, p, n, Vs, mus, rt, rng, args);
  k = 0;
  for (j=0; j<num_nodes; j++)
    {
      if ((bool) gsl_vector_get (A, j))
        {
          gsl_vector_set (*r, j, gsl_vector_get (rt, k++));
        }
    }
  gsl_vector_free (rt);
  return lpr;
}


void
mcmc_sampler (const datatable *response, gsl_matrix *not_pert,
              const datatable *A_init, const datatable *A_prior,
              datatable **A_all, datatable **r_all, gsl_matrix *lp_A_all,
              gsl_matrix *lp_A_t_all, gsl_matrix *lp_r_all, size_t run,
              struct arguments args)
{
  size_t node;
  size_t num_nodes = A_init->data->size1;

  #pragma omp parallel for private(node)
  for (node=0; node<A_init->data->size1; node++)
    {
      size_t nkmax;
      size_t i, j;
      size_t num_edges;
      gsl_matrix *resp_sub;
      gsl_vector_view not_pert_i;
      gsl_vector_view resp_sub_i;
      gsl_vector *A0, *Ai, *A_new;
      gsl_vector *r;
      gsl_matrix *resp_var=NULL, *resp_var_t;
      double n;
      gsl_matrix *Vs=NULL, *Vs_t=NULL;
      gsl_vector *mus=NULL, *mus_t=NULL;
      double bs, bs_t;
      double lp_A, lp_A_t;
      gsl_rng *rng;
      double lpr;
      /* Random number generator setup */
      rng = gsl_rng_alloc (gsl_rng_default);
      gsl_rng_set (rng, gsl_rng_default_seed + node);

      A0 = gsl_vector_alloc (num_nodes);
      gsl_matrix_get_row (A0, A_init->data, node);
      num_edges = (size_t) gsl_blas_dasum (A0);
      not_pert_i = gsl_matrix_row (not_pert, node);
      resp_sub = sub_matrix_by_col (response->data, &not_pert_i.vector);
      resp_sub_i = gsl_matrix_row (resp_sub, node);
      remove_excess_edges (A0, resp_sub_i.vector.size, rng);
      if (args.subnet_only)
        nkmax = (size_t) GSL_MIN (num_edges, (double) resp_sub->size2-1.0);
      else
        nkmax = (size_t) resp_sub->size2-1.0;

      Ai = gen_A_init (A0, nkmax, node, rng, args);
      if ((size_t) gsl_blas_dasum (Ai) > 0)
        resp_var = sub_matrix_by_row (resp_sub, Ai);
      else
        resp_var = NULL;
      lp_A = lp_A_given_R (resp_var, &resp_sub_i.vector, Ai, A0,
                           A_prior, node, &bs, &Vs, &mus, args);
      n = (double) resp_sub_i.vector.size;

      for (i=0; i<args.samples; i++)
        {
          A_new = propose_A_new (Ai, A0, nkmax, node, rng, args);
          if ((size_t) gsl_blas_dasum (A_new) > 0)
            resp_var_t = sub_matrix_by_row (resp_sub, A_new);
          else
            resp_var_t = NULL;
          lp_A_t = lp_A_given_R (resp_var_t, &resp_sub_i.vector, A_new, A0,
                                   A_prior, node, &bs_t, &Vs_t, &mus_t, args);
          if (lp_A_t > lp_A || sample_mini (lp_A_t, lp_A, rng))
            {
              gsl_vector_memcpy (Ai, A_new);
              lp_A = lp_A_t;
              bs = bs_t;
              REPLACE_MATRIX (&resp_var, &resp_var_t);
              REPLACE_MATRIX (&Vs, &Vs_t);
              REPLACE_VECTOR (&mus, &mus_t);
            }
          else
            {
              gsl_matrix_free (resp_var_t);
              gsl_vector_free (mus_t);
              mus_t = NULL;
              gsl_matrix_free (Vs_t);
              Vs_t = NULL;
            }
          if (args.trace & TRACE_LPA)
            {
              gsl_matrix_set (lp_A_all, run*args.samples+i, node, lp_A);
            }
          if (args.trace & TRACE_LPA_T)
            gsl_matrix_set (lp_A_t_all, run*args.samples+i, node, lp_A_t);
          gsl_vector_free (A_new);
          if (resp_var)
            lpr = calc_edge_strengths (Ai, bs, n, Vs, mus, &r, rng, args);
          else
            {
              r = safe_vector_calloc (num_nodes);
              lpr = GSL_NEGINF;
            }
          if (args.trace & TRACE_LPR)
            gsl_matrix_set (lp_r_all, run*i+i, node, lpr);
          if (i >= args.burnin && i % args.thinning == 0)
            {
              j = (i - args.burnin + run*(args.samples-args.burnin))/args.thinning;
              gsl_matrix_set_row (A_all[j]->data, node, Ai);
              gsl_matrix_set_row (r_all[j]->data, node, r);
            }
          gsl_vector_free (r);
        }
      gsl_matrix_free (resp_var);
      gsl_vector_free (Ai);
      gsl_vector_free (mus);
      gsl_matrix_free (Vs);
      gsl_vector_free (A0);
      gsl_matrix_free (resp_sub);
      gsl_rng_free (rng);
    }
  return;
}


void
gibbs_sampler (const datatable *response, gsl_matrix *not_pert,
               const datatable *A_init, const datatable *A_prior,
               datatable **A_all, datatable **r_all, gsl_matrix *lp_A_all,
               gsl_matrix *lp_A_t_all, gsl_matrix *lp_r_all, size_t run,
               struct arguments args)
{
  size_t node;
  size_t num_nodes = A_init->data->size1;

  #pragma omp parallel for private(node)
  for (node=0; node<A_init->data->size1; node++)
    {
      size_t i, j;
      gsl_matrix *resp_sub;
      gsl_vector_view not_pert_i;
      gsl_vector_view resp_sub_i;
      gsl_vector *A0, *A, *A_new;
      gsl_vector *r;
      gsl_matrix *resp_var=NULL, *resp_var_t;
      double n;
      gsl_matrix *Vs=NULL, *Vs_t=NULL;
      gsl_vector *mus=NULL, *mus_t=NULL;
      double bs, bs_t;
      double lp_A, lp_A_t;
      double old_val, new_val;
      gsl_rng *rng;
      double lpr;
      /* Random number generator setup */
      rng = gsl_rng_alloc (gsl_rng_default);
      gsl_rng_set (rng, gsl_rng_default_seed + node);

      A = gsl_vector_alloc (num_nodes);
      A0 = gsl_vector_alloc (num_nodes);
      gsl_matrix_get_row (A, A_init->data, node);
      gsl_vector_memcpy (A0, A);
      not_pert_i = gsl_matrix_row (not_pert, node);
      resp_sub = sub_matrix_by_col (response->data, &not_pert_i.vector);
      resp_sub_i = gsl_matrix_row (resp_sub, node);
      if ((size_t) gsl_blas_dasum (A) > 0)
        resp_var = sub_matrix_by_row (resp_sub, A);
      else
        resp_var = NULL;
      A_new = safe_vector_alloc (A->size);

      lp_A = lp_A_given_R (resp_var, &resp_sub_i.vector, A, A0,
                           A_prior, node, &bs, &Vs, &mus, args);
      n = (double) resp_sub_i.vector.size;

      for (i=0; i<args.samples; i++)
        {
          for (j=0; j<A->size; j++)
            {
              if (j == node)
                continue;
              if (args.subnet_only && gsl_vector_get (A0, j) < 1.0)
                continue;
              gsl_vector_memcpy (A_new, A);
              old_val = gsl_vector_get (A_new, j);
              gsl_vector_set (A_new, j, 1.0-old_val);
              if ((size_t) gsl_blas_dasum (A_new) > 0)
                resp_var_t = sub_matrix_by_row (resp_sub, A_new);
              else
                resp_var_t = NULL;
              lp_A_t = lp_A_given_R (resp_var_t, &resp_sub_i.vector,
                                       A_new, A0, A_prior, node, &bs_t, &Vs_t,
                                       &mus_t, args);
              gsl_matrix_free (resp_var_t);
              if (lp_A_t > lp_A || sample_mini (lp_A_t, lp_A, rng))
                {
                  new_val = 1.0-old_val;
                  lp_A = lp_A_t;
                  bs = bs_t;
                  REPLACE_MATRIX (&Vs, &Vs_t);
                  REPLACE_VECTOR (&mus, &mus_t);
                }
              else
                {
                  new_val = old_val;
                  gsl_vector_free (mus_t);
                  mus_t = NULL;
                  gsl_matrix_free (Vs_t);
                  Vs_t = NULL;
                }
              if (args.trace & TRACE_LPA)
                gsl_matrix_set (lp_A_all, run*args.samples*A->size+i*j+j, node, lp_A);
              if (args.trace & TRACE_LPA_T)
                gsl_matrix_set (lp_A_t_all, run*i*j+i*j+j, node, lp_A_t);
              gsl_vector_set (A, j, new_val);
            }
          if (mus != NULL)
            lpr = calc_edge_strengths (A, bs, n, Vs, mus, &r, rng, args);
          else
            {
              r = safe_vector_calloc (num_nodes);
              lpr = GSL_NEGINF;
            }
          if (args.trace & TRACE_LPR)
            gsl_matrix_set (lp_r_all, i, node, lpr);
          if (i >= args.burnin && i % args.thinning == 0)
            {
              j = (i - args.burnin)/args.thinning + run*((args.samples-args.burnin)/args.thinning);
              gsl_matrix_set_row (A_all[j]->data, node, A);
              gsl_matrix_set_row (r_all[j]->data, node, r);
            }
          gsl_vector_free (r);
        }
      gsl_matrix_free (resp_var);
      gsl_vector_free (A);
      gsl_vector_free (A0);
      gsl_vector_free (A_new);
      gsl_vector_free (mus);
      gsl_matrix_free (Vs);
      gsl_matrix_free (resp_sub);
      gsl_rng_free (rng);
    }
  return;
}


void
exhaustive_sampler (const datatable *response, gsl_matrix *not_pert,
                    const datatable *A_init, const datatable *A_prior,
                    datatable **A_all, datatable **r_all, gsl_matrix *lp_A_all,
                    gsl_matrix *lp_A_t_all, gsl_matrix *lp_r_all, size_t run,
                    struct arguments args)
{
  size_t node;
  size_t num_nodes = A_init->data->size1;

  #pragma omp parallel for private(node)
  for (node=0; node<A_init->data->size1; node++)
    {
      size_t i, j, k;
      gsl_matrix *resp_sub;
      gsl_vector_view not_pert_i;
      gsl_vector_view resp_sub_i;
      gsl_vector *A0, *A, *A_new;
      gsl_vector *lp_As, *edge_probs;;
      gsl_vector *r;
      gsl_matrix *resp_var=NULL, *resp_var_t;
      double n;
      gsl_matrix *Vs=NULL, *Vs_t=NULL;
      gsl_vector *mus=NULL, *mus_t=NULL;
      double bs, bs_t;
      double lp_A_t, max_lp_A=GSL_NEGINF, lp_A_sum=0.0;
      size_t best_A = 0;
      gsl_rng *rng;
      double lpr;
      bool is_subnet;

      /* Random number generator setup */
      rng = gsl_rng_alloc (gsl_rng_default);
      gsl_rng_set (rng, gsl_rng_default_seed + node);

      A = safe_vector_alloc (num_nodes);
      A0 = safe_vector_alloc (num_nodes);
      edge_probs = safe_vector_alloc (num_nodes);
      lp_As = safe_vector_alloc (exp2 ((double) num_nodes-1.0));
      gsl_matrix_get_row (A0, A_init->data, node);
      gsl_vector_set_zero (A);
      gsl_vector_set_zero (edge_probs);
      gsl_vector_set_zero (lp_As);
      not_pert_i = gsl_matrix_row (not_pert, node);
      resp_sub = sub_matrix_by_col (response->data, &not_pert_i.vector);
      resp_sub_i = gsl_matrix_row (resp_sub, node);
      resp_var = NULL;
      A_new = safe_vector_alloc (A->size);
      n = (double) resp_sub_i.vector.size;

      for (i=0; i<exp2((double) num_nodes-1.0); i++)
        {
          gsl_vector_set_zero (A_new);
          k = 0;
          is_subnet = true;
          for (j=0; j<num_nodes; j++)
            {
              if (j == node)
                continue;
              if (i & (1 << k))
                {
                  if (args.subnet_only && gsl_vector_get (A0, j) < 1.0)
                    {
                      is_subnet = false;
                      break;
                    }
                  gsl_vector_set (A_new, j, 1.0);
                }
              k++;
            }
          if (!is_subnet)
            continue;
          if ((size_t) gsl_blas_dasum (A_new) > 0)
            resp_var_t = sub_matrix_by_row (resp_sub, A_new);
          else
            resp_var_t = NULL;
          lp_A_t = lp_A_given_R (resp_var_t, &resp_sub_i.vector,
                                 A_new, A0, A_prior, node, &bs_t, &Vs_t,
                                 &mus_t, args);
          gsl_matrix_free (resp_var_t);

          j = i*run + i;
          if (mus_t != NULL)
            lpr = calc_edge_strengths (A_new, bs_t, n, Vs_t, mus_t, &r, rng,
                                       args);
          else
            {
              r = safe_vector_calloc (num_nodes);
              lpr = GSL_NEGINF;
            }
          gsl_matrix_set_row (r_all[j]->data, node, r);
          gsl_vector_free (r);

          gsl_vector_set (lp_As, i, exp (lp_A_t));
          lp_A_sum += exp (lp_A_t);
          gsl_vector_free (mus_t);
          mus_t = NULL;
          gsl_matrix_free (Vs_t);
          Vs_t = NULL;
        }
      gsl_vector_scale (lp_As, 1.0/lp_A_sum);
      max_lp_A = gsl_vector_max (lp_As);
      best_A = gsl_vector_max_index (lp_As);

      for (i=0; i<exp2((double) num_nodes-1.0); i++)
        {
          k = 0;
          for (j=0; j<num_nodes; j++)
            {
              if (j == node)
                continue;
              if (i & (1 << k))
                {
                  gsl_vector_set (edge_probs, j,
                                  (gsl_vector_get (edge_probs, j) +
                                   gsl_vector_get (lp_As, i)));
                }
              k++;
            }
        }

      gsl_matrix_set_row (A_all[0]->data, node, edge_probs);
      if ((size_t) gsl_blas_dasum (A_new) > 0)
        resp_var = sub_matrix_by_row (resp_sub, A_new);
      else
        resp_var = NULL;
      lp_A_t = lp_A_given_R (resp_var, &resp_sub_i.vector,
                           A, A0, A_prior, node, &bs, &Vs,
                           &mus, args);

      gsl_matrix_free (resp_var);
      gsl_vector_free (edge_probs);
      gsl_vector_free (lp_As);
      gsl_vector_free (A);
      gsl_vector_free (A0);
      gsl_vector_free (A_new);
      gsl_vector_free (mus);
      gsl_matrix_free (Vs);
      gsl_matrix_free (resp_sub);
      gsl_rng_free (rng);
    }
  return;
}


void
output_results (const datatable *P_A, const datatable *r_mean,
                const datatable *r_std, struct arguments args)
{
  size_t i, j;
  double pa, rm, rs;
  char *node1, *node2;
  printf ("node1%snode2%sP(A)%smean(r)%sstd(r)\n", args.delim, args.delim,
          args.delim, args.delim);
  for (i=0; i<P_A->data->size2; i++)
    {
      node1 = P_A->colnames[i];
      for (j=0; j<P_A->data->size1; j++)
        {
          node2 = P_A->rownames[j];
          pa = gsl_matrix_get (P_A->data, j, i);
          rm = gsl_matrix_get (r_mean->data, j, i);
          rs = gsl_matrix_get (r_std->data, j, i);
          printf ("%s%s%s%s%f%s%f%s%f\n", node1, args.delim, node2, args.delim,
                  pa, args.delim, rm, args.delim, rs);
        }
    }
}


datatable *
calc_matrix_means (const datatable **m_all, size_t n)
{
  datatable *m_mean;
  double mean_fact = 1.0/((double) n);
  m_mean = datatable_alloc (m_all[0]->data->size1, m_all[0]->data->size2,
                            m_all[0]->rownames, m_all[0]->colnames);
  gsl_matrix_set_zero (m_mean->data);
  for (size_t i=0; i<n; i++)
    {
      gsl_matrix_add (m_mean->data, m_all[i]->data);
    }
  gsl_matrix_scale (m_mean->data, mean_fact);
  return m_mean;
}


datatable *
calc_matrix_std (const datatable **m_all, const datatable *m_mean,
                 size_t n)
{
  datatable *m_std;
  gsl_matrix *m_mean_tmp, *m_std_tmp;
  size_t i, j;
  double std_fact;

  if (n > 1.0)
    std_fact = 1.0/((double) n-1.0);
  else
    std_fact = 1.0;
  m_std = datatable_alloc (m_all[0]->data->size1, m_all[0]->data->size2,
                           m_all[0]->rownames, m_all[0]->colnames);
  m_mean_tmp = safe_matrix_alloc (m_all[0]->data->size1, m_all[0]->data->size2);
  m_std_tmp = safe_matrix_alloc (m_all[0]->data->size1, m_all[0]->data->size2);
  gsl_matrix_set_zero (m_std->data);
  gsl_matrix_memcpy (m_mean_tmp, m_mean->data);
  for (i=0; i<n; i++)
    {
      gsl_matrix_memcpy (m_std_tmp, m_all[i]->data);
      gsl_matrix_sub (m_std_tmp, m_mean_tmp);
      gsl_matrix_mul_elements (m_std_tmp, m_std_tmp);
      gsl_matrix_add (m_std->data, m_std_tmp);
    }
  gsl_matrix_scale (m_std->data, std_fact);
  for (i=0; i<m_std->data->size1; i++)
    {
      for (j=0; j<m_std->data->size2; j++)
        {
          gsl_matrix_set (m_std->data, i, j,
                          sqrt (gsl_matrix_get (m_std->data, i, j)));
        }
    }
  gsl_matrix_free (m_mean_tmp);
  gsl_matrix_free (m_std_tmp);
  return m_std;
}


int
bmra_main_loop (const datatable *response, const datatable *perts,
                const datatable *A_init, const datatable *A_prior,
                datatable ***A_all, datatable ***r_all, gsl_matrix **lp_A_all,
                gsl_matrix **lp_A_t_all, gsl_matrix **lp_r_all,
                struct arguments args)
{
  size_t n;
  size_t num_nodes = A_init->data->size1;
  size_t i;
  gsl_matrix *not_pert;

  if (args.sampler == SAMPLER_EXHAUSTIVE)
    {
      n = args.runs * exp2((double) num_nodes-1.0);
      *A_all = (datatable **) malloc (sizeof (datatable *));
      if (!A_all)
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      (*A_all)[0] = datatable_alloc (A_init->data->size1, A_init->data->size2,
                                     A_init->rownames, A_init->colnames);
      if (!(*A_all)[0])
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      gsl_matrix_set_zero ((*A_all)[0]->data);
      *r_all = (datatable **) malloc (n*sizeof (datatable *));
      if (!r_all)
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
    }
  else
    {
      n = ((args.samples-args.burnin)/args.thinning)*args.runs;
      *A_all = (datatable **) malloc (n*sizeof (datatable *));
      if (!A_all)
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      *r_all = (datatable **) malloc (n*sizeof (datatable *));
      if (!r_all)
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
    }

  for (i=0; i<n; i++)
    {
      if (args.sampler != SAMPLER_EXHAUSTIVE)
        {
          (*A_all)[i] = datatable_alloc (A_init->data->size1, A_init->data->size2,
                                         A_init->rownames, A_init->colnames);
          if (!(*A_all)[i])
            {
              error (EXIT_FAILURE, errno, "Failed to allocate memory");
            }
          gsl_matrix_set_zero ((*A_all)[i]->data);
        }
      (*r_all)[i] = datatable_alloc (A_init->data->size1, A_init->data->size2,
                                     A_init->rownames, A_init->colnames);
      if (!(*r_all)[i])
        {
          error (EXIT_FAILURE, errno, "Failed to allocate memory");
        }
      gsl_matrix_set_zero ((*r_all)[i]->data);
    }


  if (args.trace & TRACE_LPA)
    {
      if (args.sampler == SAMPLER_GIBBS)
        *lp_A_all = safe_matrix_alloc (args.runs*args.samples*num_nodes,
                                       num_nodes);
      else
        *lp_A_all = safe_matrix_alloc (args.runs*args.samples, num_nodes);
    }
  if (args.trace & TRACE_LPA_T)
    {
      if (args.sampler == SAMPLER_GIBBS)
        *lp_A_t_all = safe_matrix_alloc (args.runs*args.samples*num_nodes,
                                         num_nodes);
      else
        *lp_A_t_all = safe_matrix_alloc (args.runs*args.samples, num_nodes);
    }
  if (args.trace & TRACE_LPR)
    {
      *lp_r_all = safe_matrix_alloc (args.runs*args.samples, num_nodes);
    }

  not_pert = gsl_matrix_alloc (perts->data->size1, perts->data->size2);
  gsl_matrix_set_all (not_pert, 1);
  gsl_matrix_sub (not_pert, perts->data);
  for (size_t run=0; run<args.runs; run++)
    {
      switch (args.sampler)
      {
      case SAMPLER_MCMC:
        mcmc_sampler (response, not_pert, A_init, A_prior, *A_all, *r_all,
                      *lp_A_all, *lp_A_t_all, *lp_r_all, run, args);
        break;
      case SAMPLER_GIBBS:
        gibbs_sampler (response, not_pert, A_init, A_prior, *A_all, *r_all,
                       *lp_A_all, *lp_A_t_all, *lp_r_all, run, args);
        break;
      case SAMPLER_EXHAUSTIVE:
        exhaustive_sampler (response, not_pert, A_init, A_prior, *A_all, *r_all,
                            *lp_A_all, *lp_A_t_all, *lp_r_all, run, args);
        break;
      }
    }
  gsl_matrix_free (not_pert);
  return (0);
}


datatable *
net_init_empty (const datatable *response)
{
  datatable *A_init;
  size_t n = response->data->size1;
  A_init = datatable_alloc (n, n, response->rownames, response->rownames);
  gsl_matrix_set_zero (A_init->data);
  return (A_init);
}


datatable *
net_init_full (const datatable *response)
{
  datatable *A_init;
  gsl_matrix *I;
  size_t n = response->data->size1;
  A_init = datatable_alloc (n, n, response->rownames, response->rownames);
  gsl_matrix_set_all (A_init->data, 1.0);
  I = gsl_matrix_alloc (n, n);
  gsl_matrix_set_identity (I);
  gsl_matrix_sub (A_init->data, I);
  gsl_matrix_free (I);
  return (A_init);
}


datatable *
net_init_random (const datatable *response, struct arguments args)
{
  datatable *A_init;
  size_t n = response->data->size1;
  gsl_rng *rng;
  double crit;
  rng = gsl_rng_alloc (gsl_rng_default);
  A_init = datatable_alloc (n, n, response->rownames, response->rownames);
  crit = gsl_sf_beta (args.beta_a,
                      args.beta_b);
  for (size_t i=0; i<n; i++)
    {
      for (size_t j=0; j<n; j++)
        {
          if (i==j)
            {
              gsl_matrix_set (A_init->data, i, j, 0.0);
            }
          else
            {
              if (gsl_rng_uniform (rng) < crit)
                gsl_matrix_set (A_init->data, i, j, 1.0);
              else
                gsl_matrix_set (A_init->data, i, j, 0.0);
            }
        }
    }
  gsl_rng_free (rng);
  return (A_init);
}


datatable *
net_init_file (const datatable *response, char *edge_file, char *delim)
{
  datatable *A_init = NULL;
  FILE *edge_stream;
  size_t line_len = 0;
  char *line = NULL;
  char *node1, *node2;
  size_t n = response->data->size1;
  A_init = datatable_alloc (n, n, response->rownames, response->rownames);
  gsl_matrix_set_zero (A_init->data);
  edge_stream = fopen (edge_file, "r");
  if (!edge_stream)
    {
      error (EXIT_FAILURE, errno, "Failed to open network edgelist");
    }
  while (getline (&line, &line_len, edge_stream) > 0)
    {
      line[strcspn(line, "\n")] = '\0';
      node1 = strtok (line, delim);
      node2 = strtok (NULL, delim);
      /* Source node in the column, dest node in the row */
      datatable_set (A_init, node2, node1, 1.0);
    }
  fclose (edge_stream);
  free (line);
  return (A_init);
}


datatable *
net_prior (const datatable *response, datatable *A_init, char *edge_file,
           char *delim)
{
  datatable *A_prior = NULL;
  FILE *edge_stream;
  size_t line_len = 0;
  char *line = NULL;
  char *node1, *node2;
  char *prob_str = NULL;
  double prob;
  size_t line_no = 0;
  size_t n = response->data->size1;
  A_prior = datatable_alloc (n, n, response->rownames, response->rownames);
  gsl_matrix_set_zero (A_prior->data);
  edge_stream = fopen (edge_file, "r");
  if (!edge_stream)
    {
      error (EXIT_FAILURE, errno, "Failed to open network edgelist");
    }
  while (getline (&line, &line_len, edge_stream) > 0)
    {
      line[strcspn(line, "\n")] = '\0';
      node1 = strtok (line, delim);
      node2 = strtok (NULL, delim);
      /* Source node in the column, dest node in the row */
      prob_str = strtok (NULL, delim);
      if (!prob_str)
        {
          error (EXIT_FAILURE, 0, "Initial network file does not contain "
                 "edge weights.");
        }
      prob = strtod (prob_str, NULL);
      datatable_set (A_prior, node2, node1, prob);
      if (prob == 0.0)
        datatable_set (A_init, node2, node1, 0.0);
      line_no++;
    }
  fclose (edge_stream);
  free (line);
  return (A_prior);
}

