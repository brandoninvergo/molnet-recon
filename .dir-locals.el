;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((c-mode
  (flycheck-gcc-include-path "." ".." "./uthash/src")
  (flycheck-clang-include-path "." ".." "./uthash/src")
  (flycheck-cppcheck-include-path "." ".." "./uthash/src")
  (flycheck-clang-args "-fopenmp=libomp")
  (flycheck-gcc-args "-fopenmp")))

